/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RadioPaging.h"

#include <cassert>

#include "CharSet.h"
#include "RdsProgram.h"

namespace ISO62106 {

RadioPaging::RadioPaging(const RdsProgram & parent) :
    m_parent(parent),
    m_lastAb(0),
    m_lastSa(0),
    m_type(0),
    m_y(),
    m_z(),
    m_a(),
    m_c(),
    m_x(),
    m_f()
{
}

void RadioPaging::decode(uint8_t ab, uint8_t sa, uint16_t p1, uint16_t p2)
{
    /* check */
    assert(ab <= 0x1);
    assert(sa <= 0xf);
    assert(p1 <= 0xffff);
    assert(p2 <= 0xffff);

    /* check if A/B has changed */
    if (m_lastAb != ab) {
        m_type = 0;
    }

    /* Additional message content */
    switch (sa) {
    case 0:
        /* No additional message */
        m_type = 0;
        m_y[0] = (p1 >> 12) & 0xf; /* Y1 */
        m_y[1] = (p1 >>  8) & 0xf; /* Y2 */
        m_z[0] = (p1 >>  4) & 0xf; /* Z1 */
        m_z[1] = (p1 >>  0) & 0xf; /* Z2 */
        m_z[2] = (p2 >> 12) & 0xf; /* Z3 */
        m_z[3] = (p2 >>  8) & 0xf; /* Z4 */
        m_x[0] = (p2 >>  4) & 0xf; /* X1 (ERP) */
        m_x[1] = (p2 >>  0) & 0xf; /* X2 (ERP) */
        /** @todo process here */
        break;
    case 1:
        /* part of functions message */
        m_type = 5;
        m_y[0] = (p1 >> 12) & 0xf; /* Y1 */
        m_y[1] = (p1 >>  8) & 0xf; /* Y2 */
        m_z[0] = (p1 >>  4) & 0xf; /* Z1 */
        m_z[1] = (p1 >>  0) & 0xf; /* Z2 */
        m_z[2] = (p2 >> 12) & 0xf; /* Z3 */
        m_z[3] = (p2 >>  8) & 0xf; /* Z4 */
        m_x[0] = (p2 >>  4) & 0xf; /* X1 */
        m_x[1] = (p2 >>  0) & 0xf; /* X2 */
        break;
    case 2:
        /* 10 digit numeric message */
        m_type = 1;
        m_y[0] = (p1 >> 12) & 0xf; /* Y1 */
        m_y[1] = (p1 >>  8) & 0xf; /* Y2 */
        m_z[0] = (p1 >>  4) & 0xf; /* Z1 */
        m_z[1] = (p1 >>  0) & 0xf; /* Z2 */
        m_z[2] = (p2 >> 12) & 0xf; /* Z3 */
        m_z[3] = (p2 >>  8) & 0xf; /* Z4 */
        m_a[0] = (p2 >>  4) & 0xf; /* A1 */
        m_a[1] = (p2 >>  0) & 0xf; /* A2 */
        break;
    case 3:
        if ((m_type == 1) && (m_lastSa == 2)) {
            /* 10 digit numeric message */
            m_a[2] = (p1 >> 12) & 0xf; /* A3 */
            m_a[3] = (p1 >>  8) & 0xf; /* A4 */
            m_a[4] = (p1 >>  4) & 0xf; /* A5 */
            m_a[5] = (p1 >>  0) & 0xf; /* A6 */
            m_a[6] = (p2 >> 12) & 0xf; /* A7 */
            m_a[7] = (p2 >>  8) & 0xf; /* A8 */
            m_a[8] = (p2 >>  4) & 0xf; /* A9 */
            m_a[9] = (p2 >>  0) & 0xf; /* A10 */
            /** @todo process here */
        } else if ((m_type == 5) && (m_lastSa == 1)) {
            /* part of functions message */
            m_x[2] = (p1 >> 12) & 0xf; /* X3 */
            m_f[0] = (p1 >>  8) & 0xf; /* F1 */
            m_f[1] = (p1 >>  4) & 0xf; /* F2 */
            m_f[2] = (p1 >>  0) & 0xf; /* F3 */
            m_f[3] = (p2 >> 12) & 0xf; /* F4 */
            m_f[4] = (p2 >>  8) & 0xf; /* F5 */
            m_f[5] = (p2 >>  4) & 0xf; /* F6 */
            m_f[6] = (p2 >>  0) & 0xf; /* F7 */
            /** @todo process here */
        }
        break;
    case 4:
        /* 18 digit numeric message */
        m_type = 2;
        m_y[0] = (p1 >> 12) & 0xf; /* Y1 */
        m_y[1] = (p1 >>  8) & 0xf; /* Y2 */
        m_z[0] = (p1 >>  4) & 0xf; /* Z1 */
        m_z[1] = (p1 >>  0) & 0xf; /* Z2 */
        m_z[2] = (p2 >> 12) & 0xf; /* Z3 */
        m_z[3] = (p2 >>  8) & 0xf; /* Z4 */
        m_a[0] = (p2 >>  4) & 0xf; /* A1 */
        m_a[1] = (p2 >>  0) & 0xf; /* A2 */
        break;
    case 5:
        if ((m_type == 2) && (m_lastSa = 4)) {
            /* 18 digit numeric message */
            m_a[2] = (p1 >> 12) & 0xf; /* A3 */
            m_a[3] = (p1 >>  8) & 0xf; /* A4 */
            m_a[4] = (p1 >>  4) & 0xf; /* A5 */
            m_a[5] = (p1 >>  0) & 0xf; /* A6 */
            m_a[6] = (p2 >> 12) & 0xf; /* A7 */
            m_a[7] = (p2 >>  8) & 0xf; /* A8 */
            m_a[8] = (p2 >>  4) & 0xf; /* A9 */
            m_a[9] = (p2 >>  0) & 0xf; /* A10 */
        } else if ((m_type == 4) && (m_lastSa = 7)) {
            /* 15 digit numeric message in international paging */
            m_x[2] = (p1 >> 12) & 0xf; /* X3 */
            m_a[0] = (p1 >>  8) & 0xf; /* A1 */
            m_a[1] = (p1 >>  4) & 0xf; /* A2 */
            m_a[2] = (p1 >>  0) & 0xf; /* A3 */
            m_a[3] = (p2 >> 12) & 0xf; /* A4 */
            m_a[4] = (p2 >>  8) & 0xf; /* A5 */
            m_a[5] = (p2 >>  4) & 0xf; /* A6 */
            m_a[6] = (p2 >>  0) & 0xf; /* A7 */
        }
        break;
    case 6:
        if ((m_type == 2) && (m_lastSa = 5)) {
            /* 18 digit numeric message */
            m_a[10] = (p1 >> 12) & 0xf; /* A11 */
            m_a[11] = (p1 >>  8) & 0xf; /* A12 */
            m_a[12] = (p1 >>  4) & 0xf; /* A13 */
            m_a[13] = (p1 >>  0) & 0xf; /* A14 */
            m_a[14] = (p2 >> 12) & 0xf; /* A15 */
            m_a[15] = (p2 >>  8) & 0xf; /* A16 */
            m_a[16] = (p2 >>  4) & 0xf; /* A17 */
            m_a[17] = (p2 >>  0) & 0xf; /* A18 */
            /** @todo process here */
        } else if ((m_type == 4) && (m_lastSa = 5)) {
            /* 15 digit numeric message in international paging */
            m_a[ 7] = (p1 >> 12) & 0xf; /* A8 */
            m_a[ 8] = (p1 >>  8) & 0xf; /* A9 */
            m_a[ 9] = (p1 >>  4) & 0xf; /* A10 */
            m_a[10] = (p1 >>  0) & 0xf; /* A11 */
            m_a[11] = (p2 >> 12) & 0xf; /* A12 */
            m_a[12] = (p2 >>  8) & 0xf; /* A13 */
            m_a[13] = (p2 >>  4) & 0xf; /* A14 */
            m_a[14] = (p2 >>  0) & 0xf; /* A15 */
            /** @todo process here */
        }
        break;
    case 7:
        /* 15 digit numeric message in international paging */
        m_type = 4;
        m_y[0] = (p1 >> 12) & 0xf; /* Y1 */
        m_y[1] = (p1 >>  8) & 0xf; /* Y2 */
        m_z[0] = (p1 >>  4) & 0xf; /* Z1 */
        m_z[1] = (p1 >>  0) & 0xf; /* Z2 */
        m_z[2] = (p2 >> 12) & 0xf; /* Z3 */
        m_z[3] = (p2 >>  8) & 0xf; /* Z4 */
        m_x[0] = (p2 >>  4) & 0xf; /* X1 */
        m_x[1] = (p2 >>  0) & 0xf; /* X2 */
        break;
    case 8:
        /* Alphanumeric message */
        m_type = 3;
        /* Group and individual code */
        m_y[0] = (p1 >> 12) & 0xf; /* Y1 */
        m_y[1] = (p1 >>  8) & 0xf; /* Y2 */
        m_z[0] = (p1 >>  4) & 0xf; /* Z1 */
        m_z[1] = (p1 >>  0) & 0xf; /* Z2 */
        m_z[2] = (p2 >> 12) & 0xf; /* Z3 */
        m_z[3] = (p2 >>  8) & 0xf; /* Z4 */
        m_x[0] = (p2 >>  4) & 0xf; /* X1 (ERP) */
        m_x[1] = (p2 >>  0) & 0xf; /* X2 (ERP) */
        break;
    case 9:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa == 8)) {
            /* Message characters */
            m_c[0] = basicCharSet[p1 >> 8];
            m_c[1] = basicCharSet[p1 & 0xf];
            m_c[2] = basicCharSet[p2 >> 8];
            m_c[3] = basicCharSet[p2 & 0xf];
            /** @todo process here (possible for ERP depending on X1X2) */
        }
        break;
    case 10:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa == 9)) {
            /* Message characters */
            m_c[4] = basicCharSet[p1 >> 8];
            m_c[5] = basicCharSet[p1 & 0xf];
            m_c[6] = basicCharSet[p2 >> 8];
            m_c[7] = basicCharSet[p2 & 0xf];
        }
        break;
    case 11:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa == 10)) {
            /* Message characters */
            m_c[ 8] = basicCharSet[p1 >> 8];
            m_c[ 9] = basicCharSet[p1 & 0xf];
            m_c[10] = basicCharSet[p2 >> 8];
            m_c[11] = basicCharSet[p2 & 0xf];
        }
        break;
    case 12:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa == 11)) {
            /* Message characters */
            m_c[12] = basicCharSet[p1 >> 8];
            m_c[13] = basicCharSet[p1 & 0xf];
            m_c[14] = basicCharSet[p2 >> 8];
            m_c[15] = basicCharSet[p2 & 0xf];
        }
        break;
    case 13:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa == 12)) {
            /* Message characters */
            m_c[16] = basicCharSet[p1 >> 8];
            m_c[17] = basicCharSet[p1 & 0xf];
            m_c[18] = basicCharSet[p2 >> 8];
            m_c[19] = basicCharSet[p2 & 0xf];
        }
        break;
    case 14:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa == 13)) {
            /* Message characters */
            m_c[20] = basicCharSet[p1 >> 8];
            m_c[21] = basicCharSet[p1 & 0xf];
            m_c[22] = basicCharSet[p2 >> 8];
            m_c[23] = basicCharSet[p2 & 0xf];
        }
        break;
    case 15:
        /* Alphanumeric message */
        if ((m_type == 3) && (m_lastSa >= 8) && (m_lastSa <= 14)) {
            /* End of alphanumeric message: last four or fewer message characters */
            m_c[(m_lastSa - 8) * 4 + 0] = basicCharSet[p1 >> 8];
            m_c[(m_lastSa - 8) * 4 + 1] = basicCharSet[p1 & 0xf];
            m_c[(m_lastSa - 8) * 4 + 2] = basicCharSet[p2 >> 8];
            m_c[(m_lastSa - 8) * 4 + 3] = basicCharSet[p2 & 0xf];
            /** @todo process here (differently for RP and ERP depending on X1X2) */
        }
        break;
    }
    m_lastAb = ab;
    m_lastSa = sa;

    decodeIssue.emit("RP: not implemented yet");
}

void RadioPaging::decodeIdent(uint16_t /*id*/)
{
    decodeIssue.emit("RP: not implemented yet");
}

void RadioPaging::decodeRpc(uint8_t rpc)
{
    /* check */
    assert(rpc <= 0x1f);

    //uint8_t pgc; /* pager group codes */

    /* network group designation */
    uint8_t ngd = (rpc >> 2) & 0x7;
    switch (ngd) {
    case 0:
        /* No basic paging on channel */
        /** @todo process here */
        break;
    case 1:
        /* Group codes: 00-99 (100 codes) */
        /** @todo process here */
        break;
    case 2:
        /* Group codes: 00-39 (40 codes) */
        /** @todo process here */
        break;
    case 3:
        /* Group codes: 40-99 (60 codes) */
        /** @todo process here */
        break;
    case 4:
        /* Group codes: 40-69 (30 codes) */
        /** @todo process here */
        break;
    case 5:
        /* Group codes: 70-99 (30 codes) */
        /** @todo process here */
        break;
    case 6:
        /* Group codes: 00-19 (20 codes) */
        /** @todo process here */
        break;
    case 7:
        /* Group codes: 20-39 (20 codes) */
        /** @todo process here */
        break;
    }

    /* battery saving interval synchronization and identification */
    uint8_t bs = rpc & 0x3;
    switch (bs) {
    case 0:
        /** @todo process here */
        break;
    case 1:
        /** @todo process here */
        break;
    case 2:
        /** @todo process here */
        break;
    case 3:
        /** @todo process here */
        break;
    }

    decodeIssue.emit("RP: not implemented yet");
}

}
