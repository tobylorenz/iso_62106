/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <map>

#include <sigc++-2.0/sigc++/signal.h>

#include "AlternativeFrequencies.h"
#include "ClockTime.h"
#include "DecoderIdentification.h"
#include "EmergencyWarningSystem.h"
#include "EnhancedOtherNetworks.h"
#include "EnhancedRadioPaging.h"
#include "ExtendedCountryCode.h"
#include "InHouse.h"
#include "LanguageIdentificationCode.h"
#include "MusicSpeech.h"
#include "OpenDataApplications.h"
#include "ProgrammeIdentification.h"
#include "ProgrammeItemNumber.h"
#include "ProgrammeService.h"
#include "ProgrammeType.h"
#include "ProgrammeTypeName.h"
#include "RadioPaging.h"
#include "RadioText.h"
#include "TrafficAnnouncement.h"
#include "TrafficProgramme.h"
#include "TransparentDataChannels.h"

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/**
 * @brief RDS program information
 *
 * This class contains all information corresponding to one RDS program (same
 * PI code).
 */
class ISO_62106_EXPORT RdsProgram
{
public:
    RdsProgram();

    /* primary key is ecc (23..16) & pi (15..0 including cc) */

    /** Programme Identification (PI) code */
    ProgrammeIdentification programmeIdentification;

    /* Type 0 groups: Basic tuning and switching information */

    /** Programme Service (PS) name */
    ProgrammService programmeService;

    /** Programme Type (PTY) code */
    ProgrammeType programmeType;

    /** Traffic Programme (TP) identification code */
    TrafficProgramme trafficProgramme;

    /** Alternative Frequencies (AF) list */
    AlternativeFrequencies alternativeFrequencies;

    /** Traffic Announcement (TA) code */
    TrafficAnnouncement trafficAnnouncement;

    /** Decoder Identification (DI) */
    DecoderIdentification decoderIdentification;

    /** Music Speech (MS) code */
    MusicSpeech musicSpeech;

    /* Type 1 groups: Programme Item Number and slow labelling codes */

    /** Programme Item Number (PIN) */
    ProgrammeItemNumber programmeItemNumber;

    /** Extended Country Code (ECC) */
    ExtendedCountryCode extendedCountryCode;

    /** Language Identification Code */
    LanguageIdentificationCode languageIdentificationCode;

    /** Emergency Warning System */
    EmergencyWarningSystem emergencyWarningSystem;

    // Linkage Actuator is in eon

    /* Type 1A groups: Enhanced Paging */

    /** Enhanced Radio Paging (ERP) */
    EnhancedRadioPaging enhancedRadioPaging;

    /* Type 2 groups: RadioText */

    /** Radio Text (RT) */
    RadioText radioText;

    /* Type 3A groups: Application identification for Open Data */

    /** Open Data Application (ODA) Assignment of AIDs to AGT */
    OpenDataApplications openDataApplications;

    /* Type 4A groups: Clock-time and date */

    /** Clock Time (CT) */
    ClockTime clockTime;

    /* Type 5 groups: Transparent data channels or ODA */

    /** Transparent data channels (TDC) */
    TransparentDataChannels transparentDataChannels;

    /* Type 6 groups: In-house applications or ODA */

    /** In-house applications (IH) */
    InHouse inHouse;

    /* Type 7A groups: Radio Paging */

    /** Radio Paging (RP) */
    RadioPaging radioPaging;

    /* Type 10A groups: Programme Type Name */

    /** Programme Type Name (PTYN) */
    ProgrammeTypeName programmeTypeName;

    /* Type 13A groups: Enhanced Paging */
    // see 1A

    /* Type 14 groups: Enhanced Other Networks information */

    /** Enhanced Other Networks (EON) */
    EnhancedOtherNetworks enhancedOtherNetworks;

    /**
     * @brief Statistics counter
     *
     * This variable contains a statistics counter for received RDS groups and versions.
     */
    uint32_t groupTypeCount[16][2];

    /**
     * @brief Decode and handle RDS message
     *
     * This function decodes and handles RDS messages.
     *
     * @param[in] blk2 Block 2 data
     * @param[in] blk3 Block 3 data (contains PI code in type B)
     * @param[in] blk4 Block 4 data
     */
    void decode(uint16_t blk2, uint16_t blk3, uint16_t blk4);
};

/**
 * This signals a decode issue.
 *
 * Usual events are:
 *   - not assigned, spare bits, variant unallocated, reserved for future use
 *   - for use by broadcasters, reserved for broadcasters use
 *   - not implemented yet
 *   - unknown ODA AID
 *   - iconv error
 */
extern ISO_62106_EXPORT sigc::signal<void, std::string> decodeIssue;

/**
 * Return reference to RDS program list.
 *
 * @return Reference to RDS program list
 */
extern ISO_62106_EXPORT std::map<uint16_t, RdsProgram> & rdsProgram(); /* singleton */

/**
 * Return reference to RDS program.
 *
 * @param[in] pi PI code
 * @return Reference to RDS program
 */
extern ISO_62106_EXPORT RdsProgram & rdsProgram(uint16_t pi);

}
