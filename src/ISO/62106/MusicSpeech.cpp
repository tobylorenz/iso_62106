/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "MusicSpeech.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

MusicSpeech::MusicSpeech(const RdsProgram & parent) :
    m_parent(parent),
    m_music(false)
{
}

bool MusicSpeech::music() const
{
    return m_music;
}

void MusicSpeech::decode(bool ms)
{
    /* check */
//    assert(ms <= 0x1);

    /* new value */
    bool oldMusic = m_music;
    m_music = ms;
    bool change = (oldMusic != m_music);

    /* change check */
    if (change) {
        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> MusicSpeech::onChange;

sigc::signal<void, uint16_t> MusicSpeech::onUpdate;

}
