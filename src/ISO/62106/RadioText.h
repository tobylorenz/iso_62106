/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Radio Text (RT) */
class ISO_62106_EXPORT RadioText
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit RadioText(const RdsProgram & parent);

    /**
     * Return the value UCS-2 encoded.
     *
     * @return value (UCS-2 encoded)
     */
    std::u16string valueUcs2() const;

    /**
     * Return the value UTF-8 encoded.
     *
     * @return value (UTF-8 encoded)
     */
    std::string valueUtf8() const;

    /**
     * @brief Decodes and handles received RT characters
     *
     * This function decodes and handles received RT characters.
     * @param[in] ab Text A/B flag
     * @param[in] sa Text segment address code
     * @param[in] c1 Text character 1
     * @param[in] c2 Text character 2
     * @param[in] c3 Text character 3
     * @param[in] c4 Text character 4
     */
    void decodeA(uint8_t ab, uint8_t sa, uint8_t c1, uint8_t c2, uint8_t c3, uint8_t c4);

    /**
     * @brief Decodes and handles received RT characters
     *
     * This function decodes and handles received RT characters.
     * @param[in] ab Text A/B flag
     * @param[in] sa Text segment address code
     * @param[in] c1 Text character 1
     * @param[in] c2 Text character 2
     */
    void decodeB(uint8_t ab, uint8_t sa, uint8_t c1, uint8_t c2);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Radio Text (RT) */
    std::array<char16_t, 64> m_value; // encoding: UCS-2

    /** RT last A/B flag */
    uint8_t m_lastAb : 1;
};

}
