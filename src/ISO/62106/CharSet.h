/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>

#include "platform.h"

namespace ISO62106 {

/**
 * @brief Basic Character Set (maps from RDS to UCS-2)
 *
 * The basic character set shall be used for programme service name
 * (PS), programme type name (PTYN), RadioText (RT) and alphanumeric radio paging (RP).
 */
constexpr std::array<char16_t, 256> basicCharSet = {{
        /* 0x00 */
        0x0000,    // NOT ASSIGNED
        0x0001,    // NOT ASSIGNED
        0x0002,    // NOT ASSIGNED
        0x0003,    // NOT ASSIGNED
        0x0004,    // NOT ASSIGNED
        0x0005,    // NOT ASSIGNED
        0x0006,    // NOT ASSIGNED
        0x0007,    // NOT ASSIGNED
        0x0008,    // NOT ASSIGNED
        0x0009,    // NOT ASSIGNED
        0x000A,    // LINE FEED
        0x000B,    // END OF HEADLINE
        0x000C,    // NOT ASSIGNED
        0x000D,    // CARRIAGE RETURN
        0x000E,    // NOT ASSIGNED
        0x000F,    // NOT ASSIGNED
        /* 0x10 */
        0x0010,    // NOT ASSIGNED
        0x0011,    // NOT ASSIGNED
        0x0012,    // NOT ASSIGNED
        0x0013,    // NOT ASSIGNED
        0x0014,    // NOT ASSIGNED
        0x0015,    // NOT ASSIGNED
        0x0016,    // NOT ASSIGNED
        0x0017,    // NOT ASSIGNED
        0x0018,    // NOT ASSIGNED
        0x0019,    // NOT ASSIGNED
        0x001A,    // NOT ASSIGNED
        0x001B,    // NOT ASSIGNED
        0x001C,    // NOT ASSIGNED
        0x001D,    // NOT ASSIGNED
        0x001E,    // NOT ASSIGNED
        0x001F,    // WORD BREAK - SOFT HYPHEN
        /* 0x20 */
        0x0020,    // SPACE
        0x0021,    // EXCLAMATION MARK
        0x0022,    // QUOTATION MARK
        0x0023,    // NUMBER SIGN
        0x00A4,    // CURRENCY SIGN
        0x0025,    // PERCENT SIGN
        0x0026,    // AMPERSAND
        0x0027,    // APOSTROPHE
        0x0028,    // LEFT PARENTHESIS
        0x0029,    // RIGHT PARENTHESIS
        0x002A,    // ASTERISK
        0x002B,    // PLUS SIGN
        0x002C,    // COMMA
        0x002D,    // HYPHEN-MINUS
        0x002E,    // FULL STOP
        0x002F,    // SOLIDUS
        /* 0x30 */
        0x0030,    // DIGIT ZERO
        0x0031,    // DIGIT ONE
        0x0032,    // DIGIT TWO
        0x0033,    // DIGIT THREE
        0x0034,    // DIGIT FOUR
        0x0035,    // DIGIT FIVE
        0x0036,    // DIGIT SIX
        0x0037,    // DIGIT SEVEN
        0x0038,    // DIGIT EIGHT
        0x0039,    // DIGIT NINE
        0x003A,    // COLON
        0x003B,    // SEMICOLON
        0x003C,    // LESS-THAN SIGN
        0x003D,    // EQUALS SIGN
        0x003E,    // GREATER-THAN SIGN
        0x003F,    // QUESTION MARK
        /* 0x40 */
        0x0040,    // COMMERCIAL AT
        0x0041,    // LATIN CAPITAL LETTER A
        0x0042,    // LATIN CAPITAL LETTER B
        0x0043,    // LATIN CAPITAL LETTER C
        0x0044,    // LATIN CAPITAL LETTER D
        0x0045,    // LATIN CAPITAL LETTER E
        0x0046,    // LATIN CAPITAL LETTER F
        0x0047,    // LATIN CAPITAL LETTER G
        0x0048,    // LATIN CAPITAL LETTER H
        0x0049,    // LATIN CAPITAL LETTER I
        0x004A,    // LATIN CAPITAL LETTER J
        0x004B,    // LATIN CAPITAL LETTER K
        0x004C,    // LATIN CAPITAL LETTER L
        0x004D,    // LATIN CAPITAL LETTER M
        0x004E,    // LATIN CAPITAL LETTER N
        0x004F,    // LATIN CAPITAL LETTER O
        /* 0x50 */
        0x0050,    // LATIN CAPITAL LETTER P
        0x0051,    // LATIN CAPITAL LETTER Q
        0x0052,    // LATIN CAPITAL LETTER R
        0x0053,    // LATIN CAPITAL LETTER S
        0x0054,    // LATIN CAPITAL LETTER T
        0x0055,    // LATIN CAPITAL LETTER U
        0x0056,    // LATIN CAPITAL LETTER V
        0x0057,    // LATIN CAPITAL LETTER W
        0x0058,    // LATIN CAPITAL LETTER X
        0x0059,    // LATIN CAPITAL LETTER Y
        0x005A,    // LATIN CAPITAL LETTER Z
        0x005B,    // LEFT SQUARE BRACKET
        0x005C,    // REVERSE SOLIDUS
        0x005D,    // RIGHT SQUARE BRACKET
        0x2015,    // HORIZONTAL BAR
        0x005F,    // LOW LINE
        /* 0x60 */
        0x2016,    // DOUBLE VERTICAL LINE
        0x0061,    // LATIN SMALL LETTER A
        0x0062,    // LATIN SMALL LETTER B
        0x0063,    // LATIN SMALL LETTER C
        0x0064,    // LATIN SMALL LETTER D
        0x0065,    // LATIN SMALL LETTER E
        0x0066,    // LATIN SMALL LETTER F
        0x0067,    // LATIN SMALL LETTER G
        0x0068,    // LATIN SMALL LETTER H
        0x0069,    // LATIN SMALL LETTER I
        0x006A,    // LATIN SMALL LETTER J
        0x006B,    // LATIN SMALL LETTER K
        0x006C,    // LATIN SMALL LETTER L
        0x006D,    // LATIN SMALL LETTER M
        0x006E,    // LATIN SMALL LETTER N
        0x006F,    // LATIN SMALL LETTER O
        /* 0x70 */
        0x0070,    // LATIN SMALL LETTER P
        0x0071,    // LATIN SMALL LETTER Q
        0x0072,    // LATIN SMALL LETTER R
        0x0073,    // LATIN SMALL LETTER S
        0x0074,    // LATIN SMALL LETTER T
        0x0075,    // LATIN SMALL LETTER U
        0x0076,    // LATIN SMALL LETTER V
        0x0077,    // LATIN SMALL LETTER W
        0x0078,    // LATIN SMALL LETTER X
        0x0079,    // LATIN SMALL LETTER Y
        0x007A,    // LATIN SMALL LETTER Z
        0x007B,    // LEFT CURLY BRACKET
        0x007C,    // VERTICAL LINE
        0x007D,    // RIGHT CURLY BRACKET
        0x203E,    // OVERLINE
        0x007F,    // NOT ASSIGNED
        /* 0x80 */
        0x00E1,    // LATIN SMALL LETTER A WITH ACUTE
        0x00E0,    // LATIN SMALL LETTER A WITH GRAVE
        0x00E9,    // LATIN SMALL LETTER E WITH ACUTE
        0x00E8,    // LATIN SMALL LETTER E WITH GRAVE
        0x00ED,    // LATIN SMALL LETTER I WITH ACUTE
        0x00EC,    // LATIN SMALL LETTER I WITH GRAVE
        0x00F3,    // LATIN SMALL LETTER O WITH ACUTE
        0x00F2,    // LATIN SMALL LETTER O WITH GRAVE
        0x00FA,    // LATIN SMALL LETTER U WITH ACUTE
        0x00F9,    // LATIN SMALL LETTER U WITH GRAVE
        0x00D1,    // LATIN CAPITAL LETTER N WITH TILDE
        0x00C7,    // LATIN CAPITAL LETTER C WITH CEDILLA
        0x015E,    // LATIN CAPITAL LETTER S WITH CEDILLA
        0x00DF,    // LATIN SMALL LETTER SHARP S (German)
        0x00A1,    // INVERTED EXCLAMATION MARK
        0x0132,    // LATIN CAPITAL LIGATURE IJ
        /* 0x90 */
        0x00E2,    // LATIN SMALL LETTER A WITH CIRCUMFLEX
        0x00E4,    // LATIN SMALL LETTER A WITH DIAERESIS
        0x00EA,    // LATIN SMALL LETTER E WITH CIRCUMFLEX
        0x00EB,    // LATIN SMALL LETTER E WITH DIAERESIS
        0x00EE,    // LATIN SMALL LETTER I WITH CIRCUMFLEX
        0x00EF,    // LATIN SMALL LETTER I WITH DIAERESIS
        0x00F4,    // LATIN SMALL LETTER O WITH CIRCUMFLEX
        0x00F6,    // LATIN SMALL LETTER O WITH DIAERESIS
        0x00FB,    // LATIN SMALL LETTER U WITH CIRCUMFLEX
        0x00FC,    // LATIN SMALL LETTER U WITH DIAERESIS
        0x00F1,    // LATIN SMALL LETTER N WITH TILDE
        0x00E7,    // LATIN SMALL LETTER C WITH CEDILLA
        0x015F,    // LATIN SMALL LETTER S WITH CEDILLA
        0x011F,    // LATIN SMALL LETTER G WITH BREVE
        0x0131,    // LATIN SMALL LETTER DOTLESS I
        0x0133,    // LATIN SMALL LIGATURE IJ
        /* 0xA0 */
        0x00AA,    // FEMININE ORDINAL INDICATOR
        0x03B1,    // GREEK SMALL LETTER ALPHA
        0x00A9,    // COPYRIGHT SIGN
        0x2030,    // PER THOUSAND SIGN
        0x011E,    // LATIN CAPITAL LETTER G WITH BREVE
        0x011B,    // LATIN SMALL LETTER E WITH CARON
        0x0148,    // LATIN SMALL LETTER N WITH CARON
        0x0151,    // LATIN SMALL LETTER O WITH DOUBLE ACUTE
        0x03C0,    // GREEK SMALL LETTER PI
        0x20AC,    // EURO SIGN
        0x00A3,    // POUND SIGN
        0x0024,    // DOALLAR SIGN
        0x2190,    // LEFTWARDS ARROW
        0x2191,    // UPWARDS ARROW
        0x2192,    // RIGHTWARDS ARROW
        0x2193,    // DOWNWARDS ARROW
        /* 0xB0 */
        0x00BA,    // MASCULIN ORDINAL INDICATOR
        0x00B9,    // SUPERSCRIPT ONE
        0x00B2,    // SUPERSCRIPT TWO
        0x00B3,    // SUPERSCRIPT THREE
        0x00B1,    // PLUS-MINUS SIGN
        0x0130,    // LATIN CAPITAL LETTER I WITH DOT ABOVE
        0x0144,    // LATIN SMALL LETTER N WITH ACUTE
        0x0171,    // LATIN SMALL LETTER U WITH DOUBLE ACUTE
        0x00B5,    // MICRO SIGN
        0x00BF,    // INVERTED QUESTION MARK
        0x00F7,    // DIVISION SIGN
        0x00B0,    // DEGREE SIGN
        0x00BC,    // VULGAR FRACTION ONE QUARTER
        0x00BD,    // VULGAR FRACTION ONE HALF
        0x00BE,    // VULGAR FRACTION THREE QUARTERS
        0x00A7,    // SECTION SIGN
        /* 0xC0 */
        0x00C1,    // LATIN CAPITAL LETTER A WITH ACUTE
        0x00C0,    // LATIN CAPITAL LETTER A WITH GRAVE
        0x00C9,    // LATIN CAPITAL LETTER E WITH ACUTE
        0x00C8,    // LATIN CAPITAL LETTER E WITH GRAVE
        0x00CD,    // LATIN CAPITAL LETTER I WITH ACUTE
        0x00CC,    // LATIN CAPITAL LETTER I WITH GRAVE
        0x00D3,    // LATIN CAPITAL LETTER O WITH ACUTE
        0x00D2,    // LATIN CAPITAL LETTER O WITH GRAVE
        0x00DA,    // LATIN CAPITAL LETTER U WITH ACUTE
        0x00D9,    // LATIN CAPITAL LETTER U WITH GRAVE
        0x0158,    // LATIN CAPITAL LETTER R WITH CARON
        0x010C,    // LATIN CAPITAL LETTER C WITH CARON
        0x0160,    // LATIN CAPITAL LETTER S WITH CARON
        0x017D,    // LATIN CAPITAL LETTER Z WITH CARON
        0x0110,    // LATIN CAPITAL LETTER D WITH STROKE
        0x013F,    // LATIN CAPITAL LETTER L WITH MIDDLE DOT
        /* 0xD0 */
        0x00C2,    // LATIN CAPITAL LETTER A WITH CIRCUMFLEX
        0x00C4,    // LATIN CAPITAL LETTER A WITH DIAERESIS
        0x00CA,    // LATIN CAPITAL LETTER E WITH CIRCUMFLEX
        0x00CB,    // LATIN CAPITAL LETTER A WITH DIAERESIS
        0x00CE,    // LATIN CAPITAL LETTER I WITH CIRCUMFLEX
        0x00CF,    // LATIN CAPITAL LETTER I WITH DIAERESIS
        0x00D4,    // LATIN CAPITAL LETTER O WITH CIRCUMFLEX
        0x00D6,    // LATIN CAPITAL LETTER O WITH DIAERESIS
        0x00DB,    // LATIN CAPITAL LETTER O WITH STROKE
        0x00DC,    // LATIN CAPITAL LETTER U WITH DIAERESIS
        0x0159,    // LATIN SMALL LETTER R WITH CARON
        0x010D,    // LATIN SMALL LETTER C WITH CARON
        0x0161,    // LATIN SMALL LETTER S WITH CARON
        0x017E,    // LATIN SMALL LETTER Z WITH CARON
        0x0111,    // LATIN SMALL LETTER D WITH STROKE
        0x0140,    // LATIN SMALL LETTER L WITH MIDDLE DOT
        /* 0xE0 */
        0x00C3,    // LATIN CAPITAL LETTER A WITH TILDE
        0x00C5,    // LATIN CAPITAL LETTER A WITH RING ABOVE
        0x00C6,    // LATIN CAPITAL LETTER AE = LATIN LIGATURE AE
        0x0152,    // LATIN CAPITAL LIGATURE OE
        0x0177,    // LATIN SMALL LETTER Y WITH CIRCUMFLEX
        0x00DD,    // LATIN CAPITAL LETTER Y WITH ACUTE
        0x00D5,    // LATIN CAPITAL LETTER O WITH TILDE
        0x00D8,    // LATIN CAPITAL LETTER O WITH STROKE
        0x00DE,    // LATIN CAPITAL LETTER THORN
        0x014A,    // LATIN CAPITAL LETTER ENG
        0x0154,    // LATIN CAPITAL LETTER R WITH ACUTE
        0x0106,    // LATIN SMAL LETTER C WITH ACUTE
        0x015A,    // LATIN CAPITAL LETTER S WITH ACUTE
        0x0179,    // LATIN CAPITAL LETTER Z WITH ACUTE
        0x0166,    // LATIN CAPITAL LETTER Z WITH STROKE
        0x00F0,    // LATIN SMALL LETTER ETH
        /* 0xF0 */
        0x00E3,    // LATIN SMALL LETTER A WITH TILDE
        0x00E5,    // LATIN SMALL LETTER A WITH RING
        0x00E6,    // LATIN SMALL LETTER AE
        0x0153,    // LATIN SMALL LETTER OE
        0x0175,    // LATIN SMALL LETTER W WITH CIRCUMFLEX
        0x00FD,    // LATIN SMALL LETTER Y WITH ACUTE
        0x00F5,    // LATIN SMALL LETTER O WITH TILDE
        0x00F8,    // LATIN SMALL LETTER O WITH STROKE
        0x00FE,    // LATIN SMALL LETTER THORN
        0x014B,    // LATIN SMALL LETTER ENG
        0x0155,    // LATIN SMALL LETTER R WITH ACUTE
        0x0107,    // LATIN SMALL LETTER C WITH ACUTE
        0x015B,    // LATIN SMALL LETTER S WITH ACUTE
        0x017A,    // LATIN SMALL LETTER Z WITH ACUTE
        0x0167,    // LATIN SMALL LETTER Z WITH STROKE
        0x00FF     // NOT ASSIGNED
    }
};

/*
 * @brief Extended Character Set
 *
 * The extended character set shall only be used with enhanced RadioText (eRT).
 */
// this is UCS-2

}
