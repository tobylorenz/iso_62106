/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/* AF codes */
/* 0: Not to be used */
/* 1..204: Carrier frequency */
/* 205: Filler code */
/* 206..223: Not assigned */
/* 224..249: 0..25 AFs follow */
/* 250: An LF/MF frequency follows */
/* 251..255: Not assigned */

/* ITU regions */
/* 1=mostly europe, middle east: LF/MF code table with 9 kHz spacing */
/* 2=mostly america, pacific: MF code table with 10 kHz spacing */
/* 3=mostly asia, oceania: LF/MF code table with 9 kHz spacing */

#include "AlternativeFrequencies.h"

#include <cassert>
#include <cstdint>

#include "RdsProgram.h"

namespace ISO62106 {

AlternativeFrequencies::MappedFrequency::MappedFrequency() :
    frequency(0),
    sameProgramme(false)
{
}

bool AlternativeFrequencies::MappedFrequency::operator==(const AlternativeFrequencies::MappedFrequency & rhs) const
{
    return
        (frequency == rhs.frequency) &&
        (sameProgramme == rhs.sameProgramme);
}

AlternativeFrequencies::AlternativeFrequencies(const RdsProgram & parent) :
    m_parent(parent),
    m_rx(),
    m_expectedAfCount(0),
    m_actualAfCount(0),
    m_bTuningFrequency(0),
    m_bValid(true),
    m_mappedFrequencies()
{
}

std::map<uint32_t, std::vector<AlternativeFrequencies::MappedFrequency>> AlternativeFrequencies::mappedFrequencies() const
{
    return m_mappedFrequencies;
}

void AlternativeFrequencies::decode(uint8_t af1, uint8_t af2)
{
    /* check */
    assert(af1 <= 0xff);
    assert(af2 <= 0xff);

    /* start of list */
    if ((af1 >= 224) && (af1 <= 249)) {
        /* process last receptions (if complete) */
        if (!m_rx.empty() &&
                (m_rx[0].first >= 224) &&
                (m_rx[0].first <= 249) &&
                (m_expectedAfCount == m_actualAfCount)) {
            if (m_bValid) {
                processMethodB();
            } else {
                processMethodA();
            }
        }

        /* clear for next list */
        m_rx.clear();
        m_expectedAfCount = af1 - 224;
        m_actualAfCount = 0;
        m_bTuningFrequency = af2;
        m_bValid = true;
    }

    /* push element (if not a duplicate) */
    Pair newPair = std::make_pair(af1, af2);
    if (m_rx.empty() || (m_rx.back() != newPair)) {
        m_rx.push_back(newPair);

        /* AF 1 */
        if ((af1 >= 1) && (af1 <= 204)) {
            m_actualAfCount++;
        }

        /* AF 2 */
        if ((af2 >= 1) && (af2 <= 204)) {
            m_actualAfCount++;
        }

        /* Method B */
        m_bValid &= (af1 == 250) || (af1 == m_bTuningFrequency) || (af2 == m_bTuningFrequency);
    }
}

uint32_t AlternativeFrequencies::frequencyKHz(uint8_t af, bool lfMf, uint8_t itu)
{
    uint32_t retVal = 0;
    if (lfMf) {
        if ((itu == 1) || (itu == 3)) {
            if ((af >= 1) && (af <= 15)) {
                /* 153 kHz .. 279 kHz */
                retVal = 153 + (af - 1) * 9;
            } else if ((af >= 16) && (af <= 135)) {
                /* 531 kHz .. 1602 kHz */
                retVal = 531 + (af - 16) * 9;
            }
        } else if (itu == 2) {
            if ((af >= 16) && (af <= 124)) {
                /* 530 kHz .. 1610 kHz */
                retVal = 530 + (af - 16) * 10;
            }
        }
    } else {
        if ((af >= 1) && (af <= 204)) {
            /* 87600 kHz .. 107900 kHz */
            retVal = 87600 + (af - 1) * 100;
        }
    }
    return retVal;
}

void AlternativeFrequencies::processMethodA()
{
    /* clear mapped frequencies first */
    std::vector<MappedFrequency> newMappedFrequencies;

    /* loop over all pairs */
    bool lfMfFrequency = false;
    for (auto & pair : m_rx) {
        for (uint8_t ab = 0; ab <= 1; ab++) {
            uint8_t af = (ab == 0) ? pair.first : pair.second;

            if ((af >= 1) && (af <= 204)) {
                /* frequency */
                MappedFrequency frequency;
                frequency.frequency = frequencyKHz(af, lfMfFrequency, m_parent.extendedCountryCode.ituRegion());
                frequency.sameProgramme = true;
                newMappedFrequencies.push_back(frequency);
                lfMfFrequency = false;
            } else if ((af >= 224) && (af <= 249)) {
                /* start of list */
            } else if (af == 250) {
                /* One LF/MF frequency follows */
                lfMfFrequency = true;
            }
        }
    }

    /* change check */
    if (m_mappedFrequencies[0] != newMappedFrequencies) {
        m_mappedFrequencies[0] = newMappedFrequencies;
        onChange.emit(m_parent.programmeIdentification.value(), 0);
    }
    onUpdate.emit(m_parent.programmeIdentification.value(), 0);
}

void AlternativeFrequencies::processMethodB()
{
    /* clear mapped frequncies first */
    uint32_t tuningFrequency = frequencyKHz(m_bTuningFrequency);
    std::vector<MappedFrequency> newMappedFrequencies;

    /* loop over all pairs */
    for (auto & pair : m_rx) {
        uint8_t af1 = pair.first;
        uint8_t af2 = pair.second;

        if ((af1 >= 224) && (af1 <= 249) && (af2 == m_bTuningFrequency)) {
            /* start of list */
        } else if ((af1 == 250) && (af2 >= 1) && (af2 <= 204)) {
            /* LF/MF frequency */
            /**
             * @note
             *   It's assumed here that a LF/MF frequency has the 250-indicator always at AF1.
             *   The tuning frequency is not contained in the pair, hence the field is 2 fields long.
             */
            MappedFrequency frequency;
            frequency.frequency = frequencyKHz(af2, true, m_parent.extendedCountryCode.ituRegion());
            frequency.sameProgramme = true;
            newMappedFrequencies.push_back(frequency);
        } else if (((af1 >= 1) && (af1 <= 204) && (af2 == m_bTuningFrequency)) ||
                   ((af1 == m_bTuningFrequency) && (af2 >= 1) && (af2 <= 204))) {
            /* VHF frequency */
            MappedFrequency frequency;
            frequency.frequency = frequencyKHz((af2 == m_bTuningFrequency) ? af1 : af2);
            frequency.sameProgramme = (af1 < af2);
            newMappedFrequencies.push_back(frequency);
        }
    }

    /* change check */
    if (m_mappedFrequencies[tuningFrequency] != newMappedFrequencies) {
        m_mappedFrequencies[tuningFrequency] = newMappedFrequencies;
        onChange.emit(m_parent.programmeIdentification.value(), tuningFrequency);
    }
    onUpdate.emit(m_parent.programmeIdentification.value(), tuningFrequency);
}

sigc::signal<void, uint16_t, uint32_t> AlternativeFrequencies::onChange;

sigc::signal<void, uint16_t, uint32_t> AlternativeFrequencies::onUpdate;

}
