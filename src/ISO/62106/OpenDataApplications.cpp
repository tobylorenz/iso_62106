/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "OpenDataApplications.h"

#include <cassert>
#include <iomanip>
#include <sstream>

#include "RdsProgram.h"

namespace ISO62106 {

OpenDataApplications::OpenDataApplications(RdsProgram & parent) :
    m_parent(parent),
    m_value()
{
}

std::array<std::array<uint16_t, 2>, 16> OpenDataApplications::value() const
{
    return m_value;
}

void OpenDataApplications::decodeIdent(uint8_t agtc, uint8_t agtv, uint16_t msg, uint16_t aid)
{
    /* check */
    assert(agtc <= 0xf);
    assert(agtv <= 0x1);
    assert(msg <= 0xffff);
    assert(aid <= 0xffff);

    /* not carried in associated group */
    if ((agtc == 0) && (agtv == 0)) {
        return;
    }

    /* temporary data fault (encoder status) */
    if ((agtc == 15) && (agtv == 1)) {
        return;
    }

    /* check change */
    if (m_value[agtc][agtv] != aid) {
        onChange.emit(m_parent.programmeIdentification.value(), agtc, agtv, aid);

        m_value[agtc][agtv] = aid;
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value(), agtc, agtv, aid);

    /* process data */
    onDecodeIdent.emit(m_parent.programmeIdentification.value(), aid, msg);
}

void OpenDataApplications::decodeA(uint8_t gtc, uint8_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(gtc <= 0xf);
    assert(blk2 <= 0x1f);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* get aid */
    uint16_t aid = m_value[gtc][0];
    if (!aid) {
        decodeIssue.emit("ODA: AID=0 in A");
        return;
    }

    /* process data */
    onDecodeA.emit(m_parent.programmeIdentification.value(), aid, blk2, blk3, blk4);
}

void OpenDataApplications::decodeB(uint8_t gtc, uint8_t blk2, uint16_t blk4)
{
    /* check */
    assert(gtc <= 0xf);
    assert(blk2 <= 0x1f);
    assert(blk4 <= 0xffff);

    /* get aid */
    uint16_t aid = m_value[gtc][1];
    if (!aid) {
        decodeIssue.emit("ODA: AID=0 in B");
        return;
    }

    /* process data */
    onDecodeB.emit(m_parent.programmeIdentification.value(), aid, blk2, blk4);
}

sigc::signal<void, uint16_t, uint16_t, uint16_t> OpenDataApplications::onDecodeIdent;

sigc::signal<void, uint16_t, uint16_t, uint8_t, uint16_t, uint16_t> OpenDataApplications::onDecodeA;

sigc::signal<void, uint16_t, uint16_t, uint8_t, uint16_t> OpenDataApplications::onDecodeB;

sigc::signal<void, uint16_t, uint8_t, uint8_t, uint16_t> OpenDataApplications::onChange;

sigc::signal<void, uint16_t, uint8_t, uint8_t, uint16_t> OpenDataApplications::onUpdate;

}
