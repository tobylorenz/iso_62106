/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Transparent Data Channel (TDC) */
class ISO_62106_EXPORT TransparentDataChannels
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit TransparentDataChannels(const RdsProgram & parent);

    /**
     * This signal gets triggered on reception.
     *
     * Parameters:
     *   - address-code
     *   - data
     */
    static sigc::signal<void, uint16_t, uint8_t, uint16_t> onUpdate;

    /**
     * @brief Handles received TDC data
     *
     * This function handles received TDC data.
     *
     * @param[in] addr address-code
     * @param[in] data data
     */
    void decode(uint8_t addr, uint16_t data);

private:
    /** link to program */
    const RdsProgram & m_parent;
};

}
