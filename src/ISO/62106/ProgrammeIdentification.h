/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Programme Identification (PI) */
class ISO_62106_EXPORT ProgrammeIdentification
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ProgrammeIdentification(const RdsProgram & parent);

    /**
     * Programme Identification (PI) code
     *
     * @return PI code
     */
    uint16_t value() const;

    /**
     * @brief Returns country code portion of PI code.
     *
     * This function returns the country code portion of the PI code.
     *
     * @return Country code
     */
    uint8_t countryCode() const;

    /**
      * @brief Decodes and handles received PI codes
      *
      * This function decodes and handles received PI codes.
      *
      * @param[in] pi PI Code
      */
    void decode(uint16_t pi);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Programme Identification (PI) code */
    uint16_t m_value : 16;
};

}
