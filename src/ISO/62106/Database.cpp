/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Database.h"

#include <string>

#include "RdsProgram.h"

namespace ISO62106 {

Database::Database() :
    m_handle(nullptr)
{
    (void) open();
}

Database::~Database()
{
    close();
}

bool Database::open(const std::string language)
{
    /* remember old database */
    sqlite3 * oldHandle = m_handle;

    /* try to open new database */
    sqlite3 * newHandle;
    std::string filename = std::string(DATABASEDIR) + "/" + language + ".db";
    bool ok = (sqlite3_open(filename.c_str(), &newHandle) == SQLITE_OK);
    if (ok) {
        m_handle = newHandle;
    }

    /* try to open default database */
    if (!ok) {
        sqlite3 * newHandle;
        std::string filename = std::string(DATABASEDIR) + "/en_CEN.db";
        ok = (sqlite3_open(filename.c_str(), &newHandle) == SQLITE_OK);
        if (ok) {
            m_handle = newHandle;
        }
    }

    /* close old database */
    if (ok && oldHandle) {
        (void) sqlite3_close(oldHandle);
    }

    /* open database */
    return ok;
}

void Database::close()
{
    /* close database */
    if (m_handle) {
        (void) sqlite3_close(m_handle);
        m_handle = nullptr;
    }
}

sqlite3 * Database::handle() const
{
    return m_handle;
}

Database & database()
{
    /* singleton */
    static Database database;
    return database;
}

}
