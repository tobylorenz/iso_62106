/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EnhancedRadioText.h"

#include <algorithm>
#include <cassert>
#include <codecvt>
#include <cstring>
#include <locale>

#include "RdsProgram.h" // for decodeIssue

namespace ISO62106 {

EnhancedRadioText::EnhancedRadioText() :
    m_utf8(false),
    m_defaultTextFormattingDirection(0),
    m_characterTableID(0),
    m_value()
{
}

bool EnhancedRadioText::utf8() const
{
    return m_utf8;
}

uint8_t EnhancedRadioText::defaultTextFormattingDirection() const
{
    return m_defaultTextFormattingDirection;
}

uint8_t EnhancedRadioText::characterTableID() const
{
    return m_characterTableID;
}

std::u16string EnhancedRadioText::valueUcs2() const
{
    std::u16string retVal;

    if (m_utf8) {
        std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
        std::wstring wstring = converter.from_bytes(m_value.begin(), m_value.end());

        /* convert wstring to u16string */
        for (wchar_t wc : wstring) {
            if (wc == 0) {
                break;
            }
            retVal.push_back(static_cast<char16_t>(wc));
        }
    } else {
        for (uint8_t i = 0; i < m_value.size(); ++i) {
            /* Convert char to char16_t taking Little/Big Endian into account */
            char16_t c =
                (static_cast<char16_t>(m_value[2 * i] << 8)) |
                (static_cast<char16_t>(m_value[2 * i + 1]));
            if (c == 0) {
                break;
            }
            retVal.push_back(c);
        }
    }

    /* null termination */
    retVal.push_back(0);

    return retVal;
}

std::string EnhancedRadioText::valueUtf8() const
{
    std::string retVal;

    if (m_utf8) {
        for (char c : m_value) {
            if (c == 0) {
                break;
            }
            retVal.push_back(c);
        }
    } else {
        std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter;
        retVal = converter.to_bytes(valueUcs2());

        /* resize UTF8 */
        retVal.resize(strnlen(retVal.data(), retVal.size()));
    }

    return retVal;
}

void EnhancedRadioText::decodeA(uint16_t pi, uint16_t aid, uint8_t ac, uint16_t y, uint16_t z)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(ac <= 0x1f);
    assert(y <= 0xffff);
    assert(z <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    /* process characters */
    bool change = false;
    if (enhancedRadioText(pi).m_value[4 * ac + 0] != (y >> 8)) {
        enhancedRadioText(pi).m_value[4 * ac + 0] = static_cast<char>(y >> 8);
        change = true;
    }
    if (enhancedRadioText(pi).m_value[4 * ac + 1] != (y & 0xff)) {
        enhancedRadioText(pi).m_value[4 * ac + 1] = static_cast<char>(y & 0xff);
        change = true;
    }
    if (enhancedRadioText(pi).m_value[4 * ac + 2] != (z >> 8)) {
        enhancedRadioText(pi).m_value[4 * ac + 2] = static_cast<char>(z >> 8);
        change = true;
    }
    if (enhancedRadioText(pi).m_value[4 * ac + 3] != (z & 0xff)) {
        enhancedRadioText(pi).m_value[4 * ac + 3] = static_cast<char>(z & 0xff);
        change = true;
    }

    /* convert if something changed */
    if (change) {
        /* call event handler */
        onChange.emit(pi);
    }

    /* call event handler */
    onUpdate.emit(pi);
}

void EnhancedRadioText::decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(msg <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    /* assign */
    uint16_t rfu = (msg >> 6) & 0x3ff; /* reserved for future use */
    if (rfu != 0) {
        decodeIssue.emit("ODA-ERT: reserved for future use");
    }
    enhancedRadioText(pi).m_characterTableID = (msg >> 2) & 0xf;
    enhancedRadioText(pi).m_defaultTextFormattingDirection = (msg >> 1) & 1;
    enhancedRadioText(pi).m_utf8 = msg & 1;
}

sigc::signal<void, uint16_t> EnhancedRadioText::onChange;

sigc::signal<void, uint16_t> EnhancedRadioText::onUpdate;

void EnhancedRadioText::registerHandler()
{
    /* register ODA */
    OpenDataApplications::onDecodeIdent.connect(sigc::ptr_fun(decodeIdent));
    OpenDataApplications::onDecodeA.connect(sigc::ptr_fun(decodeA));
}

std::map<uint16_t, EnhancedRadioText> & enhancedRadioText()
{
    /* singleton */
    static std::map<uint16_t, EnhancedRadioText> list;
    return list;
}

EnhancedRadioText & enhancedRadioText(uint16_t pi)
{
    return enhancedRadioText()[pi];
}

}
