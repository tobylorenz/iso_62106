/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Programme Type (PTY) code */
class ISO_62106_EXPORT ProgrammeType
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ProgrammeType(const RdsProgram & parent);

    /**
     * Programme Type (PTY) code
     *
     * @return value
     */
    uint8_t value() const;

    /**
     * @brief Return PTY term
     *
     * This function returns the PTY term.
     *
     * @param[in] displayLength Display length (<=8, <=16 or more)
     * @param[in] rbds Selector RDS=0 (othen then ITU region 2), RBDS=1 (ITU region 2)
     * @return PTY term
     */
    std::string term(size_t displayLength = 100, uint8_t rbds = 0);

    /**
     * @brief Decodes and handles received PTY codes
     *
     * This function decodes and handles received PTY codes.
     *
     * @param[in] pty Programme Type code
     */
    void decode(uint8_t pty);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Programme Type (PTY) code */
    uint8_t m_value : 5;
};

}
