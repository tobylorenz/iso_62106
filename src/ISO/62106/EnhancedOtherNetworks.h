/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <map>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Enhanced Other Networks (EON) */
class ISO_62106_EXPORT EnhancedOtherNetworks
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit EnhancedOtherNetworks(const RdsProgram & parent);

    /** Linkage Information (LI) */
    struct LinkageInformation {
        LinkageInformation();

        /** Linkage Actuator (LA) */
        bool linkageActuator : 1;

        /** Extended Generic indicator (EG) */
        bool extendedGeneric : 1;

        /** International Linkage Set indicator (ILS) */
        bool internationalLinkageSet : 1;

        /** Linkage Set Number (LSN) or Country Identifier (CI) and Linkage Identifier (LI) */
        uint16_t linkageSetNumber : 12;
    };

    /** Mapped Frequencies (MF) type */
    struct MappedFrequencies {
        MappedFrequencies();

        /** Tuning Network frequency (TN) */
        uint8_t tuningNetwork : 8;

        /** Mapped FM/AM frequency (MF) of Other Network (ON) */
        uint8_t mappedFrequency : 8;
    };

    /**
     * Linkage Actuator
     *
     * @return linkage actuator
     */
    bool linkageActuator() const;

    /**
     * EON LIs (PI to EON LI)
     *
     * @return linkage informations
     */
    std::map<uint16_t, LinkageInformation> linkageInformations() const;

    /**
     * Mapped Frequencies (MF). 0..3: FM, 4: AM
     *
     * @return mapped frequencies
     */
    std::array<MappedFrequencies, 5> mappedFrequencies() const;

    /**
     * EON LI reception counter (PI to EON LI count)
     *
     * @return LI counts
     */
    std::map<uint16_t, uint8_t> liCnts() const;

    /**
     * @brief Decodes and handles PI on other network
     *
     * This function decodes and handles PI on other network.
     *
     * @param[in] pi PI code
     */
    void decodePi(uint16_t pi);

    /**
     * @brief Decodes and handles LA on tuned network.
     *
     * This function decodes and handles LA on tuned network.
     *
     * @param[in] la Linkage Actuator
     */
    void decodeLa(uint8_t la);

    /**
     * @brief Handles Linkage Information
     *
     * This function decodes and handles received Linkage Information.
     *
     * @param[in,out] rs EON RDS program
     * @param[in] la Linkage Actuator
     * @param[in] eg Extended Generic indicator
     * @param[in] ils International Linkage Set indicator
     * @param[in] lsn Linkage Set Number
     */
    void decodeLi(RdsProgram * rs, bool la, bool eg, bool ils, uint16_t lsn);

    /**
     * @brief Decodes and handles received EON message
     *
     * This function decodes and handles received EON messages.
     *
     * @param[in,out] rs EON RDS program
     * @param[in] vc Variant Code
     * @param[in] i Information
     */
    void decode(RdsProgram * rs, uint8_t vc, uint16_t i);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Linkage Actuator */
    bool m_linkageActuator : 1;

    /** EON LIs (PI to EON LI) */
    std::map<uint16_t, LinkageInformation> m_linkageInformations;

    /** Mapped Frequencies (MF). 0..3: FM, 4: AM */
    std::array<MappedFrequencies, 5> m_mappedFrequencies;

    /** EON LI reception counter (PI to EON LI count) */
    std::map<uint16_t, uint8_t> m_liCnts;
};

}
