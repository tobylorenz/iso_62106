/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Emergency Warning System (EWS) */
class ISO_62106_EXPORT EmergencyWarningSystem
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit EmergencyWarningSystem(const RdsProgram & parent);

    /** event handler on reception of identification */
    static sigc::signal<void, uint16_t> onUpdateIdent;

    /** event handler on reception */
    static sigc::signal<void, uint16_t, uint8_t, uint16_t, uint16_t> onUpdate;

    /**
     * @brief Decodes and handles received EWS message
     *
     * This function decodes and handles received EWS messages.
     *
     * @param[in] id Identification of EWS channel
     */
    void decodeIdent(uint16_t id);

    /**
     * @brief Process EWS type A message
     *
     * This function processes EWS type A messages.
     *
     * @param[in] blk2 Message bits in blk 2
     * @param[in] blk3 Message bits in blk 3
     * @param[in] blk4 Message bits in blk 4
     */
    void decode(uint8_t blk2, uint16_t blk3, uint16_t blk4);

private:
    /** link to program */
    const RdsProgram & m_parent;
};

}
