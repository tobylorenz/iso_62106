/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Programm Service (PS) name */
class ISO_62106_EXPORT ProgrammService
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ProgrammService(const RdsProgram & parent);

    /**
     * Return the value UCS-2 encoded.
     *
     * @return value UCS-2 encoded
     */
    std::u16string valueUcs2() const;

    /**
     * Return the value UTF-8 encoded.
     *
     * @return value UTF-8 encoded
     */
    std::string valueUtf8() const;

    /**
     * @brief Decodes and handles received PS characters
     *
     * This function decodes and handles received PS characters.
     *
     * @param[in] sa PS name Segment Address
     * @param[in] c1 Text Character
     * @param[in] c2 Text Character
     */
    void decode(uint8_t sa, uint8_t c1, uint8_t c2);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Programme Service (PS) name */
    std::array<char16_t, 8> m_value; // encoding: UCS-2
};

}
