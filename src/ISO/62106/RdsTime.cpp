/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RdsTime.h"

#include <cassert>

namespace ISO62106 {

RdsTime::RdsTime() :
    m_modifiedJulianDay(0),
    m_hour(0),
    m_minute(0),
    m_signLocalTimeOffset(0),
    m_localTimeOffset(0)
{
}

RdsTime::RdsTime(uint32_t modifiedJulianDay) :
    m_modifiedJulianDay(modifiedJulianDay),
    m_hour(0),
    m_minute(0),
    m_signLocalTimeOffset(0),
    m_localTimeOffset(0)
{
    /* check */
    assert((value >= 15079) && (value <= 88127)); // 1900-03-01 .. 2100-02-28
}

RdsTime::RdsTime(uint8_t hour, uint8_t minute) :
    m_modifiedJulianDay(0),
    m_hour(hour),
    m_minute(minute),
    m_signLocalTimeOffset(0),
    m_localTimeOffset(0)
{
    /* check */
    assert(hour <= 23);
    assert(minute <= 59);
}

RdsTime::RdsTime(uint16_t year, uint8_t month, uint8_t day) :
    m_modifiedJulianDay(static_cast<uint32_t>(ymd2mjd(year, month, day))),
    m_hour(0),
    m_minute(0),
    m_signLocalTimeOffset(0),
    m_localTimeOffset(0)
{
    /* check */
    assert(year <= 200);
    assert((month >= 1) && (month <= 12));
    assert((day >= 1) && (day <= 31));
}

bool RdsTime::valid() const
{
    return (m_modifiedJulianDay > 0);
}

uint32_t RdsTime::modifiedJulianDay() const
{
    return m_modifiedJulianDay;
}

void RdsTime::setModifiedJulianDay(uint32_t value)
{
    /* check */
    assert((value >= 15079) && (value <= 88127)); // 1900-03-01 .. 2100-02-28

    /* set mjd */
    this->m_modifiedJulianDay = value;
}

uint16_t RdsTime::year() const
{
    /* convert mjd to year/month/day */
    int y; // Year from 1900 (e.g. for 2003, y=103)
    int m; // Month from January (=1) to December (=12)
    int d; // Day of month from 1 to 31
    mjd2ymd(static_cast<int>(m_modifiedJulianDay), y, m, d);
    return static_cast<uint16_t>(y);
}

uint8_t RdsTime::month() const
{
    /* convert mjd to year/month/day */
    int y; // Year from 1900 (e.g. for 2003, y=103)
    int m; // Month from January (=1) to December (=12)
    int d; // Day of month from 1 to 31
    mjd2ymd(static_cast<int>(m_modifiedJulianDay), y, m, d);
    return static_cast<uint8_t>(m);
}

uint8_t RdsTime::day() const
{
    /* convert mjd to year/month/day */
    int y; // Year from 1900 (e.g. for 2003, y=103)
    int m; // Month from January (=1) to December (=12)
    int d; // Day of month from 1 to 31
    mjd2ymd(static_cast<int>(m_modifiedJulianDay), y, m, d);
    return static_cast<uint8_t>(d);
}

void RdsTime::setDate(uint16_t year, uint8_t month, uint8_t day)
{
    /* check */
    assert(year <= 200);
    assert((month >= 1) && (month <= 12));
    assert((day >= 1) && (day <= 31));

    /* set */
    m_modifiedJulianDay = static_cast<uint32_t>(ymd2mjd(year, month, day));
}

uint8_t RdsTime::weekNumber() const
{
    /* convert mjd to wy/wn */
    int iwy; // int year
    int iwn; // int calendar week
    mjd2wywn(static_cast<int>(m_modifiedJulianDay), iwy, iwn);
    return static_cast<uint8_t>(iwn);
}

uint8_t RdsTime::weekDay() const
{
    /* convert mjd to wd */
    int iwd = mjd2wd(static_cast<int>(m_modifiedJulianDay));
    return static_cast<uint8_t>(iwd);
}

uint8_t RdsTime::hour() const
{
    return m_hour;
}

void RdsTime::setHour(uint8_t value)
{
    /* check */
    assert(value <= 23);

    /* set */
    m_hour = value;
}

uint8_t RdsTime::minute() const
{
    return m_minute;
}

void RdsTime::setMinute(uint8_t value)
{
    /* check */
    assert(value <= 59);

    /* set */
    m_minute = value;
}

void RdsTime::setTime(uint8_t hour, uint8_t minute)
{
    /* check */
    assert(hour <= 23);
    assert(minute <= 59);

    /* set */
    m_hour = hour;
    m_minute = minute;
}

bool RdsTime::signLocalTimeOffset() const
{
    return m_signLocalTimeOffset;
}

void RdsTime::setSignLocalTimeOffset(bool value)
{
    /* check */
//    assert(value <= 1);

    /* set */
    m_signLocalTimeOffset = value;
}

uint8_t RdsTime::localTimeOffset() const
{
    return m_localTimeOffset;
}

void RdsTime::setLocalTimeOffset(uint8_t value)
{
    /* check */
    assert(value <= 31);

    /* set */
    m_localTimeOffset = value;
}

RdsTime & RdsTime::addTime(uint8_t days, uint8_t hours, uint8_t minutes)
{
    /* check */
    assert(days <= 31);
    assert(hours <= 23);
    assert(minutes <= 59);

    /* new values */
    uint32_t newModifiedJulianDay = m_modifiedJulianDay + days;
    uint8_t newHour = m_hour + hours;
    uint8_t newMinute = m_minute + minutes;

    /* minutes overflow. increase hours. */
    newHour += (newMinute / 60);
    newMinute %= 60;

    /* hours overflow. increase date. */
    newModifiedJulianDay += (newHour / 24);
    newHour %= 24;

    /* set new values */
    m_modifiedJulianDay = newModifiedJulianDay;
    m_hour = newHour;
    m_minute = newMinute;

    return *this;
}

RdsTime & RdsTime::subTime(uint8_t days, uint8_t hours, uint8_t minutes)
{
    /* check */
    assert(days <= 31);
    assert(hours <= 23);
    assert(minutes <= 59);

    /* new values */
    int32_t newModifiedJulianDay = static_cast<int32_t>(m_modifiedJulianDay) - static_cast<int32_t>(days);
    int8_t newHour = static_cast<int8_t>(m_hour) - static_cast<int8_t>(hours);
    int8_t newMinute = static_cast<int8_t>(m_minute) - static_cast<int8_t>(minutes);

    /* minutes underflow. decrease hours. */
    while(newMinute < 0) {
        newHour--;
        newMinute += 60;
    }

    /* hours underflow. decrease date. */
    while(newHour < 0) {
        newModifiedJulianDay--;
        newHour += 24;
    }

    /* set new values */
    m_modifiedJulianDay = static_cast<uint32_t>(newModifiedJulianDay);
    m_hour = static_cast<uint8_t>(newHour);
    m_minute = static_cast<uint8_t>(newMinute);

    return *this;
}

RdsTime & RdsTime::toLocalTime()
{
    /* get local time offset as RdsTime */
    uint16_t minutes = m_localTimeOffset * 30;
    uint8_t hours = static_cast<uint8_t>(minutes / 60);
    minutes %= 60;

    /* add local time offset */
    if (m_signLocalTimeOffset) {
        subTime(0, hours, static_cast<uint8_t>(minutes));
    } else {
        addTime(0, hours, static_cast<uint8_t>(minutes));
    }

    return *this;
}

RdsTime & RdsTime::toUtcTime()
{
    /* get local time offset as RdsTime */
    uint16_t minutes = m_localTimeOffset * 30;
    uint8_t hours = static_cast<uint8_t>(minutes / 60);
    minutes %= 60;

    /* substract local time offset */
    if (m_signLocalTimeOffset) {
        addTime(0, hours, static_cast<uint8_t>(minutes));
    } else {
        subTime(0, hours, static_cast<uint8_t>(minutes));
    }

    return *this;
}

bool RdsTime::operator==(const RdsTime & rhs) const
{
    return
        (m_modifiedJulianDay == rhs.m_modifiedJulianDay) && // year, month, day
        (m_hour == rhs.m_hour) &&
        (m_minute == rhs.m_minute);
}

bool operator!=(const RdsTime & lhs, const RdsTime & rhs)
{
    return !(lhs == rhs);
}

bool RdsTime::operator<(const RdsTime & rhs) const
{
    if (m_modifiedJulianDay < rhs.m_modifiedJulianDay) { // year, month, day
        return true;
    }
    if (m_hour < rhs.m_hour) {
        return true;
    }
    if (m_minute < rhs.m_minute) {
        return true;
    }
    return false;
}

bool operator>(const RdsTime & lhs, const RdsTime & rhs)
{
    return rhs < lhs;
}

bool operator<=(const RdsTime & lhs, const RdsTime & rhs)
{
    return !(lhs > rhs);
}

bool operator>=(const RdsTime & lhs, const RdsTime & rhs)
{
    return !(lhs < rhs);
}

void RdsTime::mjd2ymd(int mjd, int & y, int & m, int & d)
{
    /* check */
    assert((mjd >= 15079) && (mjd <= 88127)); // 1900-03-01 .. 2100-02-28

    y = static_cast<int>((static_cast<double>(mjd) - 15078.2) / 365.25);
    m = static_cast<int>((static_cast<double>(mjd) - 14956.1 - static_cast<int>(static_cast<double>(y) * 365.25)) / 30.6001);
    d = static_cast<int>(mjd) - 14956 - static_cast<int>(static_cast<double>(y) * 365.25) - static_cast<int>(static_cast<double>(m) * 30.6001);

    int k = ((m == 14) || (m == 15)) ? 1 : 0;
    y = y + k;
    m = m - 1 - k * 12;
}

int RdsTime::ymd2mjd(int y, int m, int d)
{
    /* check */
    assert((y >= 0) && (y <= 200)); // requires 8 bits (16..9)
    assert((m >= 1) && (m <= 12)); // requires 4 bits (8..5)
    assert((d >= 1) && (d <= 31)); // requires 5 bits (4..0)

    int l = ((m == 1) || (m == 2)) ? 1 : 0;

    return 14956 + d + static_cast<int>((y - l) * 365.25) + static_cast<int>((m + 1 + l * 12) * 30.6001);
}

int RdsTime::mjd2wd(int mjd)
{
    /* check */
    assert((mjd >= 15079) && (mjd <= 88127)); // 1900-03-01 .. 2100-02-28

    return ((mjd + 2) % 7) + 1;
}

int RdsTime::wywnwd2mjd(int wy, int wn, int wd)
{
    /* check */
    assert((wy >= 0) && (wy <= 200)); // requires 8 bits (16..9)
    assert((wn >= 1) && (wn <= 53)); // requires 6 bits (8..3)
    assert((wd >= 1) && (wd <= 7)); // requires 3 bits (2..0)

    return 15012 + wd + 7 * (wn + static_cast<int>((static_cast<double>(wy) * 1461 / 28) + 0.41));
}

void RdsTime::mjd2wywn(int mjd, int & wy, int & wn)
{
    /* check */
    assert((mjd >= 15079) && (mjd <= 88127)); // 1900-03-01 .. 2100-02-28

    int w = static_cast<int>((static_cast<double>(mjd) / 7) - 2144.64);
    wy = static_cast<int>((static_cast<double>(w) * 28 / 1461) - 0.0079);
    wn = w - static_cast<int>((static_cast<double>(wy) * 1461 / 28) + 0.41);
}

}
