/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Open Data Application (ODA) */
class ISO_62106_EXPORT OpenDataApplications
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit OpenDataApplications(RdsProgram & parent);

    /**
     * Open Data Application (ODA) Assignment
     *   - Key: Application Group Type (AGT) Code+Version
     *   - Value: Application Identification (AID)
     *
     * @return assignments
     */
    std::array<std::array<uint16_t, 2>, 16> value() const;

    /**
     * @brief Decode and process ODA assignment message
     *
     * This function decodes and processes ODA assignment messages.
     *
     * @param[in] agtc Application Group Type Code
     * @param[in] agtv Application Group Type Version
     * @param[in] msg Message bits
     * @param[in] aid Application Identification
     */
    void decodeIdent(uint8_t agtc, uint8_t agtv, uint16_t msg, uint16_t aid);

    /**
     * decodeIdent signal
     *
     *   - PI code
     *   - Application Identification
     *   - Message
     */
    static sigc::signal<void, uint16_t, uint16_t, uint16_t> onDecodeIdent;

    /**
     * @brief Process ODA type A message
     *
     * This function processes ODA type A messages.
     *
     * @param[in] gtc Group Type Version
     * @param[in] blk2 Message bits in blk 2
     * @param[in] blk3 Message bits in blk 3
     * @param[in] blk4 Message bits in blk 4
     */
    void decodeA(uint8_t gtc, uint8_t blk2, uint16_t blk3, uint16_t blk4);

    /**
     * decodeA signal
     *
     *   - PI code
     *   - Application Identification
     *   - Message bits in blk2
     *   - Message bits in blk3
     *   - Message bits in blk4
     */
    static sigc::signal<void, uint16_t, uint16_t, uint8_t, uint16_t, uint16_t> onDecodeA;

    /**
     * @brief Process ODA type B message
     *
     * This function processes ODA type B messages.
     *
     * @param[in] gtc Group Type Version
     * @param[in] blk2 Message bits in blk 2
     * @param[in] blk4 Message bits in blk 4
     */
    void decodeB(uint8_t gtc, uint8_t blk2, uint16_t blk4);

    /**
     * decodeB signal
     *
     *   - PI code
     *   - Application Identification
     *   - Message bits in blk2
     *   - Message bits in blk4
     */
    static sigc::signal<void, uint16_t, uint16_t, uint8_t, uint16_t> onDecodeB;

    /** event handler on change */
    static sigc::signal<void, uint16_t, uint8_t, uint8_t, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t, uint8_t, uint8_t, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /**
     * Open Data Application (ODA) Assignment
     *   - Key: Application Group Type (AGT) Code+Version
     *   - Value: Application Identification (AID)
     */
    std::array<std::array<uint16_t, 2>, 16> m_value;
};

}
