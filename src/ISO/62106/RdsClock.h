/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <chrono>
#include <cstdint>
#include <thread>

#include <sigc++-2.0/sigc++/signal.h>

#include "RdsTime.h"

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/**
 * RDS Clock (free running clock)
 *
 * This implements a free running clock that gets synchronized on CT receptions.
 *
 * RDS standard 6.1.5.6 footnote d:
 * CT is transmitted within +/-0.1 s of every minute edge.
 *
 * RDS standard 6.1.5.6 footnote h:
 * Accurate CT shall ... where TMC and/or RP is implemented.
 *
 * RDS standard 7.2:
 * The CT is intended to update a free running clock in a receiver.
 * The listener will not use this information directly.
 * The conversion to local time and date will be made in the receiver.
 * CT is used as time stamp by various RDS applications and thus it shall be accurate.
 *
 * Implementation:
 * The onMinuteChange signal is emitted on every minute edge.
 * When the CT update is not received, the free running clock keeps counting and
 * the onMinuteChange signal is emitted with a timeout delay of 1 second after the expected update
 * and then every 60 seconds until a CT update is received again.
 */
class ISO_62106_EXPORT RdsClock
{
public:
    /**
     * Initializer
     *
     * @param[in] time Initial RDS time based on local time and date.
     */
    explicit RdsClock(RdsTime & time);

    /**
     * start thread
     */
    void startThread();

    /**
     * The current time.
     * It's either updated on CT update or by the free running clock, if the CT signal is not received.
     */
    RdsTime time;

    /**
     * This slot should be triggered on every minute edge.
     * Therefore it should be connected to Ct::onUpdate.
     *
     * @param[in] pi PI code
     */
    void ctUpdated(uint16_t pi);

    /** This signal gets trigegred on every minute edge */
    static sigc::signal<void, RdsTime> onMinuteChange;

    /**
     * This is the timeout at which ctUpdated is considered missing.
     * At this time the clockTimedOut function gets active.
     */
    std::chrono::time_point<std::chrono::system_clock> minuteChangeTimeout;

    /**
     * When clockTimedOut gets active, this is the time it will set.
     */
    RdsTime nextTime;

private:
    /**
     * Asychronous clock (timeout) thread.
     *
     * It wakes up once per minute and checks if the timeout has happened.
     */
    std::thread * m_clockThread;

    /**
     * This is the clock thread's main function.
     * It sleeps till a timeout occurs and will then set the new time and emit the signal.
     *
     * @param clock Reference to this class
     */
    [[ noreturn ]] static void clockTimedOut(RdsClock * clock);
};

}
