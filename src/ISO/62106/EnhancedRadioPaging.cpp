/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EnhancedRadioPaging.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

EnhancedRadioPaging::EnhancedRadioPaging(const RdsProgram & parent) :
    m_parent(parent),
    m_operatorCode(),
    m_pagingAreaCode(),
    m_currentCarrierFrequency(),
    m_cycleSelection(),
    m_interval(),
    m_sorting(),
    m_addressNotificationBits25(),
    m_addressNotificationBits50a(),
    m_addressNotificationBits50b()
{
}

uint8_t EnhancedRadioPaging::operatorCode() const
{
    return m_operatorCode;
}

uint8_t EnhancedRadioPaging::pagingAreaCode() const
{
    return m_pagingAreaCode;
}

uint8_t EnhancedRadioPaging::currentCarrierFrequency() const
{
    return m_currentCarrierFrequency;
}

uint8_t EnhancedRadioPaging::cycleSelection() const
{
    return m_cycleSelection;
}

uint8_t EnhancedRadioPaging::interval() const
{
    return m_interval;
}

uint8_t EnhancedRadioPaging::sorting() const
{
    return m_sorting;
}

uint32_t EnhancedRadioPaging::addressNotificationBits25() const
{
    return m_addressNotificationBits25;
}

uint32_t EnhancedRadioPaging::addressNotificationBits50a() const
{
    return m_addressNotificationBits50a;
}

uint32_t EnhancedRadioPaging::addressNotificationBits50b() const
{
    return m_addressNotificationBits50b;
}

void EnhancedRadioPaging::decode(uint8_t st, uint8_t cs, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(st <= 0x7);
    assert(cs <= 0x3);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* Cycle Selection */
    this->m_cycleSelection = cs;
    if (cs == 1) {
        decodeIssue.emit("ERP: CS=1: reserved for future use");
    }

    /* paging Interval numbering */
    m_interval = (blk3 >> 12) & 0xf;

    /* reserved for future use */
    if (((blk3 >> 9) & 0x1) != 0) {
        decodeIssue.emit("ERP: reserved for future use");
    }

    /* Message sorting */
    m_sorting = (blk3 >> 10) & 0x3;
    if (m_sorting == 1) {
        decodeIssue.emit("ERP: S=1: reserved for future use");
    }

    /* Evaluate sub type */
    switch (st) {
    case 0: /* address notification bits 24..0, when only 25 bits are used */
        m_addressNotificationBits25 = static_cast<uint32_t>(((blk3 & 0x1ff) << 16) | blk4);
        break;
    case 1: /* address notification bits 49..25, when 50 bits are used */
        m_addressNotificationBits50b = static_cast<uint32_t>(((blk3 & 0x1ff) << 16) | blk4);
        break;
    case 2: /* address notification bits 24..0, when 50 bits are used */
        m_addressNotificationBits50a = static_cast<uint32_t>(((blk3 & 0x1ff) << 16) | blk4);
        break;
    case 3: /* reserved for Value Added Services (VAS) system information */
        decodeIssue.emit("ERP: not implemented yet");
        break;
    default: /* 4..7: reserved for future use */
        decodeIssue.emit("ERP: reserved for future use");
        break;
    }
}

void EnhancedRadioPaging::decodeOpc(uint8_t opc)
{
    /* check */
    assert(opc <= 0xf);

    this->m_operatorCode = opc; /* OPerator Code */
}

void EnhancedRadioPaging::decodePac(uint8_t pac)
{
    /* check */
    assert(pac <= 0x3f);

    this->m_pagingAreaCode = pac; /* Paging Area Code */
}

void EnhancedRadioPaging::decodeCcf(uint8_t ccf)
{
    /* check */
    assert(ccf <= 0xff);

    this->m_currentCarrierFrequency = ccf; /* Current Carrier Frequency */
}

}
