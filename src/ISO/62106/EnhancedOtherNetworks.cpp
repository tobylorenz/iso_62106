/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EnhancedOtherNetworks.h"

#include <cassert>

#include "Database.h"
#include "RdsProgram.h"

namespace ISO62106 {

EnhancedOtherNetworks::LinkageInformation::LinkageInformation() :
    linkageActuator(false),
    extendedGeneric(false),
    internationalLinkageSet(false),
    linkageSetNumber(0)
{
}

EnhancedOtherNetworks::MappedFrequencies::MappedFrequencies() :
    tuningNetwork(0),
    mappedFrequency(0)
{
}

EnhancedOtherNetworks::EnhancedOtherNetworks(const RdsProgram & parent) :
    m_parent(parent),
    m_linkageActuator(false),
    m_linkageInformations(),
    m_mappedFrequencies(),
    m_liCnts()
{
}

bool EnhancedOtherNetworks::linkageActuator() const
{
    return m_linkageActuator;
}

std::map<uint16_t, EnhancedOtherNetworks::LinkageInformation> EnhancedOtherNetworks::linkageInformations() const
{
    return m_linkageInformations;
}

std::array<EnhancedOtherNetworks::MappedFrequencies, 5> EnhancedOtherNetworks::mappedFrequencies() const
{
    return m_mappedFrequencies;
}

std::map<uint16_t, uint8_t> EnhancedOtherNetworks::liCnts() const
{
    return m_liCnts;
}

void EnhancedOtherNetworks::decodePi(uint16_t pi)
{
    /* check */
    assert(pi <= 0xffff);

    /* increase the counter for this combination */
    m_liCnts[pi]++;

    /* check if counter has reached upper limit */
    if (m_liCnts[pi] >= 32) {
        for (auto & liCnt : m_liCnts) {
            /* divide by 2 */
            liCnt.second /= 2;

            /* clear entry if count has reached zero */
            if (liCnt.second == 0) {
                m_linkageInformations.erase(liCnt.first);
            }
        }
    }
}

void EnhancedOtherNetworks::decodeLa(uint8_t la)
{
    /* check */
    assert(la <= 0x1);

    /* set general linkage actuator */
    this->m_linkageActuator = la;

    /* if generic linkage actuator is cleared, also clear all others */
    if (la == 0) {
        for (auto & li : m_linkageInformations) {
            li.second.linkageActuator = 0;
        }
    }
}

void EnhancedOtherNetworks::decodeLi(RdsProgram * rs, bool la, bool eg, bool ils, uint16_t lsn)
{
    /* check */
//    assert(la <= 0x1);
//    assert(eg <= 0x1);
//    assert(ils <= 0x1);
    assert(lsn <= 0xfff);

    /* save information */
    m_linkageInformations[rs->programmeIdentification.value()].linkageActuator = la;
    m_linkageInformations[rs->programmeIdentification.value()].extendedGeneric = eg;
    m_linkageInformations[rs->programmeIdentification.value()].internationalLinkageSet = ils;
    m_linkageInformations[rs->programmeIdentification.value()].linkageSetNumber = lsn;

    /* activate general linkage actuator */
    if (la) {
        this->m_linkageActuator = la;
    } else {
        /* if all linkage actuators are cleared, also clear generic */
        uint8_t laCnt = 0;
        for (auto & li : m_linkageInformations) {
            laCnt += li.second.linkageActuator;
        }
        if (laCnt == 0) {
            this->m_linkageActuator = false;
        }
    }
}

void EnhancedOtherNetworks::decode(RdsProgram * rs, uint8_t vc, uint16_t i)
{
    /* check */
    assert(vc <= 0xf);
    assert(i <= 0xffff);

    /* variant code */
    switch (vc) { /* Variant Code */
    case 0:
    case 1:
    case 2:
    case 3: /* char. x, char. x+1 */
        rs->programmeService.decode(vc, i >> 8, i & 0xff);
        break;
    case 4: /* AF(ON), AF(ON)*/
        rs->alternativeFrequencies.decode(i >> 8, i & 0xff);
        break;
    case 5: /* Tuning freq. (TN), Mapped FM freq. 1 (ON) */
    case 6: /* Tuning freq. (TN), Mapped FM freq. 2 (ON) */
    case 7: /* Tuning freq. (TN), Mapped FM freq. 3 (ON) */
    case 8: /* Tuning freq. (TN), Mapped FM freq. 4 (ON) */
    case 9: /* Tuning freq. (TN), Mapped AM freq. (ON) */
        rs->enhancedOtherNetworks.m_mappedFrequencies[vc - 5].tuningNetwork = i >> 8;
        rs->enhancedOtherNetworks.m_mappedFrequencies[vc - 5].mappedFrequency = i & 0xff;
        break;
    case 10: /* unallocated */
        decodeIssue.emit("EON: VC10: unallocated");
        break;
    case 11: /* unallocated */
        decodeIssue.emit("EON: VC11: unallocated");
        break;
    case 12: /* Linkage information */
        decodeLi(rs,
                 (i >> 15) & 1,   /* linkage actuator */
                 (i >> 14) & 1,   /* extended generic indicator */
                 (i >> 12) & 1,   /* international linkage set (ILS) indicator */
                 (i & 0x0fff));   /* linkage set number (LSN) */
        if (((i >> 13) & 1) != 0) { /* reserved */
            decodeIssue.emit("EON: VC12: reserved");
        }
        break;
    case 13: /* PTY(ON), Reserved, TA(ON) */
        rs->programmeType.decode((i >> 11) & 0x1f);
        rs->trafficAnnouncement.decode(i & 1);
        break;
    case 14: /* PIN(ON) */
        rs->programmeItemNumber.decode((i >> 11) & 0x1f, (i >> 6) & 0x1f, (i >> 0) & 0x2f);
        break;
    case 15:/* Reserved for broadcasters use */
        decodeIssue.emit("EON: VC15: reserved for broadcasters use");
        break;
    }
}

}
