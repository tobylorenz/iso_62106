/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/**
 * RDS Time
 *
 * @note All times in UTC.
 * @note Formulas are applicable between the inclusive dates: 1900-03-01 till 2100-02-28.
 */
class ISO_62106_EXPORT RdsTime
{
public:
    /* constructors */
    explicit RdsTime();

    /**
     * Constructor for MJD
     *
     * @param[in] modifiedJulianDay Modified Julian Day (MJD)
     */
    explicit RdsTime(uint32_t modifiedJulianDay);

    /**
     * Constructor for Hour/Minute
     *
     * @param[in] hour Hour
     * @param[in] minute Minute
     */
    explicit RdsTime(uint8_t hour, uint8_t minute);

    /**
     * Constructor for Year/Month/Day
     *
     * @param[in] year Year
     * @param[in] month Month
     * @param[in] day Day
     */
    explicit RdsTime(uint16_t year, uint8_t month, uint8_t day);

    /* relational operators */
    bool operator==(const RdsTime & rhs) const;
    bool operator< (const RdsTime & rhs) const;

    /**
     * Get if time is valid.
     *
     * It essentially just checks if MJD=0, so if RdsTime was just default initialized.
     *
     * @return true if valid, else if invalid
     */
    bool valid() const;

    /**
     * Get Modified Julian Day (MJD).
     *
     * @return Modified Julian Day (MJD)
     */
    uint32_t modifiedJulianDay() const;

    /**
     * Set Modified Julian Day (MJD).
     *
     * @param[in] value Modified Julian Day (MJD)
     */
    void setModifiedJulianDay(uint32_t value);

    /**
     * Get year.
     *
     * @return Year since 1900 (0..200)
     */
    uint16_t year() const;

    /**
     * Get month.
     *
     * @return Month (1..12)
     */
    uint8_t month() const;

    /**
     * Get day.
     *
     * @return Day (1..31)
     */
    uint8_t day() const;

    /**
     * Set date.
     *
     * @param[in] year Year since 1900 (0..200)
     * @param[in] month Month (1..21)
     * @param[in] day Day (1..31)
     */
    void setDate(uint16_t year, uint8_t month, uint8_t day);

    /**
     * Get calendar week.
     *
     * @return Calendar week (ISO 8601, 1..53)
     */
    uint8_t weekNumber() const;

    /**
     * Get week day.
     *
     * @return Week Day (1=Mon, 7=Sun)
     */
    uint8_t weekDay() const;

    /**
     * Get hour.
     *
     * @return Hour (0..23)
     */
    uint8_t hour() const;

    /**
     * Set hour.
     *
     * @param[in] value Hour (0..23)
     */
    void setHour(uint8_t value);

    /**
     * Get minute.
     *
     * @return Minute (0..59)
     */
    uint8_t minute() const;

    /**
     * Set minute.
     *
     * @param[in] value Minute (0..59)
     */
    void setMinute(uint8_t value);

    /**
     * Set time.
     *
     * @param hour hour (0..23)
     * @param minute (0..59)
     */
    void setTime(uint8_t hour, uint8_t minute);

    /**
     * Get sign of local time offset.
     *
     * @return Sign of Local Time Offset (0=+, 1=-)
     */
    bool signLocalTimeOffset() const;

    /**
     * Set sign of local time offset.
     *
     * @param[in] value Sign of Local Time Offset (0=+, 1=-)
     */
    void setSignLocalTimeOffset(bool value);

    /**
     * Get local time offset.
     *
     * @return Local Time Offset (in 30 minutes)
     */
    uint8_t localTimeOffset() const; // 0..31 => 0..930 min => 0 .. 15.5 h

    /**
     * Set local time offset.
     *
     * @param[in] value Local Time Offset (in 30 minutes)
     */
    void setLocalTimeOffset(uint8_t value); // 0..31 => 0..930 min => 0 .. 15.5 h

    /**
     * Add days, hours and minutes.
     *
     * @param[in] days days to add
     * @param[in] hours hours to add
     * @param[in] minutes minutes to add
     * @return Reference to RdsTime
     */
    RdsTime & addTime(uint8_t days, uint8_t hours, uint8_t minutes);

    /**
     * Subtract days, hours and minutes.
     *
     * @param[in] days days to add
     * @param[in] hours hours to add
     * @param[in] minutes minutes to add
     * @return Reference to RdsTime
     */
    RdsTime & subTime(uint8_t days, uint8_t hours, uint8_t minutes);

    /**
     * Convert universal time coordinated (UTC) into local time.
     *
     * Sets hour and minute accordingly.
     * (Sign of) local time offset remains the same.
     *
     * @return local time
     */
    RdsTime & toLocalTime();

    /**
     * Convert local time (back) into universal time coordinated (UTC).
     *
     * Sets hour and minute accordingly.
     * (Sign of) local time offset remains the same.
     *
     * @return universal time coordinated (UTC)
     */
    RdsTime & toUtcTime();

    /**
     * @brief Conversion from Modified Julian Date to Year, Month, Day
     *
     * This function converts from Modified Julian Date to Year, Month and Day.
     *
     * @param[in] modifiedJulianDay Modified Julian Day
     * @param[out] year Year from 1900 (e.g. for 2003, Y=103)
     * @param[out] month Month from January (=1) to December (=12)
     * @param[out] day Day of month from 1 to 31
     */
    static void mjd2ymd(int modifiedJulianDay, int & year, int & month, int & day);

    /**
     * @brief Conversion from Year, Month, Day to Modified Julian Date
     *
     * This function converts from Year, Month and Day to Modified Julian Date.
     *
     * @param[in] year Year from 1900 (e.g. for 2003, Y=103)
     * @param[in] month Month from January (=1) to December (=12)
     * @param[in] day Day of month from 1 to 31
     * @return Modified Julian Day
     */
    static int ymd2mjd(int year, int month, int day);

    /**
     * @brief Conversion from Modified Julian Date to Week Day
     *
     * This function converts from Modified Julian Date to Week Day.
     *
     * @param[in] modifiedJulianDay Modified Julian Day
     * @return Day of week from Monday (=1) to Sunday (=7)
     */
    static int mjd2wd(int modifiedJulianDay);

    /**
     * @brief Conversion from Week Year, Number, Day to Modified Julian Date
     *
     * This function converts from Week Year, Number and Day to Modified Julian Date.
     *
     * @param[in] year "Week number" Year from 1900
     * @param[in] weekNumber Week number according to ISO 8601
     * @param[in] weekDay Day of week from Monday (=1) to Sunday (=7)
     * @return Modified Julian Day
     */
    static int wywnwd2mjd(int year, int weekNumber, int weekDay);

    /**
     * @brief Conversion from Modified Julian Date to Week Year, Number
     *
     * This function converts from Modified Julian Date to Week Year and Number.
     *
     * @param[in] modifiedJulianDay Modified Julian Day
     * @param[out] year "Week number" Year from 1900
     * @param[out] weekNumber Week number according to ISO 2015
     */
    static void mjd2wywn(int modifiedJulianDay, int & year, int & weekNumber);

private:
    /** Modified Julian Day (CT) */
    uint32_t m_modifiedJulianDay : 17;

    /** Hour (0..23) */
    uint8_t m_hour : 5;

    /** Minute (0..59) */
    uint8_t m_minute : 6;

    /** Sign of Local Time Offset (0=+, 1=-) */
    bool m_signLocalTimeOffset : 1;

    /** Local Time Offset (in 30 minutes) */
    uint8_t m_localTimeOffset : 5; // 0..31 => 0..930 min => 0 .. 15.5 h
};

/* relational operators */
bool ISO_62106_EXPORT operator!=(const RdsTime & lhs, const RdsTime & rhs);
bool ISO_62106_EXPORT operator> (const RdsTime & lhs, const RdsTime & rhs);
bool ISO_62106_EXPORT operator<=(const RdsTime & lhs, const RdsTime & rhs);
bool ISO_62106_EXPORT operator>=(const RdsTime & lhs, const RdsTime & rhs);

}
