/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "iso_62106_export.h"

/**
 * Raw RDS group (raw unsynchronized RDS bitstream, including CRC bits, with no particular alignment)
 *
 * @todo no CRC error correction yet
 */
class ISO_62106_EXPORT RdsGroup
{
public:
    RdsGroup();

    /* block 1 */
    uint16_t blk1 : 16;
    uint16_t crc1 : 10;
    bool crc1Valid;

    /* block 2 */
    uint16_t blk2 : 16;
    uint16_t crc2 : 10;
    bool crc2Valid;

    /* block 3 */
    uint16_t blk3 : 16;
    uint16_t crc3 : 10;
    bool crc3Valid;

    /* block 4 */
    uint16_t blk4 : 16;
    uint16_t crc4 : 10;
    bool crc4Valid;

    /** valid blocks */
    uint8_t validBlks;

    /** bit offset to reach synchronization */
    uint8_t bitOffset;

    void push(uint8_t data, uint8_t bit);

    /**
     * Calculate the RDS CRC for a block.
     *
     * @param data 16-bit block
     * @return 10-bit crc
     */
    static uint16_t calcRdsCrc(uint16_t data);

private:
    /** bit offset to reach synchronization (continuous) */
    uint8_t bitOffsetContinuous;
};
