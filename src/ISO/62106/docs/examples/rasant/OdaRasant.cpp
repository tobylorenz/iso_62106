/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "OdaRasant.h"

#include <cassert>

#include <ISO/62106.h>

OdaRasant::OdaRasant()
{
}

void OdaRasant::registerHandler()
{
    /* register ODA */
    ISO62106::OpenDataApplications::onDecodeA.connect(sigc::ptr_fun(decodeA));
}

/*@unused@*/
void OdaRasant::emitRtcmMsg1(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (1 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (7 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t)
              (satelliteData[prn].sf << 29) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 27) | /* 2-bit UDRE */
              (prn << 22) | /* 5-bit satellite ID */
              (satelliteData[prn].prc << 6); /* 16-bit pseudorange correction */
    rtcm[3] = (uint32_t)
              (satelliteData[prn].rrc << 22) | /* 8-bit range-rate correction */
              (satelliteData[prn].iod << 14) | /* 8-bit issue of data */
              (satelliteData[prn].sf << 13) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 11) | /* 2-bit UDRE */
              (prn << 6); /* 6-bit satellite ID */
    rtcm[4] = (uint32_t)
              (satelliteData[prn].prc << 14) | /* 16-bit pseudorange correction */
              (satelliteData[prn].rrc << 6); /* 8-bit range-rate correction */
    rtcm[5] = (uint32_t)
              (satelliteData[prn].iod << 22) | /* 8-bit issue of date */
              (satelliteData[prn].sf << 21) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 19) | /* 2-bit UDRE */
              (prn << 14) | /* 5-bit satellite ID */
              ((satelliteData[prn].prc >> 8) << 6); /* 8-bit pseudorange correction (h) */
    rtcm[6] = (uint32_t)
              ((satelliteData[prn].prc & 0xff) << 22) | /* 8-bit pseudorange correction (l) */
              (satelliteData[prn].rrc << 14) | /* 8-bit range-rate correction */
              (satelliteData[prn].iod << 6); /* 8-bit issue of date */

    /* complete with all satellites */

    onRtcm.emit(rtcm);
}

/*@unused@*/
void OdaRasant::emitRtcmMsg2(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (2 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (7 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t)
              (satelliteData[prn].sf << 29) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 27) | /* 2-bit UDRE */
              (prn << 22) | /* 5-bit satellite ID */
              (satelliteData[prn].prc << 6); /* 16-bit pseudorange correction */
    rtcm[3] = (uint32_t)
              (satelliteData[prn].rrc << 22) | /* 8-bit range-rate correction */
              (satelliteData[prn].iod << 14) | /* 8-bit issue of data */
              (satelliteData[prn].sf << 13) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 11) | /* 2-bit UDRE */
              (prn << 6); /* 6-bit satellite ID */
    rtcm[4] = (uint32_t)
              (satelliteData[prn].prc << 14) | /* 16-bit pseudorange correction */
              (satelliteData[prn].rrc << 6); /* 8-bit range-rate correction */
    rtcm[5] = (uint32_t)
              (satelliteData[prn].iod << 22) | /* 8-bit issue of date */
              (satelliteData[prn].sf << 21) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 19) | /* 2-bit UDRE */
              (prn << 14) | /* 5-bit satellite ID */
              ((satelliteData[prn].prc >> 8) << 6); /* 8-bit pseudorange correction (h) */
    rtcm[6] = (uint32_t)
              ((satelliteData[prn].prc & 0xff) << 22) | /* 8-bit pseudorange correction (l) */
              (satelliteData[prn].rrc << 14) | /* 8-bit range-rate correction */
              (satelliteData[prn].iod << 6); /* 8-bit issue of date */

    onRtcm.emit(rtcm);
}

void OdaRasant::emitRtcmMsg3(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (3 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (6 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t)
              ((rsc[0] >> 8) << 6); /* 24-bit x-coordinate (h) */
    rtcm[3] = (uint32_t)
              ((rsc[0] & 0xff) << 22) | /* 8-bit x-coordinate (l) */
              ((rsc[1] >> 16) << 6); /* 16-bit y-coordinate (h) */
    rtcm[4] = (uint32_t)
              ((rsc[1] & 0xffff) << 14) | /* 16-bit y-coordinate (l) */
              ((rsc[2] >> 24) << 6); /* 8-bit z-coordinate (h) */
    rtcm[5] = (uint32_t)
              ((rsc[2] & 0xffffff) << 6); /* 24-bit z-coordinate (l) */

    onRtcm.emit(rtcm);
}

/*@unused@*/
void OdaRasant::emitRtcmMsg4(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (3 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (6 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t) /** @todo check if this is correct */
              (0 << 27) | /* 3-bit dgnss */
              (0 << 26) | /* 1-bit dat */
              (0 << 22) | /* 4-bit spare */
              (0 << 14) | /* 8-bit datum alpha char 1 */
              (0 << 6); /* 8-bit datum alpha char 2 */
    rtcm[3] = (uint32_t) /** @todo check if this is correct */
              (0 << 22) | /* 8-bit datum sub div char 3 */
              (0 << 14) | /* 8-bit datum sub div char 1 */
              (0 << 6); /* 8-bit datum sub div char 2 */
    rtcm[4] = (uint32_t) /** @todo check if this is correct */
              (0 << 14) | /* 16-bit dx */
              (0 << 6); /* 8-bit dy (h) */
    rtcm[5] = (uint32_t) /** @todo check if this is correct */
              (0 << 22) | /* 8-bit dy (l) */
              (0 << 6); /* 16-bit dz */

    onRtcm.emit(rtcm);
}

void OdaRasant::emitRtcmMsg5(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (5 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (3 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t) /** @todo check if this is correct */
              (0 << 29) | /* 1-bit reserved */
              (0 << 24) | /* 5-bit PRN */
              (0 << 23) | /* 1-bit IOD1 */
              (0 << 20) | /* 3-bit health */
              (0 << 15) | /* 5-bit carrier/noise ratio (+24dB if >0) */
              (0 << 14) | /* 1-bit health enable */
              (0 << 13) | /* 1-bit new nav data soon */
              (0 << 12) | /* 1-bit loss of warning */
              (0 << 8) | /* 4-bit time to unhealth */
              (0 << 6); /* 2-bit unassigned */
    /** @todo this word can be repeated several times... */

    onRtcm.emit(rtcm);
}

/*@unused@*/
void OdaRasant::emitRtcmMsg6(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (6 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (2 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    /* None. It's a null message */

    onRtcm.emit(rtcm);
}

/*@unused@*/
void OdaRasant::emitRtcmMsg7(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (7 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (5 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t) /** @todo check if this is correct */
              (0 << 14) | /* 16-bit lat */
              (0 << 6); /* 8-bit lon (h) */
    rtcm[3] = (uint32_t) /** @todo check if this is correct */
              (0 << 22) | /* 8-bit lon (l) */
              (0 << 12) | /* 10-bit range */
              (0 << 6); /* 6-bit freq (h) */
    rtcm[4] = (uint32_t) /** @todo check if this is correct */
              (0 << 24) | /* 6-bit freq (l) */
              (rsh << 22) | /* 2-bit health */
              (rsid << 12) | /* 10-bit station ID */
              (0 << 9) | /* 3-bit bit rate */
              (0 << 8) | /* 1-bit mod mode */
              (0 << 7) | /* 1-bit sync type */
              (0 << 6); /* 1-bit encoding */
    /** @todo this word can be repeated several times... */

    onRtcm.emit(rtcm);
}

void OdaRasant::emitRtcmMsg9(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (9 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (7 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t)
              (satelliteData[prn].sf << 29) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 27) | /* 2-bit UDRE */
              (prn << 22) | /* 5-bit satellite ID */
              (satelliteData[prn].prc << 6); /* 16-bit pseudorange correction */
    rtcm[3] = (uint32_t)
              (satelliteData[prn].rrc << 22) | /* 8-bit range-rate correction */
              (satelliteData[prn].iod << 14) | /* 8-bit issue of data */
              (satelliteData[prn].sf << 13) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 11) | /* 2-bit UDRE */
              (prn << 6); /* 6-bit satellite ID */
    rtcm[4] = (uint32_t)
              (satelliteData[prn].prc << 14) | /* 16-bit pseudorange correction */
              (satelliteData[prn].rrc << 6); /* 8-bit range-rate correction */
    rtcm[5] = (uint32_t)
              (satelliteData[prn].iod << 22) | /* 8-bit issue of date */
              (satelliteData[prn].sf << 21) | /* 1-bit scale factor */
              (satelliteData[prn].udre << 19) | /* 2-bit UDRE */
              (prn << 14) | /* 5-bit satellite ID */
              ((satelliteData[prn].prc >> 8) << 6); /* 8-bit pseudorange correction (h) */
    rtcm[6] = (uint32_t)
              ((satelliteData[prn].prc & 0xff) << 22) | /* 8-bit pseudorange correction (l) */
              (satelliteData[prn].rrc << 14) | /* 8-bit range-rate correction */
              (satelliteData[prn].iod << 6); /* 8-bit issue of date */

    onRtcm.emit(rtcm);
}

void OdaRasant::emitRtcmMsg16(void)
{
    OdaRasant::RtcmMessage rtcm;

    /* RTCM Header */
    rtcm[0] = (uint32_t)
              (0x66 << 22) | /* 8-bit preamble */
              (16 << 16) | /* 6-bit message type */
              (rsid << 6); /* 10-bit station ID */
    rtcm[1] = (uint32_t)
              (mzc << 17) | /* 13-bit modified z-count */
              (sqnum << 14) | /* 3-bit sequence no. */
              (5 << 9) | /* 5-bit length of frame */
              (rsh << 3); /* 3-bit station health */
    sqnum = (sqnum + 1) % 8;

    /* RTCM Content */
    rtcm[2] = (uint32_t)
              (0x20 << 22) | /* 8-bit character ' ' */
              (0x52 << 14) | /* 8-bit character 'R' */
              (0x41 <<  6); /* 8-bit character 'A' */
    rtcm[3] = (uint32_t)
              (0x53 << 22) | /* 8-bit character 'S' */
              (0x41 << 14) | /* 8-bit character 'A' */
              (0x4e <<  6); /* 8-bit character 'N' */
    rtcm[4] = (uint32_t)
              (0x54 << 22) | /* 8-bit character 'T' */
              (0x20 << 14) | /* 8-bit character ' ' */
              (0x00 <<  6); /* 8-bit character */

    /** @todo this word can be repeated several times... */

    onRtcm.emit(rtcm);
}

void OdaRasant::decodeA(uint16_t pi, uint16_t aid, uint8_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(blk2 <= 0x1f);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    uint8_t msg = (blk2 >> 2) & 7;
    switch (msg) {
    case 0: /* Synchronization: Time synchronisation, ID and Health of reference station */
        /* (_x & 3) */ /* 2-bit not occupied */
        /* (_y >> 10) */ /* 8-bit not occupied */
        odaRasant().rsid = blk3 & 0x3ff; /* 10-bit station ID */
        odaRasant().rsh = blk4 >> 13; /* 3-bit station health */
        odaRasant().mzc = blk4 & 0x1fff; /* 13-bit modified Z-count */
        odaRasant().emitRtcmMsg5();
        break;
    case 1: /* Correction data: Satellite correction data */
        odaRasant().prn = blk4 >> 11; /* 5-bit PRN */
        odaRasant().satelliteData[odaRasant().prn].udre = blk2 & 3; /* 2-bit UDRE */
        odaRasant().satelliteData[odaRasant().prn].prc = blk3; /* 16-bit PRC [mm] (signed) */
        /* (_z >> 10) & 1 */ /* 1-bit T: Time switch (0=even minute, 1=odd minute) */
        odaRasant().satelliteData[odaRasant().prn].sf = (blk4 >> 9) & 1; /* 1-bit SF: Scaling Factor */
        /* (z >> 8) & 1, */ /* 1-bit IOD switch (changes with each IOD change) */
        odaRasant().satelliteData[odaRasant().prn].rrc = (blk4 & 0xff); /* 8-bit RRC [mm/sec] (signed) */
        odaRasant().emitRtcmMsg9();
        break;
    case 2: /* Delta correction: Satellite correction data (when IOD changes) */
        odaRasant().prn = blk4 >> 11; /* 5-bit PRN */
        odaRasant().satelliteData[odaRasant().prn].udre = blk2 & 3; /* 2-bit UDRE */
        odaRasant().satelliteData[odaRasant().prn].prc = blk3; /* 16-bit PRC [mm] (signed) */
        /* (z >> 10) & 1 */ /* 1-bit ?? flag */
        /* (z >> 9) & 1 */ /* 1-bit ?? flag */
        odaRasant().satelliteData[odaRasant().prn].sf = (blk4 >> 8) & 1; /* 1-bit SF */
        odaRasant().satelliteData[odaRasant().prn].iod = blk4 & 0xff; /* 8-bit IODE */
        odaRasant().emitRtcmMsg9();
        break;
    case 3: /* Reference station data: Reference station coordinates */
        if ((blk2 & 3) >= 3) {
            break;
        }
        odaRasant().rsc[blk2 & 3] = ((blk3 << 8) | blk4); /* 32-bit reference station coordinates [cm] (signed 32 bit number) */
        odaRasant().emitRtcmMsg3();
        break;
    case 4: /* Optional data */
        switch (blk2 & 3) {
        case 3:
            /* y */ /* 16-bit ??? */
            /* z */ /* 16-bit picode */
            /* z looks like a picode: */
            /* y=e0dc z=d3a3=swr3 bw */ /* -32/-36 */
            /* y=e0fc z=d3a3=swr3 bw */ /* -32/-4 */
            /* y=e0cd z=d81c=charivari neumarkt */
            odaRasant().emitRtcmMsg16(); /** @todo check if this is correct */
            break;
        default:
            ISO62106::decodeIssue.emit("RASANT: msg=4, blk2=3 is not implemented yet");
            break;
        }
        break;
    case 5: /* IOD: Satellite data IOD */
        /* ((_x & 3) << 3) || (_y >> 13) */ /* 5-bit PRN1    satellite number 0..31 */
        /* (y >> 5) & 0xff */ /* 8-bit IOD1    Issue of Data for PRN1, matching the IOD for the current ephemeris of this satellite, as transmitted by the satellite. */
        /* y & 0x1f */ /* 5-bit PRN2    satellite number 0..31 */
        /* z >> 8 */ /* 8-bit IOD2new issue of data for PRN2, new value (90sec transistion period) */
        /* z & 0xff */ /* 8-bit IOD2old issue of data for PRN2, old value */
        /* emitRtcmMsg9(); */ /** @todo check if this is correct */
        break;
    case 6:
        ISO62106::decodeIssue.emit("RASANT: msg=6 is not assiged");
        break;
    case 7:
        ISO62106::decodeIssue.emit("RASANT: msg=7 is not assiged");
        break;
    }
}

sigc::signal<void, OdaRasant::RtcmMessage> OdaRasant::onRtcm;

OdaRasant & odaRasant()
{
    /* singleton */
    static OdaRasant entry;
    return entry;
}
