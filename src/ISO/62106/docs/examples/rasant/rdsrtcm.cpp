/*
 * Copyright (C) 2009-2016 Tobias Lorenz
 *
 * This file is part of librds.
 *
 * librds is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librds is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with librds.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <unistd.h>

#include <ISO/62106.h>
#include "../RdsFile.h"
#include "../RdsGroup.h"
#include "OdaRasant.h"

/**
 * @brief Parity function
 *
 * This functions return the parity information for the given data.
 *
 * @param[in] th Data
 * @return Parity information
 */
static uint8_t isgps_parity(uint32_t th)
{
    const uint8_t parity_array[] = {
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
        0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0
    };
    constexpr uint32_t PARITY_25 = 0xbb1f3480;
    constexpr uint32_t PARITY_26 = 0x5d8f9a40;
    constexpr uint32_t PARITY_27 = 0xaec7cd00;
    constexpr uint32_t PARITY_28 = 0x5763e680;
    constexpr uint32_t PARITY_29 = 0x6bb1f340;
    constexpr uint32_t PARITY_30 = 0x8b7a89c0;

    uint32_t t;
    uint8_t p;
    t = th & PARITY_25;
    p = parity_array[t & 0xff] ^ parity_array[(t >> 8) & 0xff] ^
        parity_array[(t >> 16) & 0xff] ^ parity_array[(t >> 24) & 0xff];
    t = th & PARITY_26;
    p = (p << 1) | (parity_array[t & 0xff] ^ parity_array[(t >> 8) & 0xff] ^
                    parity_array[(t >> 16) & 0xff] ^ parity_array[(t >> 24) & 0xff]);
    t = th & PARITY_27;
    p = (p << 1) | (parity_array[t & 0xff] ^ parity_array[(t >> 8) & 0xff] ^
                    parity_array[(t >> 16) & 0xff] ^ parity_array[(t >> 24) & 0xff]);
    t = th & PARITY_28;
    p = (p << 1) | (parity_array[t & 0xff] ^ parity_array[(t >> 8) & 0xff] ^
                    parity_array[(t >> 16) & 0xff] ^ parity_array[(t >> 24) & 0xff]);
    t = th & PARITY_29;
    p = (p << 1) | (parity_array[t & 0xff] ^ parity_array[(t >> 8) & 0xff] ^
                    parity_array[(t >> 16) & 0xff] ^ parity_array[(t >> 24) & 0xff]);
    t = th & PARITY_30;
    p = (p << 1) | (parity_array[t & 0xff] ^ parity_array[(t >> 8) & 0xff] ^
                    parity_array[(t >> 16) & 0xff] ^ parity_array[(t >> 24) & 0xff]);

    return p;
}

static std::ofstream ofs;

/**
 * @brief RTCM stream generation function
 *
 * This functions generated the RTCM stream based on RTCM blocks.
 *
 * @param[in] rtcmMsg RTCM message
 */
static void handleRtcm(OdaRasant::RtcmMessage rtcmMsg)
{
    static uint32_t w;
    const std::array <uint8_t, 64> reverseBits = {
        {
            0, 32, 16, 48, 8, 40, 24, 56, 4, 36, 20, 52, 12, 44, 28, 60,
            2, 34, 18, 50, 10, 42, 26, 58, 6, 38, 22, 54, 14, 46, 30, 62,
            1, 33, 17, 49, 9, 41, 25, 57, 5, 37, 21, 53, 13, 45, 29, 61,
            3, 35, 19, 51, 11, 43, 27, 59, 7, 39, 23, 55, 15, 47, 31, 63
        }
    };
    constexpr uint32_t P_30_MASK = 0x40000000;
    constexpr uint32_t W_DATA_MASK = 0x3fffffc0;
    constexpr uint8_t MAG_SHIFT = 6;
    constexpr uint8_t MAG_TAG_DATA = (1 << MAG_SHIFT);
    constexpr uint8_t MAG_TAG_MASK = (3 << MAG_SHIFT);

    for (uint32_t ip : rtcmMsg) {
        w <<= 30;
        w |= ip & W_DATA_MASK;
        w |= isgps_parity(w);

        /* weird-assed inversion */
        if ((w & P_30_MASK) == P_30_MASK) {
            w ^= W_DATA_MASK;
        }

        /*
         * Write each 30-bit IS-GPS-200 data word as 5 Magnavox-format bytes
         * with data in the low 6-bits of the byte.  MSB first.
         */
        uint8_t data;
        data = MAG_TAG_DATA | reverseBits[(w >> 24) & 0x3f];
        ofs.write(reinterpret_cast<char *>(&data), sizeof(data));
        data = MAG_TAG_DATA | reverseBits[(w >> 18) & 0x3f];
        ofs.write(reinterpret_cast<char *>(&data), sizeof(data));
        data = MAG_TAG_DATA | reverseBits[(w >> 12) & 0x3f];
        ofs.write(reinterpret_cast<char *>(&data), sizeof(data));
        data = MAG_TAG_DATA | reverseBits[(w >>  6) & 0x3f];
        ofs.write(reinterpret_cast<char *>(&data), sizeof(data));
        data = MAG_TAG_DATA | reverseBits[ w        & 0x3f];
        ofs.write(reinterpret_cast<char *>(&data), sizeof(data));
    }
}

/**
 * This program dumps all received RASANT data from a RDS stream
 * as RTCM binary output suitable for gpsd
 */
int main(int argc, char * argv[])
{
    /* evaluate options */
    int opt;
    int filter = 0;
    int usage  = 0;
    while ((opt = getopt(argc, argv, "f:")) != -1) {
        switch (opt) {
        case 'f':
            if (strcmp(optarg, "v4l") == 0) {
                filter = 0;
            } else if (strcmp(optarg, "raw") == 0) {
                filter = 1;
            } else if (strcmp(optarg, "csv") == 0) {
                filter = 2;
            } else if (strcmp(optarg, "smp") == 0) {
                filter = 3;
            } else {
                filter = -1;
            }
            break;
        default: /* '?' */
            usage = 1;
        }
    }

    /* safety checks */
    if ((filter == -1) || ((argc - optind) != 2)) {
        usage = 1;
    }
    if (usage == 1) {
        std::cout << "Usage: " << argv[0] << " [-f filter] filename rtcmout" << std::endl;
        return EXIT_FAILURE;
    }

    /* set callbacks */
    RdsFile rdsFile;
    OdaRasant::onRtcm.connect(sigc::ptr_fun(handleRtcm));

    /* open file */
    std::ifstream ifs;
    bool inputFromCin = (strcmp(argv[optind], "-") == 0);
    if (!inputFromCin) {
        ifs.open(argv[optind]);
        if (!ifs.is_open()) {
            std::cout << "Unable to open file " << argv[optind] << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* open RTCM output file */
    bool outputToCout = (strcmp(argv[optind], "-") == 0);
    if (!outputToCout) {
        ofs.open(argv[optind]);
        if (!ofs.is_open()) {
            std::cout << "Unable to open file " << argv[optind] << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* read in stream */
    bool decodeSuccess = false;
    do {
        switch (filter) {
        case 0:
            decodeSuccess = rdsFile.decodeV4l(inputFromCin ? std::cin : ifs);
            break;
        case 1:
            decodeSuccess = rdsFile.decodeRaw(inputFromCin ? std::cin : ifs);
            break;
        case 2:
            decodeSuccess = rdsFile.decodeCsv(inputFromCin ? std::cin : ifs);
            break;
        case 3:
            decodeSuccess = rdsFile.decodeSmp(inputFromCin ? std::cin : ifs);
            break;
        }
    } while (decodeSuccess);

    /* close input file */
    if (inputFromCin) {
        ifs.close();
    }

    /* close RTCM output file */
    if (outputToCout) {
        ofs.close();
    }

    return EXIT_SUCCESS;
}
