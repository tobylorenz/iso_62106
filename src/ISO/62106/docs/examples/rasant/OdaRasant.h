/*
 * Copyright (C) 2009-2016 Tobias Lorenz
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <vector>

#include <ISO/62106.h>

#include "iso_62106_export.h"

/*
 * RTCM 2.0 information:
 * MZC      Modified Z-Count
 * T        Time Mark                       odd reference minute (GPS time), wechselt xx:xx:49 UTC
 * UDRE     User Differential Range Error (One Sigma Differential Error)
 *          0: <1m
 *          1: >1m and <= 4m
 *          2: >4m and <= 8m
 *          3: >8m
 * SATID    Satellite PRN                   PRN satellite number 0..31
 * PRC      Pseudorange Correction          [mm]     (signed) (s defines scale factor)
 * SF       Scale Factor (for PRC+RRC)
 *          0: range *  20, rangerate *  2
 *          1: range * 320, rangerate * 32
 *          [m] [m from]    [m to]      [m/s]   [m/s from]  [m/s to]    SF
 *          0.02    -655.34     +655.34     0.002   -0.254      +0.254      0
 *          0.32    -10485.44   +10485.44   0.032   -4.064      +4.064      1
 * RRC      Pseudorange Correction Rate     [mm/sec] (signed) (s defines scale factor)
 * IODE     Satellite Ephemeris             Issue Of Data for PRN, new value
                                            Issue of Data for PRN, matching the IOD for the current ephemeris of this satellite, as transmitted by the satellite.
                                            (90sec transistion period)
 * rsc      reference station coordinates [cm] (signed 32 bit number)
 */

/**
 * Radio Aided Satellite Navigation Technique
 *
 * This class received ODA-RASANT information on
 * AID=0x4aa1 and converts it into RTCM 2.0 data
 * for navigation equipment.
 */
class ISO_62106_EXPORT OdaRasant
{
public:
    explicit OdaRasant();

    /** RASANT AID */
    static constexpr uint16_t applicationId = 0x4aa1;

    /**
     * @brief RTCM message
     *
     * This variable contains the RTCM message content.
     */
    using RtcmMessage = std::vector<uint32_t>;

    /** event handler on new rtcm data */
    static sigc::signal<void, RtcmMessage> onRtcm;

    /**
     * @brief Decodes and handles received RASANT information
     *
     * This function decodes and handles received RASANT information.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] blk2 Block 2 data
     * @param[in] blk3 Block 3 data
     * @param[in] blk4 Block 4 data
     */
    static void decodeA(uint16_t pi, uint16_t aid, uint8_t blk2, uint16_t blk3, uint16_t blk4);

    /** register handler */
    static void registerHandler();

private:
    /** modified z-count */
    uint16_t mzc : 13;

    /** reference station coordinates [x, y, z], signed */
    int32_t rsc[3];

    /** reference station ID */
    uint16_t rsid : 10;

    /** health */
    uint8_t rsh : 3;

    /** satellite data structure */
    struct SatelliteData {
        /** scale factor */
        uint8_t sf : 1;

        /** user differential range error */
        uint8_t udre : 2;

        /** pseudorange correction, signed */
        uint16_t prc : 16;

        /** pseudorange correction rate, signed */
        uint8_t rrc : 8;

        /** issue of date ephemeris */
        uint8_t iod : 8;
    };

    /** satellite data */
    std::map<uint8_t, SatelliteData> satelliteData;

    /** satellite number */
    uint8_t prn : 5;

    /**
     * @brief Sequence number
     *
     * This variable contains the RTCM sequence number.
     */
    uint32_t sqnum;

    /**
     * @brief Differential GPS Corrections
     *
     * This function generates RTCM msg1 blocks.
     */
    void emitRtcmMsg1();

    /**
     * @brief Delta Differential GPS Corrections
     *
     * This function generates RTCM msg2 blocks.
     */
    void emitRtcmMsg2();

    /**
     * @brief Reference Station Parameters
     *
     * This function generates RTCM msg3 blocks.
     */
    void emitRtcmMsg3();

    /**
     * @brief Reference Station Datum
     * @todo This is probably not needed for RASANT.
     *
     * This function generates RTCM msg4 blocks.
     */
    void emitRtcmMsg4();

    /**
     * @brief Constellation Health
     * @todo This is probably not needed for RASANT.
     *
     * This function generates RTCM msg5 blocks.
     */
    void emitRtcmMsg5();

    /**
     * @brief Null Frame
     * @todo This is probably not needed for RASANT.
     *
     * This function generates RTCM msg6 blocks.
     */
    void emitRtcmMsg6();

    /**
     * @brief Radio Beacon Almanac
     * @todo This is probably not needed for RASANT.
     *
     * This function generates RTCM msg7 blocks.
     */
    void emitRtcmMsg7();

    /**
     * @brief Partial Satellite Set Differential Corrections
     *
     * This function generates RTCM msg9 blocks.
     */
    void emitRtcmMsg9(void);

    /**
     * @brief Special Message
     *
     * This function generates RTCM msg5 blocks.
     */
    void emitRtcmMsg16(void);
};

/**
 * Return reference to RASANT entry.
 *
 * @return Reference to RASANT entry.
 */
extern ISO_62106_EXPORT OdaRasant & odaRasant(); /* singleton */
