/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <ctime>
#include <unistd.h>

#include <ISO/62106.h>
#include <sigc++-2.0/sigc++/signal.h>

#include "../RdsFile.h"
#include "print.h"
#include "ansi.h"

int main(int argc, char * argv[])
{
    /* evaluate options */
    int opt;
    int filter = 0;
    int usage  = 0;
    while ((opt = getopt(argc, argv, "f:")) != -1) {
        switch (opt) {
        case 'f':
            if (strcmp(optarg, "v4l") == 0) {
                filter = 0;
            } else if (strcmp(optarg, "raw") == 0) {
                filter = 1;
            } else if (strcmp(optarg, "csv") == 0) {
                filter = 2;
            } else if (strcmp(optarg, "smp") == 0) {
                filter = 3;
            } else {
                filter = -1;
            }
            break;
        default: /* '?' */
            usage = 1;
        }
    }

    /* checks */
    if ((filter == -1) || ((argc - optind) != 1)) {
        usage = 1;
    }
    if (usage == 1) {
        std::cout << "Usage: " << argv[0] << " [-f filter] filename" << std::endl;
        return EXIT_FAILURE;
    }

    ansiCursorHome();
    ansiClearScreen();

    /* set callbacks */
    RdsFile rdsFile;
    rdsFile.onChange.connect(sigc::ptr_fun(piPrint));
    ISO62106::AlternativeFrequencies::onChange.connect(sigc::ptr_fun(afPrint));
    ISO62106::ClockTime::onChange.connect(sigc::ptr_fun(ctPrint));
    ISO62106::DecoderIdentification::onChange.connect(sigc::ptr_fun(diPrint));
    ISO62106::ExtendedCountryCode::onChange.connect(sigc::ptr_fun(eccPrint));
    // eon
    // erp
    // ews
    ISO62106::InHouse::onUpdateA.connect(sigc::ptr_fun(ihPrintA));
    ISO62106::InHouse::onUpdateB.connect(sigc::ptr_fun(ihPrintB));
    ISO62106::LanguageIdentificationCode::onChange.connect(sigc::ptr_fun(licPrint));
    ISO62106::MusicSpeech::onChange.connect(sigc::ptr_fun(msPrint));
    ISO62106::OpenDataApplications::onChange.connect(sigc::ptr_fun(odaPrint));
    ISO62106::EnhancedRadioText::onChange.connect(sigc::ptr_fun(odaErtPrint));
    ISO62106::RadioTextPlus::onUpdate.connect(sigc::ptr_fun(odaRtpPrint));
    // pi
    ISO62106::ProgrammeItemNumber::onChange.connect(sigc::ptr_fun(pinPrint));
    ISO62106::ProgrammService::onChange.connect(sigc::ptr_fun(psPrint));
    ISO62106::ProgrammeType::onChange.connect(sigc::ptr_fun(ptyPrint));
    ISO62106::ProgrammeTypeName::onChange.connect(sigc::ptr_fun(ptynPrint));
    // -- rds, rdsProgram, rdsReceiver
    // rp
    ISO62106::RadioText::onChange.connect(sigc::ptr_fun(rtPrint));
    ISO62106::TrafficAnnouncement::onChange.connect(sigc::ptr_fun(tpTaPrint));
    ISO62106::TransparentDataChannels::onUpdate.connect(sigc::ptr_fun(tdcPrint));
    ISO62106::TrafficProgramme::onChange.connect(sigc::ptr_fun(tpTaPrint));

    /* open file */
    std::ifstream ifs;
    bool inputFromCin = (strcmp(argv[optind], "-") == 0);
    if (!inputFromCin) {
        ifs.open(argv[optind]);
        if (!ifs.is_open()) {
            std::cout << "Unable to open file " << argv[optind] << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* read in stream */
    bool decodeSuccess = false;
    do {
        switch (filter) {
        case 0:
            decodeSuccess = rdsFile.decodeV4l(inputFromCin ? std::cin : ifs);
            break;
        case 1:
            decodeSuccess = rdsFile.decodeRaw(inputFromCin ? std::cin : ifs);
            break;
        case 2:
            decodeSuccess = rdsFile.decodeCsv(inputFromCin ? std::cin : ifs);
            break;
        case 3:
            decodeSuccess = rdsFile.decodeSmp(inputFromCin ? std::cin : ifs);
            break;
        }
    } while (decodeSuccess);

    /* close file */
    if (inputFromCin) {
        ifs.close();
    }

    /* go to last line */
    goToEnd();

    return EXIT_SUCCESS;
}
