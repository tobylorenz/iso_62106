/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <cwchar>

#include <ISO/62106.h>

void afPrint(uint16_t pi, uint32_t tuningFrequency);
void ctPrint(uint16_t pi);
void diPrint(uint16_t pi);
void eccPrint(uint16_t pi);
void eonPrint(uint16_t pi);
void eonTnMfPrint(uint16_t pi);
void ihPrintA(uint16_t pi, uint8_t x, uint16_t y, uint16_t z);
void ihPrintB(uint16_t pi, uint8_t x, uint16_t z);
void licPrint(uint16_t pi);
void msPrint(uint16_t pi);
void odaPrint(uint16_t pi, uint8_t agtc, uint8_t agtv, uint16_t aid);
void odaErtPrint(uint16_t pi);
void odaRtpPrint(uint16_t pi, uint8_t ct0, uint8_t ct1);
void piPrint(uint16_t pi);
void pinPrint(uint16_t pi);
void psPrint(uint16_t pi);
void ptyPrint(uint16_t pi);
void ptynPrint(uint16_t pi);
void rtPrint(uint16_t pi);
void tdcPrint(uint16_t pi, uint8_t addr, uint16_t data);
void tpTaPrint(uint16_t pi);

void goToEnd();
