include_directories(${PROJECT_SOURCE_DIR}/src)

add_executable(rdsdump rdsdump.cpp ansi.h ansi.cpp print.h print.cpp)

target_link_libraries(rdsdump ${PROJECT_NAME} RdsFile)
