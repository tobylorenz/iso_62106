/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "print.h"

#include <cstdint>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

#include <ISO/62106.h>

#include "ansi.h"

/* line numbers */
enum {
    piLine = 1,
    ctLine,
    diLine,
    eccLine,
    eonLine,
    ihLine,
    licLine,
    msLine,
    odaLine,
    odaErtLine,
    odaRtpLine0,
    odaRtpLine1,
    pinLine,
    psLine,
    ptyLine,
    ptynLine,
    rtLine,
    tdcLine,
    tpTaLine,
    afLine,
    lastLine
};

void afPrint(uint16_t pi, uint32_t /*tuningFrequency*/)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(afLine);

    /* print list */
    std::map<uint32_t, std::vector<ISO62106::AlternativeFrequencies::MappedFrequency>> mappedFrequencies =
                program.alternativeFrequencies.mappedFrequencies();
    for (auto & list : mappedFrequencies) {
        ansiClearLine();
        std::cout << "AF:";
        std::cout << " " << list.first << " kHz (TF)";
        for (auto & entry : list.second) {
            std::cout << ", " << entry.frequency << " kHz";
            if (entry.sameProgramme) {
                std::cout << " (SP)";
            } else {
                std::cout << " (VP)";
            }
        }
        std::cout << std::endl;
    }
}

void ctPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(ctLine);
    ansiClearLine();

    ISO62106::RdsTime t = program.clockTime.value();
    std::cout
            << std::setfill('0') << std::dec
            << "CT: "
            << std::setw(4) << (t.year() + 1900) << "-"
            << std::setw(2) << static_cast<uint16_t>(t.month()) << "-"
            << std::setw(2) << static_cast<uint16_t>(t.day())
            << " "
            << std::setw(2) << static_cast<uint16_t>(t.hour()) << ":"
            << std::setw(2) << static_cast<uint16_t>(t.minute())
            << " ("
            << (t.signLocalTimeOffset() ? "-" : "+")
            << std::setw(2) << static_cast<uint16_t>(t.localTimeOffset() / 2) << ":"
            << std::setw(2) << static_cast<uint16_t>(30 * (t.localTimeOffset() % 2))
            << ")" << std::endl;
}

void diPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(diLine);
    ansiClearLine();

    std::cout << "DI: ";

    std::cout << (program.decoderIdentification.stereo() ? "Stereo" : "Mono");
    std::cout << ", ";

    std::cout << (program.decoderIdentification.artificialHead() ? "Artificial Head" : "Not Artificial Head");
    std::cout << ", ";

    std::cout << (program.decoderIdentification.compressed() ? "Compressed" : "Not compressed");
    std::cout << ", ";

    std::cout << (program.decoderIdentification.dynamicallySwitchedPty() ? "Dynamically switched PTY" : "Static PTY");
    std::cout << std::endl;
}

void eccPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(eccLine);
    ansiClearLine();

    std::cout
            << "ECC:"
            << " ECC=" << std::hex << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(program.extendedCountryCode.value())
            << " CC=" << std::hex << std::setw(0) << static_cast<uint16_t>(program.programmeIdentification.countryCode())
            << " ISO=" << program.extendedCountryCode.isoCountryCode()
            << " ITU=" << std::dec << std::setw(0) << static_cast<uint16_t>(program.extendedCountryCode.ituRegion())
            << std::endl;
}

void eonPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(eonLine);
    ansiClearLine();

    std::cout << "EON: cross-referenced programs:";
    for (auto linkageInformation : program.enhancedOtherNetworks.linkageInformations()) {
        std::cout
                << " ("
                << " PI:" << std::hex << std::setw(2) << std::setfill('0') << linkageInformation.first
                << " LA:" << std::dec << std::setw(0) << static_cast<uint16_t>(linkageInformation.second.linkageActuator)
                << " EG:" << std::dec << std::setw(0) << static_cast<uint16_t>(linkageInformation.second.extendedGeneric)
                << " ILS:" << std::dec << std::setw(0) << static_cast<uint16_t>(linkageInformation.second.internationalLinkageSet)
                << " LSN:" << std::dec << std::setw(0) << linkageInformation.second.linkageSetNumber
                << ")";
    }
    std::cout << std::endl;
}

void eonTnMfPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    std::cout
            << "EON: Tuned Networks:"
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[0].tuningNetwork)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[1].tuningNetwork)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[2].tuningNetwork)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[3].tuningNetwork)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[4].tuningNetwork)
            << std::endl;

    std::cout
            << "EON: Mapped Frequencies:"
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[0].mappedFrequency)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[1].mappedFrequency)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[2].mappedFrequency)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[3].mappedFrequency)
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.enhancedOtherNetworks.mappedFrequencies()[4].mappedFrequency)
            << std::endl;
}

void ihPrintA(uint16_t, uint8_t x, uint16_t y, uint16_t z)
{
    /* ansi position */
    ansiCursorRow(ihLine);
    ansiClearLine();

    std::cout
            << "IH: A"
            << " x=" << std::hex << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(x)
            << " y=" << std::hex << std::setw(4) << std::setfill('0') << y
            << " z=" << std::hex << std::setw(4) << std::setfill('0') << z
            << std::endl;
}

void ihPrintB(uint16_t, uint8_t x, uint16_t z)
{
    /* ansi position */
    ansiCursorRow(ihLine);
    ansiClearLine();

    std::cout
            << "IH: B"
            << " x=" << std::hex << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(x)
            << " z=" << std::hex << std::setw(4) << std::setfill('0') << z
            << std::endl;
}

void licPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    std::string str = program.languageIdentificationCode.language();

    /* ansi position */
    ansiCursorRow(licLine);
    ansiClearLine();

    std::cout
            << "LIC:"
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.languageIdentificationCode.value())
            << " (" << str << ")"
            << std::endl;
}

void msPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(msLine);
    ansiClearLine();

    std::cout << "MS: ";
    std::cout << (program.musicSpeech.music() ? "Music" : "Speech");
    std::cout << std::endl;
}

void odaPrint(uint16_t, uint8_t agtc, uint8_t agtv, uint16_t aid)
{
    /* ansi position */
    ansiCursorRow(odaLine);
    ansiClearLine();

    std::cout
            << "ODA: "
            << ((aid == 0) ? "deallocation" : "allocation") << " of "
            << "agt=" << std::dec << static_cast<uint16_t>(agtc) << (agtv == 0 ? 'A' : 'B');
    if (aid != 0) {
        std::cout << " to aid=" << std::hex << std::setw(4) << std::setfill('0') << aid;
    }
    std::cout << std::endl;
}

void odaErtPrint(uint16_t pi)
{
    /* ansi position */
    ansiCursorRow(odaErtLine);
    ansiClearLine();

    std::cout
            << "ERT: "
            << ISO62106::enhancedRadioText(pi).valueUtf8()
            << std::endl;
}

void odaRtpPrint(uint16_t pi, uint8_t ct0, uint8_t ct1)
{
    std::string str0 = ISO62106::radioTextPlus(pi).classStr(ct0);
    std::string str1 = ISO62106::radioTextPlus(pi).classStr(ct1);

    /* ansi position */
    ansiCursorRow(odaRtpLine0);
    ansiClearLine();

    //    std::cout
    //            << "RTP: "
    //            << str0
    //            << "='";
    //    std::wcout << program.odaRtp.value[ct0];
    //    std::cout
    //            << "'"
    //            << std::endl;

    /* ansi position */
    ansiCursorRow(odaRtpLine1);
    ansiClearLine();

    //    std::cout
    //            << "RTP: "
    //            << str1
    //            << "='";
    //    std::wcout << program.odaRtp.value[ct1];
    //    std::cout
    //            << "'"
    //            << std::endl;
}

/* country code */
static const std::array<std::string, 16> piCcStr = {{
        "",
        "Deutschland",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "Deutschland",
        "",
        ""
    }
};

/* programme in terms of area coverage */
static const std::array<std::string, 16> piAcStr = {{
        "local",
        "international",
        "national",
        "supra-regional",
        "regional 1",
        "regional 2",
        "regional 3",
        "regional 4",
        "regional 5",
        "regional 6",
        "regional 7",
        "regional 8",
        "regional 9",
        "regional 10",
        "regional 11",
        "regional 12"
    }
};

/* Germany: prn1 is federal land/state */
static const std::array<std::string, 16> piPrn1Str = {{
        "Baden-Württemberg",
        "Bayern",
        "Berlin",
        "Brandenburg",
        "Bremen und Bremerhaven",
        "Hamburg",
        "Hessen",
        "Mecklenburg-Vorpommern",
        "Niedersachsen",
        "Nordrhein-Westfalen",
        "Rheinland-Pfalz",
        "Saarland",
        "Sachsen",
        "Sachsen-Anhalt",
        "Schleswig-Holstein",
        "Thüringen"
    }
};

void piPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    unsigned char cc;
    unsigned char ac;
    unsigned char prn1;
    unsigned char prn2;
    unsigned char prn;

    /* ansi position */
    ansiCursorRow(piLine);
    ansiClearLine();

    cc   = (program.programmeIdentification.value() >> 12) & 0x0f; /* nibble 1: country code */
    ac   = (program.programmeIdentification.value() >>  8) & 0x0f; /* nibble 2: programme in terms of area coverage */
    prn1 = (program.programmeIdentification.value() >>  4) & 0x0f; /* nibble 3: programme reference number - part 1 */
    prn2 = (program.programmeIdentification.value() >>  0) & 0x0f; /* nibble 4: programme reference number - part 2 */
    prn  = (program.programmeIdentification.value() >>  0) & 0xff;

    if (program.extendedCountryCode.value() != 0) {
        eccPrint(pi);
    }
    std::cout
            << "PI: "
            << std::hex << std::setw(4) << std::setfill('0') << program.programmeIdentification.value()
            << " (" << piCcStr[cc]
            << ", " << piAcStr[ac];

    /* Germany specific */
    if ((cc == 0x1) || (cc == 0xd)) {
        if ((cc == 0xd) && (prn2 < 8)) {
            std::cout << ", öffentlich-rechtlich";
        } else {
            std::cout << ", privat";
        }

        if ((ac != 1) && (ac != 2)) {
            std::cout << ", " << piPrn1Str[prn1];
        }
    }

    /* France specific */
    if (cc == 0xF) {
        /* Indicatif zone géographique */
        switch (ac) {
        case 0:
            std::cout << ", Radios locales";
            break;
        case 1:
            std::cout << ", Radios internationales";
            break;
        case 2:
            std::cout << ", Radios nationales";
            break;
        default: /* 4..F */
            std::cout << ", Radios à couverture régionales";
            break;
        };

        /* Indicatif de la radio */
        if ((prn >= 0x01) && (prn <= 0x0A)) {
            std::cout << ", service public";
        } else if ((prn >= 0x0B) && (prn <= 0x1E)) {
            std::cout << ", autres réseaux nationaux";
        } else if ((prn >= 0x1F) && (prn <= 0x32)) {
            std::cout << ", réseaux régionaux";
        } else if ((prn >= 0x33) && (prn <= 0x50)) {
            std::cout << ", autres réseaux régionaux";
        } else if ((prn >= 0x51) && (prn <= 0x78)) {
            std::cout << ", radios locales";
        } else {
            std::cout << ", Unknown";
        }
    }

    std::cout << ")" << std::endl;
}

void pinPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(pinLine);
    ansiClearLine();

    std::cout
            << "PIN:"
            << " day " << std::dec << std::setw(2) << std::setfill(' ') << static_cast<uint16_t>(program.programmeItemNumber.day())
            << " time " << std::dec << std::setw(2) << std::setfill(' ') << static_cast<uint16_t>(program.programmeItemNumber.hour())
            << ":" << std::dec << std::setw(2) << std::setfill(' ') << static_cast<uint16_t>(program.programmeItemNumber.minute())
            << std::endl;
}

void psPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(psLine);
    ansiClearLine();

    std::cout
            << "PS: "
            << program.programmeService.valueUtf8()
            << std::endl;
}

void ptyPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    std::string str = program.programmeType.term(80, (program.extendedCountryCode.ituRegion() == 2) ? 1 : 0);

    /* ansi position */
    ansiCursorRow(ptyLine);
    ansiClearLine();

    std::cout
            << "PTY:"
            << " " << std::dec << std::setw(0) << static_cast<uint16_t>(program.programmeType.value())
            << " (" << str << ")"
            << std::endl;
}

void ptynPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(ptynLine);
    ansiClearLine();

    std::cout
            << "PTYN: "
            << program.programmeTypeName.valueUtf8()
            << std::endl;
}

void rtPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(rtLine);
    ansiClearLine();

    std::cout
            << "RT: "
            << program.radioText.valueUtf8()
            << std::endl;
}

void tdcPrint(uint16_t, uint8_t addr, uint16_t data)
{
    /* ansi position */
    ansiCursorRow(tdcLine);
    ansiClearLine();

    std::cout
            << "TDC:"
            << " channel " << std::dec << std::setw(0) << static_cast<uint16_t>(addr)
            << ": " << std::hex << std::setw(4) << std::setfill('0') << data
            << std::endl;
}

void tpTaPrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* ansi position */
    ansiCursorRow(tpTaLine);
    ansiClearLine();

    if (program.trafficProgramme.value() == 0) {
        if (program.trafficAnnouncement.value() == 0) {
            /* This programme does not carry traffic announcements */
            /* nor does it refer, via EON, to a programme that does. */
            std::cout << "TPTA: TP=0 TA=0 (no EON referral to other TP)" << std::endl;
        } else {
            /* This programme carries EON information about another programme */
            /* which gives traffic information. */
            std::cout << "TPTA: TP=0 TA=1 (EON refers to traffic info)" << std::endl;
        }
    } else {
        if (program.trafficAnnouncement.value() == 0) {
            /* This programme carries traffic announcements */
            /* but none are being broadcast at present and */
            /* may also carry EON information about */
            /* other traffic announcements. */
            std::cout << "TPTA: TP=1 TA=0 (EON may be available)" << std::endl;
        } else {
            /* A traffic announcement is being broadcast */
            /* on this programme at present. */
            std::cout << "TPTA: TP=1 TA=1 (TA is being broadcast)" << std::endl;
        }
    }
}

void goToEnd()
{
    ansiCursorRow(lastLine);
}
