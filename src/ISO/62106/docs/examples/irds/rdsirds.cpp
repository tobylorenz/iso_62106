/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <ISO/62106.h>

#include "OdaIrds.h"

int main()
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::OdaIrds::registerHandler();

    program.openDataApplications.decodeIdent(11, 0, 0x1234, 0xc563);

    /* eom => update */
    program.openDataApplications.decodeA(11, 31, (0xff << 8), 0);

    /* just trigger all address codes */
    for (uint8_t ac = 0; ac <= 31; ac++) {
        program.openDataApplications.decodeA(11, ac, 0x1234, 0x5678);
    }
}
