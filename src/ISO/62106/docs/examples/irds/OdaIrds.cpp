/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "OdaIrds.h"

#include <array>
#include <cassert>
#include <cstring>

#include <ISO/62106.h>

namespace ISO62106 {

OdaIrds::OdaIrds()
{
}

/**
 * @brief In-Receiver Database System update data information
 *
 * This structure contains update data information.
 */
static struct IrdsUpdateData {
    /* */

    /** ROM Class number (0..31) */
    uint8_t romClassNumber : 5;

    /** Update Transmission Serial number (0..1023) */
    uint16_t updateTransmissionSerialNumber : 10;

    /** Scope flag (0=Partial, 1=Total) */
    uint8_t scopeFlag : 1;

    /* */

    /** ROM Addresses */
    uint16_t pointer : 16;

    /** Update length */
    uint16_t updateLength : 16;

    /** End Of Message (0xff) */
    uint8_t eom;

    /* */

    /** Grid: center grid of updated region (0..16383) */
    uint16_t grid : 14;

    /** Coverage: 0=1grid(1x1) 1=9 grids(3x3) 2=25grids(5x5) 3=81grids(9x9) */
    uint8_t coverage;

    /** Channel */
    uint8_t channel : 8;

    /** Callsign */
    std::array<uint8_t, 4> callsign;

    /** City Pointer */
    uint16_t cityPtr : 16;

    /** Format */
    uint8_t format;

    /* */

    /** Received update of channel (Bit 0), Callsign (Bit 1+2), Format (Bit 3) */
    uint8_t receiveFlags;

    /** Received length */
    uint16_t receivedLength;
} IrdsUpdateData;

/**
 * @brief In-Receiver Database System new station data information
 *
 * This structure contains new station data information.
 */
static struct irdsNewStationData {
    /* */

    /** Grid number: center grid of updated region (0..16383) */
    uint16_t grid;

    /** Channel */
    uint8_t channel;

    /** Callsign */
    std::array<uint8_t, 4> callsign;

    /** City Pointer */
    uint16_t cityPtr;

    /** Format */
    uint8_t format;

    /* */

    /** Received update type 7 (Bit 0), ~ type 8 (bit 1), ~ type 9 (bit 2) */
    uint8_t receiveFlags;
} irdsNewStationData;

/**
 * @brief In-Receiver Database System RAM
 *
 * This structure contains the 2 kilobyte update RAM.
 *
 * Notes on RAM Header File:
 * - Maximum number of updates: (0x27F-4)/4=158
 * - First 2 bytes in RAM Header File contain Header1, the pointer to the first free entry in RAM Header File.
 * - Second 2 bytes in RAM Header File contain Header2, the pointer to the first free entry in Update Data File.
 *
 * Notes on Update Data File:
 * - Maximum number of entries: 202 with average length or 0x37F/4=127 with maximum length (7 bytes)
 *
 * Notes on New Stations File:
 * - Maximum number of entries: 600/10=60
 */
static struct irdsUpdateRam {
    /** 0x000: RAM Header File containing ROM addresses, RAM data pointers */
    std::array<uint8_t, 0x200> ramHeaderFile;

    /** 0x200: Update Data File containing Type, Update Data */
    std::array<uint8_t, 0x380> updateDataFile; // @todo 0x400???

    /** 0x600: New Stations File containing Grid No., Channel, Callsign, City Pointer, Format */
    std::array<uint8_t, 0x100> newStationsFile;

    /** 0x700: Preset Memory area */
    std::array<uint8_t, 0x0d8> presetMemoryArea;

    /** 0x7D8: General Purpose RAM area */
    std::array<uint8_t, 0x28> gnlPurposeRam;
} irdsUpdateRam;

/**
 * @brief Erase Update Data
 *
 * This function erases Updata Data.
 */
static void odaIrdsUpdateDataErase(void)
{
    uint16_t ptr = 4;

    do {
        /* check for ROM Address */
        if ((irdsUpdateRam.ramHeaderFile[ptr + 0] == (IrdsUpdateData.pointer >> 8)) &&
                (irdsUpdateRam.ramHeaderFile[ptr + 1] == (IrdsUpdateData.pointer & 0xff))) {
            /* just reset RAM Data Pointers */
            irdsUpdateRam.ramHeaderFile[ptr + 2] = 0;
            irdsUpdateRam.ramHeaderFile[ptr + 3] = 0;
        }
        ptr += 4;
    } while (ptr < sizeof(irdsUpdateRam.ramHeaderFile));
}

/**
 * @brief Update Update Data
 *
 * This function updates Updata Data.
 */
static void odaIrdsUpdateDataUpdate(void)
{
    uint16_t header1 = static_cast<uint16_t>((irdsUpdateRam.ramHeaderFile[0] << 8) | irdsUpdateRam.ramHeaderFile[1]); /* point to first free adress of the Header Area */
    uint16_t header2 = static_cast<uint16_t>((irdsUpdateRam.ramHeaderFile[2] << 8) | irdsUpdateRam.ramHeaderFile[3]); /* point to first free adress of the Update Area */

    /* Update Type */
    uint8_t update_type;
    uint8_t update_type_length;
    switch (IrdsUpdateData.receiveFlags) {
    case 1: /* Channel */
        update_type = 0;
        update_type_length = 1;
        break;
    case 2: /* Callsign */
        update_type = 1;
        update_type_length = 4;
        break;
    case 3: /* Channel + Callsign */
        update_type = 3;
        update_type_length = 5;
        break;
    case 4: /* Format */
        update_type = 2;
        update_type_length = 1;
        break;
    case 5: /* Channel + Format */
        update_type = 4;
        update_type_length = 2;
        break;
    case 6: /* Callsign + Format */
        update_type = 5;
        update_type_length = 5;
        break;
    case 7: /* Channel + Callsign + Format */
        update_type = 6;
        update_type_length = 6;
        break;
    default:
        /* do nothing */
        return;
    }

    /* check update length */
    if (IrdsUpdateData.updateLength != update_type_length) {
        return;
    }

    /* RAM Header File */
    irdsUpdateRam.ramHeaderFile[header1 + 0] = IrdsUpdateData.pointer >> 8; /* ROM Address */
    irdsUpdateRam.ramHeaderFile[header1 + 1] = IrdsUpdateData.pointer & 0xff;
    irdsUpdateRam.ramHeaderFile[header1 + 2] = header2 >> 8; /* RAM Data Pointers */
    irdsUpdateRam.ramHeaderFile[header1 + 3] = header2 & 0xff;
    header1++;
    irdsUpdateRam.ramHeaderFile[0] = header1 >> 8;
    irdsUpdateRam.ramHeaderFile[1] = header1 & 0xff;

    /* Update Data File */
    irdsUpdateRam.updateDataFile[header2 - 0x200] = update_type; /* Type */
    header2++;
    if ((IrdsUpdateData.receiveFlags & (1 << 0)) != 0) {
        irdsUpdateRam.updateDataFile[header2 - 0x200] = IrdsUpdateData.channel;
        header2++;
    }
    if ((IrdsUpdateData.receiveFlags & (1 << 1)) != 0) {
        irdsUpdateRam.updateDataFile[header2 - 0x200] = IrdsUpdateData.callsign[0];
        header2++;
        irdsUpdateRam.updateDataFile[header2 - 0x200] = IrdsUpdateData.callsign[1];
        header2++;
        irdsUpdateRam.updateDataFile[header2 - 0x200] = IrdsUpdateData.callsign[2];
        header2++;
        irdsUpdateRam.updateDataFile[header2 - 0x200] = IrdsUpdateData.callsign[3];
        header2++;
    }
    if ((IrdsUpdateData.receiveFlags & (1 << 2)) != 0) {
        irdsUpdateRam.updateDataFile[header2 - 0x200] = IrdsUpdateData.format;
        header2++;
    }
    irdsUpdateRam.ramHeaderFile[2] = header2 >> 8;
    irdsUpdateRam.ramHeaderFile[3] = header2 & 0xff;
}

/**
 * @brief Erase New Station
 *
 * This function erases New Station.
 */
static void odaIrdsNewStationDataErase(void)
{
    uint16_t ptr = 0;
    do {
        /* check if entry already exists */
        if (memcmp(&irdsUpdateRam.newStationsFile[ptr], &irdsNewStationData, 10) == 0) {
            memset(&irdsUpdateRam.newStationsFile[ptr], 0, 10);
            return;
        }
        ptr += 10;
    } while (ptr < sizeof(irdsUpdateRam.newStationsFile));
}

/**
 * @brief Update New Station
 *
 * This function updates New Station.
 */
static void odaIrdsNewStationDataUpdate(void)
{
    uint8_t empty[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    uint16_t first_free = 0xffff;

    /* search if this entry already exists */
    uint16_t ptr = 0;
    do {
        /* remember first free entry */
        if ((first_free == 0xffff) && (memcmp(&irdsUpdateRam.newStationsFile[ptr], &empty, 10) == 0)) {
            first_free = ptr;
        }
        /* check if entry already exists */
        if (memcmp(&irdsUpdateRam.newStationsFile[ptr], &irdsNewStationData, 10) == 0) {
            return;
        }
        ptr += 10;
    } while (ptr < sizeof(irdsUpdateRam.newStationsFile));

    /* entry does not exists */
    if (first_free == 0xffff) {
        /* overwrite oldest entry */
        /* the standard gives no clue which the oldest entry is, so just do nothing here... */
    } else {
        memcpy(&irdsUpdateRam.newStationsFile[first_free], &irdsNewStationData, 10);
    }
}

void OdaIrds::decodeA(uint16_t pi, uint16_t aid, uint8_t ac, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(ac <= 0x1f);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    switch (ac) {
    case 0:
        /* update identifier */
        IrdsUpdateData.grid = blk3 >> 2;
        IrdsUpdateData.coverage = blk3 & 3;
        IrdsUpdateData.updateLength = blk4;

        IrdsUpdateData.receiveFlags = 0;
        IrdsUpdateData.receivedLength = 0;
        break;
    case 1:
        /* update data (channel) */
        IrdsUpdateData.pointer = blk3;
        IrdsUpdateData.channel = blk4 >> 8;
        if ((blk4 & 0xff) != 0) { /* unused */
            decodeIssue.emit("ODA-IRDS: AC=1: unused");
        }

        IrdsUpdateData.receiveFlags |= (1 << 0);
        IrdsUpdateData.receivedLength += 1;
        break;
    case 2:
        /* update data (callsign) */
        IrdsUpdateData.pointer = blk3;
        IrdsUpdateData.callsign[0] = blk4 >> 8;
        IrdsUpdateData.callsign[1] = blk4 & 0xff;

        IrdsUpdateData.receiveFlags |= (1 << 1);
        IrdsUpdateData.receivedLength += 2;
        break;
    case 3:
        /* update data (callsign) */
        IrdsUpdateData.pointer = blk3;
        IrdsUpdateData.callsign[2] = blk4 >> 8;
        IrdsUpdateData.callsign[3] = blk4 & 0xff;

        IrdsUpdateData.receiveFlags |= (1 << 2);
        IrdsUpdateData.receivedLength += 2;
        break;
    case 4:
        /* update data (format) */
        IrdsUpdateData.pointer = blk3;
        IrdsUpdateData.format = blk4 & 0xff;
        if ((blk4 >> 8) != 0) { /* unused */
            decodeIssue.emit("ODA-IRDS: AC=4: unused");
        }

        IrdsUpdateData.receiveFlags |= (1 << 3);
        IrdsUpdateData.receivedLength += 1;
        break;
    case 5:
        /* erase rom record */
        IrdsUpdateData.pointer = blk3;
        if (((blk4 >> 8) != 0) || ((blk4 & 0xff) != 0)) { /* unused, unused */
            decodeIssue.emit("ODA-IRDS: AC=5: unused");
        }

        IrdsUpdateData.receiveFlags = 0;
        IrdsUpdateData.receivedLength = 0;
        odaIrdsUpdateDataErase();
        break;
    case 6:
        /* erase ram record */
        irdsNewStationData.callsign[0] = blk3 >> 8;
        irdsNewStationData.callsign[1] = blk3 & 0xff;
        irdsNewStationData.callsign[2] = blk4 >> 8;
        irdsNewStationData.callsign[3] = blk4 & 0xff;

        irdsNewStationData.receiveFlags = 0;
        odaIrdsNewStationDataErase();
        break;
    case 7:
        /* new record (channel, format) */
        irdsNewStationData.grid = blk3;
        irdsNewStationData.channel = blk4 >> 8;
        irdsNewStationData.format = blk4 & 0xff;

        irdsNewStationData.receiveFlags = 0; /* transmission likely starts with update type 7 */
        irdsNewStationData.receiveFlags |= (1 << 0);
        break;
    case 8:
        /* new record (callsign) */
        irdsNewStationData.callsign[0] = blk3 >> 8;
        irdsNewStationData.callsign[1] = blk3 & 0xff;
        irdsNewStationData.callsign[2] = blk4 >> 8;
        irdsNewStationData.callsign[3] = blk4 & 0xff;

        irdsNewStationData.receiveFlags |= (1 << 1);
        break;
    case 9:
        /* new record (city pointer) */
        irdsNewStationData.cityPtr = blk3;
        if (blk4 != 0) { /* unused */
            decodeIssue.emit("ODA-IRDS: AC-9: unused");
        }

        irdsNewStationData.receiveFlags |= (1 << 2);
        if (irdsNewStationData.receiveFlags == 7) {
            odaIrdsNewStationDataUpdate(); /* transmission likely ends with update type 9 */
        }
        break;
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
        /* reserved for future use */
        decodeIssue.emit("ODA-IRDS: AC=10..30: reserved for future use");
        break;
    case 31:
        /* eom */
        IrdsUpdateData.eom = blk3 >> 8;
        if ((IrdsUpdateData.eom != 0xff) || ((blk3 & 0xff) != 0) || (blk4 != 0)) { /* EOM, unused, unused */
            decodeIssue.emit("ODA-IRDS: AC=31: unused");
        }
        if (IrdsUpdateData.eom == 0xff) {
            if (IrdsUpdateData.updateLength == IrdsUpdateData.receivedLength) {
                /* reception is complete */
                odaIrdsUpdateDataUpdate();
            } else {
                /* reception is incomplete, do nothing */
            }
        }

        break;
    }
}

void OdaIrds::decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(msg <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    IrdsUpdateData.romClassNumber = (msg >> 11) & 0x1f;
    IrdsUpdateData.updateTransmissionSerialNumber = (msg >> 1) & 0x3ff;
    IrdsUpdateData.scopeFlag = msg & 1;
}

void OdaIrds::registerHandler()
{
    /* register ODA */
    OpenDataApplications::onDecodeA.connect(sigc::ptr_fun(decodeA));
    OpenDataApplications::onDecodeIdent.connect(sigc::ptr_fun(decodeIdent));
}

OdaIrds & odaIrds()
{
    /* singleton */
    static OdaIrds entry;
    return entry;
}

}
