/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <ISO/62106.h>

#include "iso_62106_export.h"

namespace ISO62106 {

/** In-Receiver Database System (I-RDS) */
class ISO_62106_EXPORT OdaIrds
{
public:
    explicit OdaIrds();

    /** I-RDS AID */
    static constexpr uint16_t applicationId = 0xc563;

    /**
     * @brief Decodes and handles received I-RDS information
     *
     * This function decodes and handles received I-RDS information.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] ac Address code
     * @param[in] blk3 Block 3 data
     * @param[in] blk4 Block 4 data
     */
    static void decodeA(uint16_t pi, uint16_t aid, uint8_t ac, uint16_t blk3, uint16_t blk4);

    /**
     * @brief Decode and handles received I-RDS assign message
     *
     * This function decodes and handles received I-RDS assign messages.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] msg Message bits
     */
    static void decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg);

    /** register handler */
    static void registerHandler();
};

/**
 * Return reference to I-RDS entry.
 *
 * @return Reference to I-RDS entry.
 */
extern ISO_62106_EXPORT OdaIrds & odaIrds(); /* singleton */

}
