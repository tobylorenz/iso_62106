/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "InHouse.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

InHouse::InHouse(const RdsProgram & parent) :
    m_parent(parent)
{
}

void InHouse::decodeA(uint8_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* assert */
    assert(blk2 <= 0x1f);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* call event handler */
    onUpdateA.emit(m_parent.programmeIdentification.value(), blk2, blk3, blk4);
}

void InHouse::decodeB(uint8_t blk2, uint16_t blk4)
{
    /* assert */
    assert(blk2 <= 0x1f);
    assert(blk4 <= 0xffff);

    /* call event handler */
    onUpdateB.emit(m_parent.programmeIdentification.value(), blk2, blk4);
}

sigc::signal<void, uint16_t, uint8_t, uint16_t, uint16_t> InHouse::onUpdateA;

sigc::signal<void, uint16_t, uint8_t, uint16_t> InHouse::onUpdateB;

}
