/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <sqlite3.h>

#include "RdsProgram.h"
#include "RdsReceiver.h"

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/**
 * Language Database
 *
 * Several services offer local string conversion.
 * They all use the language database to retrieve native strings.
 * The services are: ECC, EON, LIC, ODA-RTP, PTY.
 */
class ISO_62106_EXPORT Database
{
public:
    Database();
    virtual ~Database();

    /**
     * @brief Open language database
     *
     * This function opens the database for the given language.
     *
     * @param[in] language Language (e.g. "de_DE")
     * @return True on success, false otherwise
     */
    bool open(const std::string language = "en_CEN");

    /** Close language database */
    void close();

    /**
     * Get database handle
     *
     * @return Database handle
     */
    sqlite3 * handle() const;

private:

    /** Database handle */
    sqlite3 * m_handle;
};

/**
 * Returns a reference to the database object.
 *
 * @return Database object
 */
extern ISO_62106_EXPORT Database & database(); /* singleton */

}
