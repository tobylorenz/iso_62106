/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>

#include <sigc++-2.0/sigc++/signal.h>

#include "RdsProgram.h"

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/**
 * @brief Structure for RDS receiver
 *
 * This structure contains all information corresponding to one RDS receiver.
 */
class ISO_62106_EXPORT RdsReceiver
{
public:
    RdsReceiver();

    /**
     * @brief Statistics counter
     *
     * This variable contains a statistics counter for received RDS groups and versions.
     *
     * @return group counts
     */
    std::array<std::array<uint32_t, 2>, 16> groupTypeCnt() const;

    /**
     * @brief Current RDS program
     *
     * This points to the current RDS program.
     *
     * @return current RDS program
     */
    RdsProgram * currentProgram() const;

    /** event handler on program change */
    sigc::signal<void, uint16_t> onChange;

    /** event handler on program update */
    sigc::signal<void, uint16_t> onUpdate;

    /**
     * @brief Decode and handle RDS message
     *
     * This function decodes and handles RDS messages.
     *
     * @param[in] pi Block 1 data (contains PI code)
     * @param[in] blk2 Block 2 data
     * @param[in] blk3 Block 3 data (contains PI code in type B)
     * @param[in] blk4 Block 4 data
     */
    void decode(uint16_t pi, uint16_t blk2, uint16_t blk3, uint16_t blk4);

private:
    /**
     * @brief Statistics counter
     *
     * This variable contains a statistics counter for received RDS groups and versions.
     */
    std::array<std::array<uint32_t, 2>, 16> m_groupTypeCnt;

    /**
     * @brief Current RDS program
     *
     * This points to the current RDS program.
     */
    RdsProgram * m_currentProgram;
};

}
