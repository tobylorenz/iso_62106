/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ProgrammeItemNumber.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

ProgrammeItemNumber::ProgrammeItemNumber(const RdsProgram & parent) :
    m_parent(parent),
    m_day(0),
    m_hour(0),
    m_minute(0)
{
}

uint8_t ProgrammeItemNumber::day() const
{
    return m_day;
}

uint8_t ProgrammeItemNumber::hour() const
{
    return m_hour;
}

uint8_t ProgrammeItemNumber::minute() const
{
    return m_minute;
}

void ProgrammeItemNumber::decode(uint8_t day, uint8_t hour, uint8_t minute)
{
    /* check */
    assert(day <= 0x1f);
    assert(hour < 0x1f);
    assert(minute < 0x3f);

    /* ignore the rest of the message */
    if (day == 0) {
        return;
    }

    /* change check */
    if ((this->m_day != day) || (this->m_hour != hour) || (this->m_minute != minute)) {
        this->m_day = day;
        this->m_hour = hour;
        this->m_minute = minute;

        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> ProgrammeItemNumber::onChange;

sigc::signal<void, uint16_t> ProgrammeItemNumber::onUpdate;

}
