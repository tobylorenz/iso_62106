/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Extended Country Code (ECC) */
class ISO_62106_EXPORT ExtendedCountryCode
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ExtendedCountryCode(const RdsProgram & parent);

    /**
     * Extended Country Code (ECC)
     *
     * @return value
     */
    uint8_t value() const;

    /**
     * ITU region
     *
     * @return ITU region (1..3)
     */
    uint8_t ituRegion() const; // 1..3

    /**
     * Return the ISO 3166 country code.
     *
     * @return ISO 3166 country code
     */
    std::string isoCountryCode() const;

    /**
     * @brief Return country name
     *
     * This function returns the country name.
     *
     * @return Country name
     */
    std::string country() const;

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

    /**
     * @brief Decodes and handles received EC Codes
     *
     * This function decodes and handles received EC Codes.
     *
     * @param[in] ecc EC Code
     */
    void decode(uint8_t ecc);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Extended Country Code (ECC) */
    uint8_t m_value : 8;

    /** ISO 3166 country code */
    std::string m_isoCountryCode; // encoding: ISO 646 (ASCII)

    /** ITU region */
    uint8_t m_ituRegion : 2; // 1..3

    /**
     * @brief Set ISO and ITU based on CC and ECC
     *
     * This function sets ISO and ITU based on CC and ECC.
     */
    void setIsoItu();
};

}
