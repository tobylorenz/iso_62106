/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <map>
#include <vector>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Alternative Frequencies (AF) */
class ISO_62106_EXPORT AlternativeFrequencies
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit AlternativeFrequencies(const RdsProgram & parent);

    /** mapped frequency */
    struct MappedFrequency {
        MappedFrequency();

        /* relational operators */
        bool operator==(const MappedFrequency & rhs) const;

        /**
         * frequency in kHz
         *
         *   - 87600 .. 107900 is VHF
         *   - 530 .. 1610 is MF
         *   - 153 .. 279 is LF
         */
        uint32_t frequency; // unit: kHz

        /**
         * same programme (or regional variant)
         *   - true: x is an alternative frequency, and is the same programme
         *   - false: x is an alternative frequency, and is a regional variant
         */
        bool sameProgramme;
    };

    /**
     * Get map from tuned frequency to mapped frequencies.
     *
     * All frequencies including the key value are in kHz.
     *
     * Key 0 is used to store method A receptions.
     * Method A receptions are always related to the currently tuned frequency.
     *
     * @return Alternative Frequencies map
     */
    std::map<uint32_t, std::vector<MappedFrequency>> mappedFrequencies() const;

    /** event handler on change */
    static sigc::signal<void, uint16_t, uint32_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t, uint32_t> onUpdate;

    /**
     * @brief Handles received Alternative Frequencies codes
     *
     * This function handles received Alternative Frequencies.
     *
     * @param[in] af1 Alternative Frequency 1
     * @param[in] af2 Alternative Frequency 2
     */
    void decode(uint8_t af1, uint8_t af2);

    /**
     * get frequency in kHz
     *
     * @param[in] af Alternative Frequency code
     * @param[in] lfMf Frequency is LF/MF (instead of VHF)
     * @param[in] itu ITU region (0=invalid, 1..3)
     *
     * @return frequency in kHz, 0 if error
     */
    static uint32_t frequencyKHz(uint8_t af, bool lfMf = false, uint8_t itu = 0);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** pair of two AFs */
    using Pair = std::pair<uint8_t, uint8_t>;

    /** last receptions */
    std::vector<Pair> m_rx;

    /** expected number of Alternative Frequencies */
    uint8_t m_expectedAfCount;

    /** actual number of Alternative Frequencies */
    uint8_t m_actualAfCount;

    /** method B: tuning frequency code */
    uint8_t m_bTuningFrequency;

    /** method B: valid (=all pairs have tuning frequency in) */
    bool m_bValid;

    /** method B: tuned frequencies to mapped frequencies */
    std::map<uint32_t, std::vector<MappedFrequency>> m_mappedFrequencies;

    /** process reception using method A */
    void processMethodA();

    /** process reception using method B */
    void processMethodB();
};

}
