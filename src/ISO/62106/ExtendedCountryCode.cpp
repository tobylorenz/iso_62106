/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ExtendedCountryCode.h"

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <sstream>

#include <sqlite3.h>

#include "Database.h"
#include "RdsProgram.h"

namespace ISO62106 {

ExtendedCountryCode::ExtendedCountryCode(const RdsProgram & parent) :
    m_parent(parent),
    m_value(0),
    m_isoCountryCode(),
    m_ituRegion(0)
{
}

uint8_t ExtendedCountryCode::value() const
{
    return m_value;
}

uint8_t ExtendedCountryCode::ituRegion() const
{
    return m_ituRegion;
}

std::string ExtendedCountryCode::isoCountryCode() const
{
    return m_isoCountryCode;
}

std::string ExtendedCountryCode::country() const
{
    /* prepare SQL query */
    std::stringstream sql;
    sql
            << std::hex << std::setfill('0') << std::uppercase
            << "select NAME from CC where ECC='"
            << std::setw(2) << static_cast<uint16_t>(m_value)
            << "' and CCD='"
            << std::setw(1) << static_cast<uint16_t>(m_parent.programmeIdentification.countryCode())
            << "'";

    /* execute SQL query */
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(database().handle(), sql.str().c_str(), sizeof(sql), &stmt, nullptr);
    std::string str;
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        str = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    }

    /* finish SQL */
    (void) sqlite3_finalize(stmt);

    return str;
}

void ExtendedCountryCode::decode(uint8_t ecc)
{
    /* check */
    assert(ecc <= 0xff);

    /* change check */
    if (m_value != ecc) {
        /* ECC */
        m_value = ecc;

        /* complete ISO and ITU */
        setIsoItu();

        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> ExtendedCountryCode::onChange;

sigc::signal<void, uint16_t> ExtendedCountryCode::onUpdate;

void ExtendedCountryCode::setIsoItu()
{
    /* clear old entries */
    m_isoCountryCode.clear();
    m_ituRegion = 0;

    /* prepare SQL query */
    std::stringstream sql;
    sql
            << std::hex << std::setfill('0') << std::uppercase
            << "select ISO,ITU from CC where ECC='"
            << std::setw(2) << static_cast<uint16_t>(m_value)
            << "' and CCD='"
            << std::setw(1) << static_cast<uint16_t>(m_parent.programmeIdentification.countryCode())
            << "'";

    /* execute SQL query */
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(database().handle(), sql.str().c_str(), sizeof(sql), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        m_isoCountryCode = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
        m_ituRegion = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
    }

    /* finish SQL */
    (void) sqlite3_finalize(stmt);
}

}
