/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ProgrammeType.h"

#include <cassert>
#include <iomanip>
#include <sstream>

#include <sqlite3.h>

#include "Database.h"
#include "RdsProgram.h"

namespace ISO62106 {

ProgrammeType::ProgrammeType(const RdsProgram & parent) :
    m_parent(parent),
    m_value(0)
{
}

uint8_t ProgrammeType::value() const
{
    return m_value;
}

std::string ProgrammeType::term(size_t displayLength, uint8_t rbds)
{
    /* prepare SQL query */
    std::stringstream sql;
    sql << "select " << ((rbds == 0) ? "RDS" : "RBDS");
    if (displayLength <= 8) {
        sql << "8";
    } else if (displayLength <= 16) {
        sql << "16";
    }
    sql << " from PTY where PTY=" << static_cast<uint16_t>(m_value);

    /* execute SQL query */
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(database().handle(), sql.str().c_str(), sizeof(sql), &stmt, nullptr);
    std::string str;
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        str = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    }

    /* finish SQL */
    (void) sqlite3_finalize(stmt);

    return str;
}

void ProgrammeType::decode(uint8_t pty)
{
    /* check */
    assert(pty <= 0x1f);

    /* change check */
    if (m_value != pty) {
        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());

        m_value = pty;
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> ProgrammeType::onChange;

sigc::signal<void, uint16_t> ProgrammeType::onUpdate;

}
