/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Decoder Identification (DI) */
class ISO_62106_EXPORT DecoderIdentification
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit DecoderIdentification(const RdsProgram & parent);

    /**
     * Code 0: mono / stereo
     *
     * @return true if stereo, false otherwise
     */
    bool stereo() const;

    /**
     * Code 1: not artificial head / atrtificial head
     *
     * @return true if artificial head, false otherwise
     */
    bool artificialHead() const;

    /**
     * Code 2: not compressed / compressed
     *
     * @return true if compressed, false otherwise
     */
    bool compressed() const;

    /**
     * Code 3: static PTY / dynamically switched PTY
     *
     * @return true if dynamically switched PTY, false otherwise
     */
    bool dynamicallySwitchedPty() const;

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

    /**
     * @brief Decodes and handles received DI codes
     *
     * This function decodes and handles received DI flags.
     *
     * @param[in] c DI segment address
     * @param[in] di DI segment
     */
    void decode(uint8_t c, bool di);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Code 0: mono / stereo */
    bool m_stereo : 1;

    /** Code 1: not artificial head / atrtificial head */
    bool m_artificialHead : 1;

    /** Code 2: not compressed / compressed */
    bool m_compressed : 1;

    /** Code 3: static PTY / dynamically switched PTY */
    bool m_dynamicallySwitchedPty : 1;
};

}
