/* create tables */
.read create_tables.sql

/* define CSV format */
.mode list
.separator ;

/* import CSV data */
.import en_US/CC.DAT CC
.import en_US/LIC.DAT LIC
.import en_US/MS.DAT MS
.import en_US/PTY.DAT PTY
.import en_US/RTP.DAT RTP

/* ready */
.quit
