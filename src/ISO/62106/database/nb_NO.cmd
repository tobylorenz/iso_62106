/* create tables */
.read create_tables.sql

/* define CSV format */
.mode list
.separator ;

/* import CSV data */
.import en_CEN/CC.DAT CC
.import en_CEN/LIC.DAT LIC
.import en_CEN/MS.DAT MS
.import nb_NO/PTY.DAT PTY
.import en_CEN/RTP.DAT RTP

/* ready */
.quit
