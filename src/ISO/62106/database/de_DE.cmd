/* create tables */
.read create_tables.sql

/* define CSV format */
.mode list
.separator ;

/* import CSV data */
.import de_DE/CC.DAT CC
.import de_DE/LIC.DAT LIC
.import de_DE/MS.DAT MS
.import de_DE/PTY.DAT PTY
.import de_DE/RTP.DAT RTP

/* ready */
.quit
