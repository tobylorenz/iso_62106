/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Radio Paging (RP) */
class ISO_62106_EXPORT RadioPaging
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit RadioPaging(const RdsProgram & parent);

    /**
     * @brief Process RP message
     *
     * This function processes RP messages.
     *
     * @param[in] ab Paging A/B
     * @param[in] sa Paging segment address code
     * @param[in] p1 Paging data in blk 3
     * @param[in] p2 Paging data in blk 4
     */
    void decode(uint8_t ab, uint8_t sa, uint16_t p1, uint16_t p2);

    /**
     * @brief Process Paging Identification
     *
     * This function processes Paging Identification.
     *
     * @param[in] id Paging Identification
     */
    void decodeIdent(uint16_t id);

    /**
     * @brief Process Radio Paging Code
     *
     * This function processes Radio Paging Codes.
     *
     * @param[in] rpc Ragio Paging Code
     */
    void decodeRpc(uint8_t rpc);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** RP last A/B flag */
    uint8_t m_lastAb : 1;

    /** RP last Paging segment address code */
    uint8_t m_lastSa : 4;

    /** RP type of additional message */
    uint8_t m_type : 3;
    /* t=0: No additional message */
    /* t=1: 10 digit numeric message */
    /* t=2: 18 digit numeric message */
    /* t=3: alphanumeric message */
    /* t=4: 15 digit numeric message in international paging */
    /* t=5: functions message in international paging */

    /** Y1..Y2: group code */
    std::array<uint8_t, 2> m_y;

    /** Z1..Z4: individual code within group */
    std::array<uint8_t, 4> m_z;

    /** A1..A18: numeric message */
    std::array<uint8_t, 18> m_a;

    /** C1..C24: message characters */
    std::array<char16_t, 24> m_c; // encoding: UCS-2

    /** X1..X3: country code according to ITU-T Rec. E212 */
    std::array<uint8_t, 3> m_x;

    /** F1..F7 : functions message in international paging */
    std::array<uint8_t, 7> m_f;
};

}
