/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "DecoderIdentification.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

DecoderIdentification::DecoderIdentification(const RdsProgram & parent) :
    m_parent(parent),
    m_stereo(false),
    m_artificialHead(false),
    m_compressed(false),
    m_dynamicallySwitchedPty(false)
{
}

bool DecoderIdentification::stereo() const
{
    return m_stereo;
}

bool DecoderIdentification::artificialHead() const
{
    return m_artificialHead;
}

bool DecoderIdentification::compressed() const
{
    return m_compressed;
}

bool DecoderIdentification::dynamicallySwitchedPty() const
{
    return m_dynamicallySwitchedPty;
}

void DecoderIdentification::decode(uint8_t c, bool di)
{
    /* check */
    assert(c <= 0x3);
//    assert(di <= 0x1);

    /* set flags */
    bool change = false;
    switch (c) {
    case 0: {
        bool oldStereo = m_stereo;
        m_stereo = di;
        change = (oldStereo != m_stereo);
    }
    break;
    case 1: {
        bool oldArtificialHead = m_artificialHead;
        m_artificialHead = di;
        change = (oldArtificialHead != m_artificialHead);
    }
    break;
    case 2: {
        bool oldCompressed = m_compressed;
        m_compressed = di;
        change = (oldCompressed != m_compressed);
    }
    break;
    case 3: {
        bool oldDynamicallySwitchedPty = m_dynamicallySwitchedPty;
        m_dynamicallySwitchedPty = di;
        change = (oldDynamicallySwitchedPty != m_dynamicallySwitchedPty);
    }
    break;
    }

    /* change check */
    if (change) {
        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> DecoderIdentification::onChange;

sigc::signal<void, uint16_t> DecoderIdentification::onUpdate;

}
