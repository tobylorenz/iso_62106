/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Enhanced Radio Text (eRT) */
class ISO_62106_EXPORT EnhancedRadioText
{
public:
    explicit EnhancedRadioText();

    /** eRT AID */
    static constexpr uint16_t applicationId = 0x6552;

    /**
     * UTF-8 flag (0=UTF-16, 1=UTF-8)
     *
     * @return true if UTF-8, false otherwise
     */
    bool utf8() const;

    /**
     * Default Text Formatting Direction
     *
     * @return Default Text Formatting Direction
     */
    uint8_t defaultTextFormattingDirection() const;

    /**
     * Character Table ID
     *
     * @return Character Table ID
     */
    uint8_t characterTableID() const;

    /**
     * String (UCS-2 encoded)
     *
     * @return string (UCS-2 encoded)
     */
    std::u16string valueUcs2() const;

    /**
     * String (UTF-8 encoded)
     *
     * @return string (UTF-8 encoded)
     */
    std::string valueUtf8() const;

    /**
     * @brief Decodes and handles received eRT information
     *
     * This function decodes and handles received eRT information.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] ac eRT byte pair address code
     * @param[in] y eRT byte numbers (block 3)
     * @param[in] z eRT byte numbers (block 4)
     */
    static void decodeA(uint16_t pi, uint16_t aid, uint8_t ac, uint16_t y, uint16_t z);

    /**
     * @brief Decodes and handles received eRT assign message
     *
     * This function decodes and handles received eRT assign messages.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] msg eRT byte pair address
     */
    static void decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

    /** register handler */
    static void registerHandler();

private:

    /** eRT: UTF-8 flag (0=UTF-16, 1=UTF-8) */
    bool m_utf8 : 1;

    /** eRT: Default Text Formatting Direction */
    uint8_t m_defaultTextFormattingDirection : 1;

    /** eRT: Character Table ID */
    uint8_t m_characterTableID : 4;

    /** eRT: String */
    std::array<char, 128> m_value; // encoding: UCS-2 or UTF-8
};

/**
 * Return reference to ODA-ERT list.
 *
 * @return Reference to ODA-ERT list
 */
extern ISO_62106_EXPORT std::map<uint16_t, EnhancedRadioText> & enhancedRadioText(); /* singleton */

/**
 * Return reference to ODA-ERT entry.
 *
 * @param[in] pi PI code
 * @return Reference to ODA-ERT entry
 */
extern ISO_62106_EXPORT EnhancedRadioText & enhancedRadioText(uint16_t pi);

}
