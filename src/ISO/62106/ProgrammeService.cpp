/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ProgrammeService.h"

#include <cassert>
#include <codecvt>
#include <cstring>
#include <locale>

#include "CharSet.h"
#include "RdsProgram.h"

namespace ISO62106 {

ProgrammService::ProgrammService(const RdsProgram & parent) :
    m_parent(parent),
    m_value()
{
}

std::u16string ProgrammService::valueUcs2() const
{
    /* convert u16string to wstring */
    std::u16string retVal;
    for (auto c = m_value.begin(); c != m_value.end() && *c; c++) {
        retVal.push_back(*c);
    }

    /* null termination */
    retVal.push_back(0);

    return retVal;
}

std::string ProgrammService::valueUtf8() const
{
    std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter;
    std::string retVal = converter.to_bytes(m_value.begin(), m_value.end());
    retVal.resize(strnlen(retVal.data(), retVal.size()));
    return retVal;
}

void ProgrammService::decode(uint8_t sa, uint8_t c1, uint8_t c2)
{
    /* check */
    assert(sa <= 0x3);
    assert(c1 <= 0xff);
    assert(c2 <= 0xff);

    /* change check */
    if (
        (m_value[sa * 2 + 0] != basicCharSet[c1]) ||
        (m_value[sa * 2 + 1] != basicCharSet[c2])) {
        m_value[sa * 2 + 0] = basicCharSet[c1];
        m_value[sa * 2 + 1] = basicCharSet[c2];

        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> ProgrammService::onChange;

sigc::signal<void, uint16_t> ProgrammService::onUpdate;

}
