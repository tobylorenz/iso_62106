/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ClockTime.h"

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include "RdsProgram.h"

namespace ISO62106 {

ClockTime::ClockTime(const RdsProgram & parent) :
    m_parent(parent),
    m_value()
{
}

RdsTime ClockTime::value() const
{
    return m_value;
}

void ClockTime::decode(uint32_t mjd, uint8_t hour, uint8_t minute, bool slto, uint8_t lto)
{
    /* check */
    assert(mjd <= 0x1ffff);
    assert(hour <= 0x1f);
    assert(minute <= 0x3f);
//    assert(slto <= 1);
    assert(lto <= 0x1f);

    /* Do not update. */
    if (mjd == 0) {
        /* RdsClock should timeout soon and provide an updated time information. */
        return;
    }

    /* validity checks */
    if (mjd > 99999) {
        return;
    }
    if (hour > 23) {
        return;
    }
    if (minute > 59) {
        return;
    }

    /* change check */
    if (
        (m_value.modifiedJulianDay() != mjd) ||
        (m_value.hour() != hour) ||
        (m_value.minute() != minute) ||
        (m_value.signLocalTimeOffset() != slto) ||
        (m_value.localTimeOffset() != lto)) {
        m_value.setModifiedJulianDay(mjd);
        m_value.setHour(hour);
        m_value.setMinute(minute);
        m_value.setSignLocalTimeOffset(slto);
        m_value.setLocalTimeOffset(lto);

        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> ClockTime::onChange;

sigc::signal<void, uint16_t> ClockTime::onUpdate;

}
