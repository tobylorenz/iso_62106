/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Oda
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(OdaDecodeAssign)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* oda assignments */
    program.openDataApplications.decodeIdent(0, 0, 0, 0); // no assignment
    program.openDataApplications.decodeIdent(15, 1, 0, 0); // temporary data fault
    program.openDataApplications.decodeIdent(11, 0, 0x1234, 0xABCD); // 11A msg=0x1234 aid=0xABCD
    program.openDataApplications.decodeIdent(11, 1, 0x5678, 0xABCD); // 11A msg=0x5678 aid=0xABCD
    BOOST_CHECK(program.openDataApplications.value()[11][0] == 0xABCD);
    BOOST_CHECK(program.openDataApplications.value()[11][1] == 0xABCD);
}

BOOST_AUTO_TEST_CASE(OdaDecodeA)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* decode with AID=0xABCD */
    program.openDataApplications.decodeA(11, 0x12, 0x3456, 0x789A);

    /* decode with AID=0 */
    program.openDataApplications.decodeA(12, 0x12, 0x3456, 0x789A);
}

BOOST_AUTO_TEST_CASE(OdaDecodeB)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* decode with AID=0xABCD */
    program.openDataApplications.decodeB(11, 0x12, 0x789A);

    /* decode with AID=0 */
    program.openDataApplications.decodeB(12, 0x12, 0x789A);
}

static void issueHandler(std::string str)
{
    BOOST_CHECK(str.find("ODA base: not implemented in ") == 0);
}

BOOST_AUTO_TEST_CASE(Handler)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* reception before ODA is identified */
    program.openDataApplications.decodeA(12, 0x00, 0x0000, 0x0000);
    program.openDataApplications.decodeB(12, 0x00, 0x0000);

    /* connect "not implemented" handler */
    ISO62106::decodeIssue.connect(sigc::ptr_fun(issueHandler));

    /* decode A */
    program.openDataApplications.decodeIdent(12, 0, 0x0000, 0xDCBA);
    program.openDataApplications.decodeA(12, 0x00, 0x0000, 0x0000);

    /* decode B */
    program.openDataApplications.decodeIdent(12, 1, 0x0000, 0xDCBA);
    program.openDataApplications.decodeB(12, 0x00, 0x0000);
}
