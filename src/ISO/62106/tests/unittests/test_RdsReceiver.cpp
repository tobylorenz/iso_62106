/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE RdsReceiver
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(Receiver)
{
    ISO62106::RdsReceiver receiver;

    /* decode A */
    uint16_t pi = 0x1234;
    uint8_t gtc = 11;
    uint8_t gtv = 0; // A
    uint16_t blk2 = (gtc << 12) | (gtv << 11);
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;
    uint32_t oldGtc = receiver.groupTypeCnt()[11][0];
    receiver.decode(pi, blk2, blk3, blk4);
    BOOST_CHECK(receiver.groupTypeCnt()[11][0] == oldGtc + 1);
    BOOST_CHECK(receiver.currentProgram()->programmeIdentification.value() == 0x1234);

    /* decode B */
    gtc = 11;
    gtv = 1; // B
    blk2 = (gtc << 12) | (gtv << 11);
    blk3 = pi;
    blk4 = 0;
    oldGtc = receiver.groupTypeCnt()[11][1];
    receiver.decode(pi, blk2, blk3, blk4);
    BOOST_CHECK(receiver.groupTypeCnt()[11][1] == oldGtc + 1);
    BOOST_CHECK(receiver.currentProgram()->programmeIdentification.value() == 0x1234);

    /* erroneous decode B */
    blk3 = 0;
    oldGtc = receiver.groupTypeCnt()[11][1];
    receiver.decode(pi, blk2, blk3, blk4);
    BOOST_CHECK(receiver.groupTypeCnt()[11][1] == oldGtc); // erroneous reception, no count

    /* different program */
    pi++;
    blk3 = pi;
    oldGtc = receiver.groupTypeCnt()[11][1];
    receiver.decode(pi, blk2, blk3, blk4);
    BOOST_CHECK(receiver.groupTypeCnt()[11][1] == oldGtc + 1);
    BOOST_CHECK(receiver.currentProgram()->programmeIdentification.value() == 0x1235);
}
