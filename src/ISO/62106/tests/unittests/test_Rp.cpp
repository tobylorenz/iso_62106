/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Rp
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(RpDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* clear */
    program.radioPaging.decode(1, 0, 0, 0);
    program.radioPaging.decode(0, 0, 0, 0);

    /* SA = 0 */
    program.radioPaging.decode(0, 0, 0x1234, 0x5678);

    /* SA = 1 */
    program.radioPaging.decode(0, 1, 0x1234, 0x5678);

    /* SA = 2 */
    program.radioPaging.decode(0, 2, 0x1234, 0x5678);

    /* SA = 3 */
    program.radioPaging.decode(0, 3, 0x1234, 0x5678);

    /* SA = 4 */
    program.radioPaging.decode(0, 4, 0x1234, 0x5678);

    /* SA = 5 */
    program.radioPaging.decode(0, 5, 0x1234, 0x5678);

    /* SA = 6 */
    program.radioPaging.decode(0, 6, 0x1234, 0x5678);

    /* SA = 7 */
    program.radioPaging.decode(0, 7, 0x1234, 0x5678);

    /* SA = 8 */
    program.radioPaging.decode(0, 8, 0x1234, 0x5678);

    /* SA = 9 */
    program.radioPaging.decode(0, 9, 0x1234, 0x5678);

    /* SA = 10 */
    program.radioPaging.decode(0, 10, 0x1234, 0x5678);

    /* SA = 11 */
    program.radioPaging.decode(0, 11, 0x1234, 0x5678);

    /* SA = 12 */
    program.radioPaging.decode(0, 12, 0x1234, 0x5678);

    /* SA = 13 */
    program.radioPaging.decode(0, 13, 0x1234, 0x5678);

    /* SA = 14 */
    program.radioPaging.decode(0, 14, 0x1234, 0x5678);

    /* SA = 15 */
    program.radioPaging.decode(0, 15, 0x1234, 0x5678);

    /* multiple messages: type=5, functions message, SA=1,3 */
    program.radioPaging.decode(0, 1, 0x1234, 0x5678);
    program.radioPaging.decode(0, 3, 0x9abc, 0xdef0);

    /* multiple messages: type=4, 15 digit numeric message in international paging, SA=7,5 */
    program.radioPaging.decode(0, 7, 0x1234, 0x5678);
    program.radioPaging.decode(0, 5, 0x9abc, 0xdef0);

    /* multiple messages: type=4, 15 digit numeric message in international paging, SA=5,6 */
    program.radioPaging.decode(0, 5, 0x1234, 0x5678);
    program.radioPaging.decode(0, 6, 0x9abc, 0xdef0);
}

BOOST_AUTO_TEST_CASE(RpDecodeIdent)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.radioPaging.decodeIdent(0);
}

BOOST_AUTO_TEST_CASE(RpDecodeRpc)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* NGD = 0 */
    program.radioPaging.decodeRpc(0 << 2);

    /* NGD = 1 */
    program.radioPaging.decodeRpc(1 << 2);

    /* NGD = 2 */
    program.radioPaging.decodeRpc(2 << 2);

    /* NGD = 3 */
    program.radioPaging.decodeRpc(3 << 2);

    /* NGD = 4 */
    program.radioPaging.decodeRpc(4 << 2);

    /* NGD = 5 */
    program.radioPaging.decodeRpc(5 << 2);

    /* NGD = 6 */
    program.radioPaging.decodeRpc(6 << 2);

    /* NGD = 7 */
    program.radioPaging.decodeRpc(7 << 2);

    /* BS = 0 */
    program.radioPaging.decodeRpc(0);

    /* BS = 1 */
    program.radioPaging.decodeRpc(1);

    /* BS = 2 */
    program.radioPaging.decodeRpc(2);

    /* BS = 3 */
    program.radioPaging.decodeRpc(3);
}
