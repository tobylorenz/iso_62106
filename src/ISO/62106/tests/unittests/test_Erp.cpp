/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Erp
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(ErpDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* anb25 */
    program.enhancedRadioPaging.decode(0, 0x12, 0x3456, 0x789A);
    BOOST_CHECK(program.enhancedRadioPaging.addressNotificationBits25() == 0x56789A);

    /* anb50b */
    program.enhancedRadioPaging.decode(1, 0x12, 0x3456, 0x789A);
    BOOST_CHECK(program.enhancedRadioPaging.addressNotificationBits50b() == 0x56789A);

    /* anb50a */
    program.enhancedRadioPaging.decode(2, 0x12, 0x3456, 0x789A);
    BOOST_CHECK(program.enhancedRadioPaging.addressNotificationBits50a() == 0x56789A);

    /* VAS */
    program.enhancedRadioPaging.decode(3, 0x12, 0x3456, 0x789A);

    /* reserved for future use */
    program.enhancedRadioPaging.decode(0, 1, 0, 0);
    program.enhancedRadioPaging.decode(0, 0, (1 << 9), 0);
    program.enhancedRadioPaging.decode(4, 0x12, 0x3456, 0x789A);

    /* OPerator Code */
    program.enhancedRadioPaging.decodeOpc(1);
    BOOST_CHECK(program.enhancedRadioPaging.operatorCode() == 1);

    /* Paging Area Code */
    program.enhancedRadioPaging.decodePac(1);
    BOOST_CHECK(program.enhancedRadioPaging.pagingAreaCode() == 1);

    /* Current Carrier Frequency */
    program.enhancedRadioPaging.decodeCcf(1);
    BOOST_CHECK(program.enhancedRadioPaging.currentCarrierFrequency() == 1);

    /* Cycle Selection */
    program.enhancedRadioPaging.decode(0, 1, 0, 0);
    BOOST_CHECK(program.enhancedRadioPaging.cycleSelection() == 1);

    /* Interval */
    program.enhancedRadioPaging.decode(0, 0, (1 << 12), 0);
    BOOST_CHECK(program.enhancedRadioPaging.interval() == 1);

    /* Sorting */
    program.enhancedRadioPaging.decode(0, 0, (1 << 10), 0);
    BOOST_CHECK(program.enhancedRadioPaging.sorting() == 1);
}
