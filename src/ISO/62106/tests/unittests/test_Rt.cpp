/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Rt
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(RtDecodeA)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* clear */
    program.radioText.decodeA(1, 0, 0, 0, 0, 0);
    program.radioText.decodeA(0, 0, 0, 0, 0, 0);

    /* set */
    program.radioText.decodeA(0, 0, 0x31, 0x32, 0x33, 0x34);
    program.radioText.decodeA(0, 1, 0x35, 0x36, 0x37, 0x38);

    /* check */
    BOOST_CHECK(program.radioText.valueUcs2()[0] == 0x0031);
    BOOST_CHECK(program.radioText.valueUcs2()[1] == 0x0032);
    BOOST_CHECK(program.radioText.valueUcs2()[2] == 0x0033);
    BOOST_CHECK(program.radioText.valueUcs2()[3] == 0x0034);
    BOOST_CHECK(program.radioText.valueUcs2()[4] == 0x0035);
    BOOST_CHECK(program.radioText.valueUcs2()[5] == 0x0036);
    BOOST_CHECK(program.radioText.valueUcs2()[6] == 0x0037);
    BOOST_CHECK(program.radioText.valueUcs2()[7] == 0x0038);

    /* set */
    program.radioText.decodeA(0, 0, 0x31, 0x42, 0x33, 0x34);
    program.radioText.decodeA(0, 1, 0x35, 0x36, 0x37, 0x48);

    /* check */
    BOOST_CHECK(program.radioText.valueUcs2()[0] == 0x0031);
    BOOST_CHECK(program.radioText.valueUcs2()[1] == 0x0042);
    BOOST_CHECK(program.radioText.valueUcs2()[2] == 0x0033);
    BOOST_CHECK(program.radioText.valueUcs2()[3] == 0x0034);
    BOOST_CHECK(program.radioText.valueUcs2()[4] == 0x0035);
    BOOST_CHECK(program.radioText.valueUcs2()[5] == 0x0036);
    BOOST_CHECK(program.radioText.valueUcs2()[6] == 0x0037);
    BOOST_CHECK(program.radioText.valueUcs2()[7] == 0x0048);
}

BOOST_AUTO_TEST_CASE(RtDecodeB)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* clear and set */
    program.radioText.decodeB(1, 0, 0x3a, 0x3b);

    /* check */
    BOOST_CHECK(program.radioText.valueUcs2()[0] == 0x003a);
    BOOST_CHECK(program.radioText.valueUcs2()[1] == 0x003b);
    BOOST_CHECK(program.radioText.valueUcs2()[2] == 0x0000);

    /* set */
    program.radioText.decodeB(1, 0, 0x3a, 0x4b);

    /* check */
    BOOST_CHECK(program.radioText.valueUcs2()[0] == 0x003a);
    BOOST_CHECK(program.radioText.valueUcs2()[1] == 0x004b);
    BOOST_CHECK(program.radioText.valueUcs2()[2] == 0x0000);
}
