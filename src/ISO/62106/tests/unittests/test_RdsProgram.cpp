/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE RdsProgram
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(Decode0A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (0 << 12) | (0 << 11); // gtc=0, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Basic tuning and switching information */
    blk2 |= (1 << 4) | (1 << 3) | (0 << 0) | (1 << 2); // ta=1, ms=1, di(0)=1
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[0][0] == 1);
    BOOST_CHECK(program.trafficAnnouncement.value() == true);
    BOOST_CHECK(program.musicSpeech.music() == true);
    BOOST_CHECK(program.decoderIdentification.stereo() == true);
}

BOOST_AUTO_TEST_CASE(Decode0B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (0 << 12) | (1 << 11); // gtc=0, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Basic tuning and switching information */
    blk2 |= (1 << 4) | (1 << 3) | (0 << 0) | (1 << 2); // ta=1, ms=1, di(0)=1
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[0][1] == 1);
    BOOST_CHECK(program.trafficAnnouncement.value() == true);
    BOOST_CHECK(program.musicSpeech.music() == true);
    BOOST_CHECK(program.decoderIdentification.stereo() == true);
}

BOOST_AUTO_TEST_CASE(Decode1A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (1 << 12) | (0 << 11); // gtc=1, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Programme Item Number and slow labelling codes */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[1][0] == 1);

    /* Paging, Extended Country Code */
    uint8_t vc = 0;
    blk3 = (vc << 12);
    program.decode(blk2, blk3, blk4);
    for (uint8_t suc = 0; suc <= 3; suc++) {
        blk4 = (1 << 10) | (suc << 8);
        program.decode(blk2, blk3, blk4);
    }

    /* not assigned */
    vc = 1;
    blk3 = (vc << 12);
    blk4 = 0;
    program.decode(blk2, blk3, blk4);

    /* Paging identification */
    vc = 2;
    blk3 = (vc << 12) | 0xc0;
    program.decode(blk2, blk3, blk4);
    for (uint8_t suc = 0; suc <= 3; suc++) {
        blk4 = (1 << 10) | (suc << 8);
        program.decode(blk2, blk3, blk4);
    }

    /* Language identification codes */
    vc = 3;
    blk3 = (vc << 12);
    program.decode(blk2, blk3, blk4);

    /* not assigned */
    vc = 4;
    blk3 = (vc << 12);
    program.decode(blk2, blk3, blk4);

    /* not assigned */
    vc = 5;
    blk3 = (vc << 12);
    program.decode(blk2, blk3, blk4);

    /* For use by broadcasters */
    vc = 6;
    blk3 = (vc << 12);
    program.decode(blk2, blk3, blk4);

    /* Identification of EWS channel */
    vc = 7;
    blk3 = (vc << 12);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode1B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (1 << 12) | (1 << 11); // gtc=1, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Programme Item Number and slow labelling codes */
    blk2 |= 1;
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[1][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode2A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (2 << 12) | (0 << 11); // gtc=2, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* RadioText */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[2][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode2B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (2 << 12) | (1 << 11); // gtc=2, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* RadioText */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[2][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode3A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (3 << 12) | (0 << 11); // gtc=3, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Application identification for Open Data */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[3][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode3B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (3 << 12) | (1 << 11); // gtc=3, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[3][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode4A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (4 << 12) | (0 << 11); // gtc=4, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Clock-time and date */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[4][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode4B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (4 << 12) | (1 << 11); // gtc=4, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[4][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode5A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (5 << 12) | (0 << 11); // gtc=5, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Transparent data channels or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[5][0] == 1);

    program.openDataApplications.decodeIdent(5, 0, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode5B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (5 << 12) | (1 << 11); // gtc=5, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Transparent data channels or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[5][1] == 1);

    program.openDataApplications.decodeIdent(5, 1, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode6A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (6 << 12) | (0 << 11); // gtc=6, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* In-house applications or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[6][0] == 1);

    program.openDataApplications.decodeIdent(6, 0, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode6B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (6 << 12) | (1 << 11); // gtc=1, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* In-house applications or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[6][1] == 1);

    program.openDataApplications.decodeIdent(6, 1, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode7A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (7 << 12) | (0 << 11); // gtc=7, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Radio paging or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[7][0] == 1);

    program.openDataApplications.decodeIdent(7, 0, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode7B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (7 << 12) | (1 << 11); // gtc=7, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[7][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode8A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (8 << 12) | (0 << 11); // gtc=8, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Traffic Message Channel or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[8][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode8B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (8 << 12) | (1 << 11); // gtc=8, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Traffic Message Channel or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[8][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode9A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (9 << 12) | (0 << 11); // gtc=9, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Emergency warning systems or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[9][0] == 1);

    program.openDataApplications.decodeIdent(9, 0, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode9B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (9 << 12) | (1 << 11); // gtc=9, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Emergency warning systems or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[9][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode10A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (10 << 12) | (0 << 11); // gtc=10, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Programme Type Name */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[10][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode10B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (10 << 12) | (1 << 11); // gtc=10, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[10][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode11A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (11 << 12) | (0 << 11); // gtc=11, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[11][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode11B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (11 << 12) | (1 << 11); // gtc=11, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[11][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode12A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (12 << 12) | (0 << 11); // gtc=12, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[12][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode12B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (12 << 12) | (1 << 11); // gtc=12, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[12][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode13A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (13 << 12) | (0 << 11); // gtc=13, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Enhanced Radio Paging or ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[13][0] == 1);

    program.openDataApplications.decodeIdent(13, 0, 0, 1);
    program.decode(blk2, blk3, blk4);
}

BOOST_AUTO_TEST_CASE(Decode13B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (13 << 12) | (1 << 11); // gtc=13, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Open Data Application */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[13][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode14A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (14 << 12) | (0 << 11); // gtc=14, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* Enhanced Other Networks information */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[14][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode14B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (14 << 12) | (1 << 11); // gtc=14, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Enhanced Other Networks information */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[14][1] == 1);
}

BOOST_AUTO_TEST_CASE(Decode15A)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (15 << 12) | (0 << 11); // gtc=15, gvc=A
    uint16_t blk3 = 0;
    uint16_t blk4 = 0;

    /* former US NRSC RDBS, now ODA */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[15][0] == 1);
}

BOOST_AUTO_TEST_CASE(Decode15B)
{
    ISO62106::RdsProgram program;
    uint16_t blk2 = (15 << 12) | (1 << 11); // gtc=15, gvc=B
    uint16_t blk3 = 0; // pi
    uint16_t blk4 = 0;

    /* Fast basic tuning and switching information */
    program.decode(blk2, blk3, blk4);
    BOOST_CHECK(program.groupTypeCount[15][1] == 1);
}
