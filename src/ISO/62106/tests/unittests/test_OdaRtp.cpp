/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE OdaRtp
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <cstring>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(WrongAid)
{
    /* normal registration */
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::RadioTextPlus::registerHandler();

    /* decode with wrong AID, just to trigger return condition */
    ISO62106::radioTextPlus(program.programmeIdentification.value()).decodeIdent(program.programmeIdentification.value(), 0x1234, (1 << 13));
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).ert() == false);
    ISO62106::radioTextPlus(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), 0x1234, (1 << 4), 0, 0);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).itemToggleBit() == false);
}

BOOST_AUTO_TEST_CASE(OdaRtpRt)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::RadioTextPlus::registerHandler();

    /* You are listening to 'House of the rising sun' by Eric Burdon    */
    /* 0----0----1----1----2----2----3----3----4----4----5----5----6--- */
    /* 0----5----0----5----0----5----0----5----0----5----0----5----0--- */
    program.radioText.decodeA(0,  0, 'Y', 'o', 'u', ' ');
    program.radioText.decodeA(0,  1, 'a', 'r', 'e', ' ');
    program.radioText.decodeA(0,  2, 'l', 'i', 's', 't');
    program.radioText.decodeA(0,  3, 'e', 'n', 'i', 'n');
    program.radioText.decodeA(0,  4, 'g', ' ', 't', 'o');
    program.radioText.decodeA(0,  5, ' ', '\'', 'H', 'o');
    program.radioText.decodeA(0,  6, 'u', 's', 'e', ' ');
    program.radioText.decodeA(0,  7, 'o', 'f', ' ', 't');
    program.radioText.decodeA(0,  8, 'h', 'e', ' ', 'r');
    program.radioText.decodeA(0,  9, 'i', 's', 'i', 'n');
    program.radioText.decodeA(0, 10, 'g', ' ', 's', 'u');
    program.radioText.decodeA(0, 11, 'n', '\'', ' ', 'b');
    program.radioText.decodeA(0, 12, 'y', ' ', 'E', 'r');
    program.radioText.decodeA(0, 13, 'i', 'c', ' ', 'B');
    program.radioText.decodeA(0, 14, 'u', 'r', 'd', 'o');
    program.radioText.decodeA(0, 15, 'n', 0, 0, 0);
    BOOST_CHECK(program.radioText.valueUtf8() == "You are listening to 'House of the rising sun' by Eric Burdon");

    /* rds_oda_rtp_decode_assign */
    program.openDataApplications.decodeIdent(11, 0, 0, ISO62106::RadioTextPlus::applicationId); // RT
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).ert() == false);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).controlBit() == 0);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).serverControlBits() == 0);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).templateNumber() == 0);

    /* rds_oda_rtp_decode */
    program.openDataApplications.decodeA(11, 0,
                                         (1 << 13) | (22 << 7) | (22 << 1), /* content=ITEM.TITLE=1 start=22 length=22 */
                                         (4 << 11) | (50 << 5) | 10); /* content=ITEM.ARTIST=4 start=50 length=10 */

    /* check ITEM.TITLE */
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(1) == "House of the rising sun");

    /* check ITEM.ARTIST */
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(4) == "Eric Burdon");
}

BOOST_AUTO_TEST_CASE(OdaRtpErt)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::EnhancedRadioText::registerHandler();
    ISO62106::RadioTextPlus::registerHandler();

    /* You are listening to 'House of the rising sun' by Eric Burdon    */
    /* 0----0----1----1----2----2----3----3----4----4----5----5----6--- */
    /* 0----5----0----5----0----5----0----5----0----5----0----5----0--- */
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeIdent(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 0);
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 0, 'Y', 'o');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 1, 'u', ' ');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 2, 'a', 'r');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 3, 'e', ' ');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 4, 'l', 'i');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 5, 's', 't');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 6, 'e', 'n');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 7, 'i', 'n');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 8, 'g', ' ');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 9, 't', 'o');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 10, ' ', '\'');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 11, 'H', 'o');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 12, 'u', 's');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 13, 'e', ' ');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 14, 'o', 'f');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 15, ' ', 't');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 16, 'h', 'e');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 17, ' ', 'r');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 18, 'i', 's');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 19, 'i', 'n');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 20, 'g', ' ');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 21, 's', 'u');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 22, 'n', '\'');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 23, ' ', 'b');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 24, 'y', ' ');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 25, 'E', 'r');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 26, 'i', 'c');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 27, ' ', 'B');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 28, 'u', 'r');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 29, 'd', 'o');
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), ISO62106::EnhancedRadioText::applicationId, 30, 'n',  0 );
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUtf8() == "You are listening to 'House of the rising sun' by Eric Burdon");

    /* rds_oda_rtp_decode_assign */
    program.openDataApplications.decodeIdent(11, 0, 1 << 13, ISO62106::RadioTextPlus::applicationId); // eRT
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).ert() == true);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).controlBit() == 0);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).serverControlBits() == 0);
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).templateNumber() == 0);

    /* rds_oda_rtp_decode */
    program.openDataApplications.decodeA(11, 0,
                                         (1 << 13) | (22 << 7) | (22 << 1), /* content=ITEM.TITLE=1 start=22 length=22 */
                                         (4 << 11) | (50 << 5) | 10); /* content=ITEM.ARTIST=4 start=50 length=10 */

    /* check ITEM.TITLE */
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(1) == "House of the rising sun");

    /* check ITEM.ARTIST */
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(4) == "Eric Burdon");

    /* delete ITEM 1, but reset ITEM 4 */
    program.openDataApplications.decodeA(11, 0,
                                         (1 << 13) | (20 << 7) | (0 << 1), /* content=ITEM.TITLE=1 start=20 length=0 => space */
                                         (4 << 11) | (50 << 5) | 10); /* content=ITEM.ARTIST=4 start=50 length=10 */
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(1).empty());
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(4) == "Eric Burdon");

    /* set ITEM1, but then reset both */
    program.openDataApplications.decodeA(11, 0,
                                         (1 << 13) | (22 << 7) | (22 << 1), /* content=ITEM.TITLE=1 start=22 length=22 */
                                         (4 << 11) | (20 << 5) | 0); /* content=ITEM.ARTIST=4 start=20 length=0 => space */
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(1).empty());
    BOOST_CHECK(ISO62106::radioTextPlus(program.programmeIdentification.value()).valueUtf8(4).empty());
}

BOOST_AUTO_TEST_CASE(OdaRtpReservedForFutureUse)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::radioTextPlus(program.programmeIdentification.value()).decodeIdent(program.programmeIdentification.value(), ISO62106::RadioTextPlus::applicationId, 1 << 14);
}

BOOST_AUTO_TEST_CASE(OdaRtpGetClass)
{
    ISO62106::RadioTextPlus::registerHandler();

    /* 27;Info;INFO.ALARM */
    BOOST_CHECK(ISO62106::RadioTextPlus::classStr(27) == "INFO.ALARM");
}

BOOST_AUTO_TEST_CASE(Instantiate)
{
    ISO62106::RadioTextPlus odaRtp;

    BOOST_CHECK(odaRtp.ert() == false);
    BOOST_CHECK(odaRtp.controlBit() == 0);
    BOOST_CHECK(odaRtp.serverControlBits() == 0);
    BOOST_CHECK(odaRtp.templateNumber() == 0);
    BOOST_CHECK(odaRtp.itemToggleBit() == 0);
    BOOST_CHECK(odaRtp.itemRunningBit() == 0);
}
