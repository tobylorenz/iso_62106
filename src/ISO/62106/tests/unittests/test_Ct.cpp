/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Ct
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(CtDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* MJD = 45218, Y=(19)82 M=9 (September) D=6 WY=4315 WN=36 WD=1 (Monday) */
    program.clockTime.decode(45218, 0, 0, 0, 0);
    BOOST_CHECK(program.clockTime.value().year() == 82); // 1982
    BOOST_CHECK(program.clockTime.value().month() == 9); // September
    BOOST_CHECK(program.clockTime.value().day() == 6);
    BOOST_CHECK(program.clockTime.value().weekNumber() == 36);
    BOOST_CHECK(program.clockTime.value().weekDay() == 1); // Monday
    BOOST_CHECK(program.clockTime.value().hour() == 0);
    BOOST_CHECK(program.clockTime.value().minute() == 0);
    BOOST_CHECK(program.clockTime.value().signLocalTimeOffset() == 0);
    BOOST_CHECK(program.clockTime.value().localTimeOffset() == 0);
    ISO62106::RdsTime rdsTime = program.clockTime.value();

    /* no updated time */
    program.clockTime.decode(0, 0, 0, 0, 0); // mjd=0
    BOOST_CHECK(program.clockTime.value() == rdsTime);

    /* different failure situations should not change the old time */
    program.clockTime.decode(100000, 0, 0, 0, 0); // mjd>99999
    BOOST_CHECK(program.clockTime.value() == rdsTime);
    program.clockTime.decode(45218, 24,  0, 0, 0); // hour>23
    BOOST_CHECK(program.clockTime.value() == rdsTime);
    program.clockTime.decode(45218,  0, 60, 0, 0); // minute>59
    BOOST_CHECK(program.clockTime.value() == rdsTime);

    /* change check */
    program.clockTime.decode(45219, 0, 0, 0, 0); // mjd
    BOOST_CHECK(program.clockTime.value().modifiedJulianDay() == 45219);
    program.clockTime.decode(45219, 1, 0, 0, 0); // hour
    BOOST_CHECK(program.clockTime.value().hour() == 1);
    program.clockTime.decode(45219, 1, 1, 0, 0); // minute
    BOOST_CHECK(program.clockTime.value().minute() == 1);
    program.clockTime.decode(45219, 1, 1, 1, 0); // slto
    BOOST_CHECK(program.clockTime.value().signLocalTimeOffset() == 1);
    program.clockTime.decode(45219, 1, 1, 1, 1); // lto
    BOOST_CHECK(program.clockTime.value().localTimeOffset() == 1);

    /* check exception - below Min */
    program.clockTime.decode(15078, 0, 0, 0, 0);
    BOOST_CHECK(program.clockTime.value().modifiedJulianDay() == 15078); // out-of-range is saved anyway

    /* Min MJD=15079, 1900-03-01 */
    program.clockTime.decode(15079, 0, 0, 0, 0);
    BOOST_CHECK(program.clockTime.value().year() == 0); // 1900
    BOOST_CHECK(program.clockTime.value().month() == 3);
    BOOST_CHECK(program.clockTime.value().day() == 1);
    BOOST_CHECK(program.clockTime.value().weekNumber() == 9);
    BOOST_CHECK(program.clockTime.value().weekDay() == 4); // Thursday

    /* Max MJD=88127, 2100-02-28 */
    program.clockTime.decode(88127, 0, 0, 0, 0);
    BOOST_CHECK(program.clockTime.value().year() == 200); // 2100
    BOOST_CHECK(program.clockTime.value().month() == 2);
    BOOST_CHECK(program.clockTime.value().day() == 28);
    BOOST_CHECK(program.clockTime.value().weekNumber() == 8);
    BOOST_CHECK(program.clockTime.value().weekDay() == 7); // Sunday

    /* check exception - above Max  */
    program.clockTime.decode(88128, 0, 0, 0, 0);
    BOOST_CHECK(program.clockTime.value().modifiedJulianDay() == 88128); // out-of-range is saved anyway
}
