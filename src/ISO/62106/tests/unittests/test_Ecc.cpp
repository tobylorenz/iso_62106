/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Ecc
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(EccDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.programmeIdentification.decode(0xd3a3);
    program.extendedCountryCode.decode(0xE0);

    BOOST_CHECK(program.extendedCountryCode.value() == 0xE0);
    BOOST_CHECK(program.extendedCountryCode.isoCountryCode() == "DE");
    BOOST_CHECK(program.extendedCountryCode.ituRegion() == 1);
}

BOOST_AUTO_TEST_CASE(EccName)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.programmeIdentification.decode(0xd3a3); // cc=0xd
    program.extendedCountryCode.decode(0xE0);
    BOOST_CHECK(program.extendedCountryCode.country() == "Germany");
    BOOST_CHECK(program.extendedCountryCode.isoCountryCode() == "DE");
}
