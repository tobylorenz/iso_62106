/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Di
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(DiDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.decoderIdentification.decode(0, 0);
    BOOST_CHECK(program.decoderIdentification.stereo() == false);
    program.decoderIdentification.decode(0, 1);
    BOOST_CHECK(program.decoderIdentification.stereo() == true);

    program.decoderIdentification.decode(1, 0);
    BOOST_CHECK(program.decoderIdentification.artificialHead() == false);
    program.decoderIdentification.decode(1, 1);
    BOOST_CHECK(program.decoderIdentification.artificialHead() == true);

    program.decoderIdentification.decode(2, 0);
    BOOST_CHECK(program.decoderIdentification.compressed() == false);
    program.decoderIdentification.decode(2, 1);
    BOOST_CHECK(program.decoderIdentification.compressed() == true);

    program.decoderIdentification.decode(3, 0);
    BOOST_CHECK(program.decoderIdentification.dynamicallySwitchedPty() == false);
    program.decoderIdentification.decode(3, 1);
    BOOST_CHECK(program.decoderIdentification.dynamicallySwitchedPty() == true);
}
