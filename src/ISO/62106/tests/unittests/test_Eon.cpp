/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Eon
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(EonDecodePi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* PI */
    program.enhancedOtherNetworks.decodePi(0x1234);
    BOOST_CHECK(program.enhancedOtherNetworks.liCnts()[0x1234] == 1);
    program.enhancedOtherNetworks.decodePi(0x1234);
    BOOST_CHECK(program.enhancedOtherNetworks.liCnts()[0x1234] == 2);

    /* counter overflow */
    program.enhancedOtherNetworks.decodePi(0x5678);
    for (int i = 3; i < 32; i++) {
        program.enhancedOtherNetworks.decodePi(0x1234);
        BOOST_CHECK(program.enhancedOtherNetworks.liCnts()[0x1234] == i);
        BOOST_CHECK(program.enhancedOtherNetworks.liCnts()[0x5678] == 1);
    }
    program.enhancedOtherNetworks.decodePi(0x1234);
    BOOST_CHECK(program.enhancedOtherNetworks.liCnts()[0x1234] == 16);
    BOOST_CHECK(program.enhancedOtherNetworks.liCnts()[0x5678] == 0);
}

BOOST_AUTO_TEST_CASE(EonDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* PS */
    program.enhancedOtherNetworks.decode(&program, 0, 0x3132);
    program.enhancedOtherNetworks.decode(&program, 1, 0x3334);
    program.enhancedOtherNetworks.decode(&program, 2, 0x3536);
    program.enhancedOtherNetworks.decode(&program, 3, 0x3738);
    BOOST_CHECK(program.programmeService.valueUcs2()[0] == 0x0031);
    BOOST_CHECK(program.programmeService.valueUcs2()[1] == 0x0032);
    BOOST_CHECK(program.programmeService.valueUcs2()[2] == 0x0033);
    BOOST_CHECK(program.programmeService.valueUcs2()[3] == 0x0034);
    BOOST_CHECK(program.programmeService.valueUcs2()[4] == 0x0035);
    BOOST_CHECK(program.programmeService.valueUcs2()[5] == 0x0036);
    BOOST_CHECK(program.programmeService.valueUcs2()[6] == 0x0037);
    BOOST_CHECK(program.programmeService.valueUcs2()[7] == 0x0038);

    /* AF(ON), AF(ON)*/
    program.enhancedOtherNetworks.decode(&program, 4, (224 << 8) | 205);
    program.enhancedOtherNetworks.decode(&program, 4, (224 << 8) | 205);

    /* Tuning freq. (TN), Mapped FM freq. x (ON) */
    program.enhancedOtherNetworks.decode(&program, 5, (1 << 8) | 5);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[0].tuningNetwork == 1);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[0].mappedFrequency == 5);
    program.enhancedOtherNetworks.decode(&program, 6, (2 << 8) | 6);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[1].tuningNetwork == 2);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[1].mappedFrequency == 6);
    program.enhancedOtherNetworks.decode(&program, 7, (3 << 8) | 7);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[2].tuningNetwork == 3);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[2].mappedFrequency == 7);
    program.enhancedOtherNetworks.decode(&program, 8, (4 << 8) | 8);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[3].tuningNetwork == 4);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[3].mappedFrequency == 8);

    /* Tuning freq. (TN), Mapped AM freq. (ON) */
    program.enhancedOtherNetworks.decode(&program, 9, (10 << 8) | 11);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[4].tuningNetwork == 10);
    BOOST_CHECK(program.enhancedOtherNetworks.mappedFrequencies()[4].mappedFrequency == 11);

    /* Unallocated */
    program.enhancedOtherNetworks.decode(&program, 10, 0);
    // BOOST_CHECK(rds_decode_status == RDS_DECODE_STATUS_NOT_ASSIGNED);
    program.enhancedOtherNetworks.decode(&program, 11, 0);
    // BOOST_CHECK(rds_decode_status == RDS_DECODE_STATUS_NOT_ASSIGNED);

    /* Linkage information: set by VC12 */
    BOOST_CHECK(program.enhancedOtherNetworks.linkageActuator() == 0);
    program.enhancedOtherNetworks.decode(&program, 12, (1 << 15) | (1 << 13)); // (1<<13) is only to trigger rfu
    // BOOST_CHECK(rds_decode_status == RDS_DECODE_STATUS_NOT_ASSIGNED);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageInformations()[0].linkageActuator == 1);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageActuator() == 1);
    /* Linkage information: clear by VC12 */
    program.enhancedOtherNetworks.decode(&program, 12, (0 << 15));
    BOOST_CHECK(program.enhancedOtherNetworks.linkageInformations()[0].linkageActuator == 0);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageActuator() == 0);
    /* Linkage information: set by VC12 */
    program.enhancedOtherNetworks.decode(&program, 12, (1 << 15));
    BOOST_CHECK(program.enhancedOtherNetworks.linkageInformations()[0].linkageActuator == 1);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageActuator() == 1);
    /* Linkage information: clear by generic LA */
    program.enhancedOtherNetworks.decodeLa(0);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageInformations()[0].linkageActuator == 0);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageActuator() == 0);
    /* Linkage information: set only generic LA */
    program.enhancedOtherNetworks.decodeLa(1);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageInformations()[0].linkageActuator == 0);
    BOOST_CHECK(program.enhancedOtherNetworks.linkageActuator() == 1);

    /* PTY, TA */
    program.enhancedOtherNetworks.decode(&program, 13, (31 << 11) | 1);
    BOOST_CHECK(program.programmeType.value() == 31);
    BOOST_CHECK(program.trafficAnnouncement.value() == 1);

    /* PIN */
    program.enhancedOtherNetworks.decode(&program, 14, (1 << 11) | (2 << 6) | 3);
    BOOST_CHECK(program.programmeItemNumber.day() == 1);
    BOOST_CHECK(program.programmeItemNumber.hour() == 2);
    BOOST_CHECK(program.programmeItemNumber.minute() == 3);
    program.enhancedOtherNetworks.decode(&program, 14, (0 << 11) | (4 << 6) | 5);
    BOOST_CHECK(program.programmeItemNumber.day() == 1);
    BOOST_CHECK(program.programmeItemNumber.hour() == 2);
    BOOST_CHECK(program.programmeItemNumber.minute() == 3);

    /* Reserved for broadcasters use */
    program.enhancedOtherNetworks.decode(&program, 15, 0);
}
