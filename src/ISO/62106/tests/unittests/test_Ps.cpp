/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Ps
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(PsDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.programmeService.decode(0, 'R', 'a');
    program.programmeService.decode(1, 'd', 'i');
    program.programmeService.decode(2, 'o', ' ');
    program.programmeService.decode(3, 'O', 'K');
    BOOST_CHECK(program.programmeService.valueUcs2()[0] == 'R');
    BOOST_CHECK(program.programmeService.valueUcs2()[1] == 'a');
    BOOST_CHECK(program.programmeService.valueUcs2()[2] == 'd');
    BOOST_CHECK(program.programmeService.valueUcs2()[3] == 'i');
    BOOST_CHECK(program.programmeService.valueUcs2()[4] == 'o');
    BOOST_CHECK(program.programmeService.valueUcs2()[5] == ' ');
    BOOST_CHECK(program.programmeService.valueUcs2()[6] == 'O');
    BOOST_CHECK(program.programmeService.valueUcs2()[7] == 'K');
    BOOST_CHECK(program.programmeService.valueUtf8() == "Radio OK");

    /* change C1 */
    program.programmeService.decode(1, 't', 'i');
    BOOST_CHECK(program.programmeService.valueUtf8() == "Ratio OK");

    /* change C2 */
    program.programmeService.decode(2, 'o', '_');
    BOOST_CHECK(program.programmeService.valueUtf8() == "Ratio_OK");
}
