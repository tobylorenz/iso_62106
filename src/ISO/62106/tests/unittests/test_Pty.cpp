/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Pty
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(PtyDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.programmeType.decode(0);
    BOOST_CHECK(program.programmeType.value() == 0);
    program.programmeType.decode(1);
    BOOST_CHECK(program.programmeType.value() == 1);
}

BOOST_AUTO_TEST_CASE(PtyGetStr)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    std::string s;

    /* RDS: 1;Nachrichtendienst;Nachrich;Nachrichten */
    program.programmeType.decode(1);
    BOOST_CHECK(program.programmeType.term(8, 0) == "News");
    BOOST_CHECK(program.programmeType.term(16, 0) == "News");
    BOOST_CHECK(program.programmeType.term(100, 0) == "News");

    /* RBDS: 1;News;News;News */
    program.programmeType.decode(1);
    BOOST_CHECK(program.programmeType.term(8, 1) == "News");
    BOOST_CHECK(program.programmeType.term(16, 1) == "News");
    BOOST_CHECK(program.programmeType.term(100, 1) == "News");

    /* these should not exists */
    program.programmeType.decode(32);
    BOOST_CHECK(program.programmeType.term(100, 0) == "No programme type or undefined");
    program.programmeType.decode(0);
    BOOST_CHECK(program.programmeType.term(100, 2) == "No programme type or undefined");
    program.programmeType.decode(32);
    BOOST_CHECK(program.programmeType.term(100, 2) == "No programme type or undefined");
}
