/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Af
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

/** implementation of IEC 62106 method A example 1 */
BOOST_AUTO_TEST_CASE(AfDecodeA1)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0xD000);
    program.programmeIdentification.decode(0xD000);
    program.extendedCountryCode.decode(0xE0);
    BOOST_REQUIRE(program.extendedCountryCode.ituRegion() == 1);

    /* IEC 62106 method A example 1 */
    program.alternativeFrequencies.decode(224 + 5, 1); // #5   AF1
    program.alternativeFrequencies.decode(2      , 3); // AF2  AF3
    program.alternativeFrequencies.decode(4      , 5); // AF4  AF5
    /* repeat */
    program.alternativeFrequencies.decode(224 + 5, 1); // #5   AF1
    program.alternativeFrequencies.decode(2      , 3); // AF2  AF3
    program.alternativeFrequencies.decode(4      , 5); // AF4  AF5

    /* check list */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = program.alternativeFrequencies.mappedFrequencies()[0];
    BOOST_REQUIRE(mf.size() == 5);
    BOOST_CHECK(mf[0].frequency == 87600);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 87700);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 87800);
    BOOST_CHECK(mf[2].sameProgramme == true);
    BOOST_CHECK(mf[3].frequency == 87900);
    BOOST_CHECK(mf[3].sameProgramme == true);
    BOOST_CHECK(mf[4].frequency == 88000);
    BOOST_CHECK(mf[4].sameProgramme == true);
}

/** implementation of IEC 62106 method A example 2 */
BOOST_AUTO_TEST_CASE(AfDecodeA2)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0xD000);
    program.programmeIdentification.decode(0xD000);
    program.extendedCountryCode.decode(0xE0);
    BOOST_REQUIRE(program.extendedCountryCode.ituRegion() == 1);

    /* IEC 62106 method A example 2 */
    program.alternativeFrequencies.decode(224 + 4,   1); // #4   AF1
    program.alternativeFrequencies.decode(2      ,   3); // AF2  AF3
    program.alternativeFrequencies.decode(4      , 205); // AF4  Filler
    /* repeat */
    program.alternativeFrequencies.decode(224 + 4,   1); // #4   AF1
    program.alternativeFrequencies.decode(2      ,   3); // AF2  AF3
    program.alternativeFrequencies.decode(4      , 205); // AF4  Filler

    /* check list */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = program.alternativeFrequencies.mappedFrequencies()[0];
    BOOST_REQUIRE(mf.size() == 4);
    BOOST_CHECK(mf[0].frequency == 87600);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 87700);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 87800);
    BOOST_CHECK(mf[2].sameProgramme == true);
    BOOST_CHECK(mf[3].frequency == 87900);
    BOOST_CHECK(mf[3].sameProgramme == true);
}

/** implementation of IEC 62106 method A example 3 */
BOOST_AUTO_TEST_CASE(AfDecodeA3)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0xD000);
    program.programmeIdentification.decode(0xD000);
    program.extendedCountryCode.decode(0xE0);
    BOOST_REQUIRE(program.extendedCountryCode.ituRegion() == 1);

    /* IEC 62106 method A example 3 */
    program.alternativeFrequencies.decode(224 + 4, 1); // #4   AF1
    program.alternativeFrequencies.decode(2    , 3); // AF2  AF3
    program.alternativeFrequencies.decode(250  , 1); // LF/MF follows  AF4
    /* repeat */
    program.alternativeFrequencies.decode(224 + 4, 1); // #4   AF1
    program.alternativeFrequencies.decode(2    , 3); // AF2  AF3
    program.alternativeFrequencies.decode(250  , 1); // LF/MF follows  AF4

    /* check list */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = program.alternativeFrequencies.mappedFrequencies()[0];
    BOOST_REQUIRE(mf.size() == 4);
    BOOST_CHECK(mf[0].frequency == 87600);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 87700);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 87800);
    BOOST_CHECK(mf[2].sameProgramme == true);
    BOOST_CHECK(mf[3].frequency == 153);
    BOOST_CHECK(mf[3].sameProgramme == true);
}

/** implementation of IEC 62106 method B example 1 */
BOOST_AUTO_TEST_CASE(AfDecodeB1)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0xD000);
    program.programmeIdentification.decode(0xD000);
    program.extendedCountryCode.decode(0xE0);
    BOOST_REQUIRE(program.extendedCountryCode.ituRegion() == 1);

    /* IEC 62106 method B example 1 */
    program.alternativeFrequencies.decode(224 + 11,  18); // #11     89.3 (same program = tuning frequency)
    program.alternativeFrequencies.decode( 18   , 120); //  89.3   99.5 (same program)
    program.alternativeFrequencies.decode( 18   , 142); //  89.3  101.7 (same program)
    program.alternativeFrequencies.decode( 13   ,  18); //  88.8   89.3 (same program)
    program.alternativeFrequencies.decode(151   ,  18); // 102.6   89.3 (regional variant)
    program.alternativeFrequencies.decode( 18   ,  15); //  89.3   89.0 (regional variant)
    /* repeat */
    program.alternativeFrequencies.decode(224 + 11,  18); // #11     89.3 (same program = tuning frequency)
    program.alternativeFrequencies.decode( 18   , 120); //  89.3   99.5 (same program)
    program.alternativeFrequencies.decode( 18   , 142); //  89.3  101.7 (same program)
    program.alternativeFrequencies.decode( 13   ,  18); //  88.8   89.3 (same program)
    program.alternativeFrequencies.decode(151   ,  18); // 102.6   89.3 (regional variant)
    program.alternativeFrequencies.decode( 18   ,  15); //  89.3   89.0 (regional variant)

    /* check list */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = program.alternativeFrequencies.mappedFrequencies()[89300];
    BOOST_REQUIRE(mf.size() == 5);
    BOOST_CHECK(mf[0].frequency == 99500);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 101700);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 88800);
    BOOST_CHECK(mf[2].sameProgramme == true);
    BOOST_CHECK(mf[3].frequency == 102600);
    BOOST_CHECK(mf[3].sameProgramme == false);
    BOOST_CHECK(mf[4].frequency == 89000);
    BOOST_CHECK(mf[4].sameProgramme == false);
}

/** implementation of IEC 62106 method B example 2 */
BOOST_AUTO_TEST_CASE(AfDecodeB2)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0xD000);
    program.programmeIdentification.decode(0xD000);
    program.extendedCountryCode.decode(0xE0);
    BOOST_REQUIRE(program.extendedCountryCode.ituRegion() == 1);

    /* IEC 62106 method B example 2 */
    program.alternativeFrequencies.decode(224 + 9, 120); // #9      99.5 (same program = tuning frequency)
    program.alternativeFrequencies.decode( 18  , 120); //  89.3   99.5 (same program)
    program.alternativeFrequencies.decode(120  , 134); //  99.5  100.9 (same program)
    program.alternativeFrequencies.decode(173  , 120); // 104.8   99.5 (regional variant)
    program.alternativeFrequencies.decode(120  ,  16); //  99.5   89.1 (regional variant)
    /* repeat */
    program.alternativeFrequencies.decode(224 + 9, 120); // #9      99.5 (same program = tuning frequency)
    program.alternativeFrequencies.decode( 18  , 120); //  89.3   99.5 (same program)
    program.alternativeFrequencies.decode(120  , 134); //  99.5  100.9 (same program)
    program.alternativeFrequencies.decode(173  , 120); // 104.8   99.5 (regional variant)
    program.alternativeFrequencies.decode(120  ,  16); //  99.5   89.1 (regional variant)

    /* check list */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = program.alternativeFrequencies.mappedFrequencies()[99500];
    BOOST_REQUIRE(mf.size() == 4);
    BOOST_CHECK(mf[0].frequency == 89300);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 100900);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 104800);
    BOOST_CHECK(mf[2].sameProgramme == false);
    BOOST_CHECK(mf[3].frequency == 89100);
    BOOST_CHECK(mf[3].sameProgramme == false);
}

/** variant of method B example 2 with LF/MF frequency */
BOOST_AUTO_TEST_CASE(AfDecodeB2LfMf)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0xD000);
    program.programmeIdentification.decode(0xD000);
    program.extendedCountryCode.decode(0xE0);
    BOOST_REQUIRE(program.extendedCountryCode.ituRegion() == 1);

    /* IEC 62106 method B example 2 */
    program.alternativeFrequencies.decode(224 + 8, 121); // #8      99.6 (same program = tuning frequency)
    program.alternativeFrequencies.decode( 18  , 121); //  89.3   99.6 (same program)
    program.alternativeFrequencies.decode(121  , 134); //  99.6  100.9 (same program)
    program.alternativeFrequencies.decode(173  , 121); // 104.8   99.6 (regional variant)
    program.alternativeFrequencies.decode(250  ,  16); // LF/MF    531 (same program)
    /* repeat */
    program.alternativeFrequencies.decode(224 + 8, 121); // #8      99.6 (same program = tuning frequency)
    program.alternativeFrequencies.decode( 18  , 121); //  89.3   99.6 (same program)
    program.alternativeFrequencies.decode(121  , 134); //  99.6  100.9 (same program)
    program.alternativeFrequencies.decode(173  , 121); // 104.8   99.6 (regional variant)
    program.alternativeFrequencies.decode(250  ,  16); // LF/MF    531 (same program)

    /* check list */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = program.alternativeFrequencies.mappedFrequencies()[99600];
    BOOST_REQUIRE(mf.size() == 4);
    BOOST_CHECK(mf[0].frequency == 89300);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 100900);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 104800);
    BOOST_CHECK(mf[2].sameProgramme == false);
    BOOST_CHECK(mf[3].frequency == 531);
    BOOST_CHECK(mf[3].sameProgramme == true);
}

BOOST_AUTO_TEST_CASE(AfFrequencies)
{
    /* VHF 1..204 in ITU 2 */
    for (uint8_t af = 1; af <= 204; af++) {
        uint32_t frequency = 87600 + (af - 1) * 100;
        BOOST_CHECK(ISO62106::AlternativeFrequencies::frequencyKHz(af) == frequency);
    }

    /* LF 1..15 in ITU 1+3 */
    for (uint8_t af = 1; af <= 15; af++) {
        uint32_t frequency = 153 + (af - 1) * 9;
        BOOST_CHECK(ISO62106::AlternativeFrequencies::frequencyKHz(af, true, 1) == frequency);
        BOOST_CHECK(ISO62106::AlternativeFrequencies::frequencyKHz(af, true, 3) == frequency);
    }

    /* MF 16..135 in ITU 1+3 */
    for (uint8_t af = 16; af <= 135; af++) {
        uint32_t frequency = 531 + (af - 16) * 9;
        BOOST_CHECK(ISO62106::AlternativeFrequencies::frequencyKHz(af, true, 1) == frequency);
        BOOST_CHECK(ISO62106::AlternativeFrequencies::frequencyKHz(af, true, 3) == frequency);
    }

    /* MF 16..124 in ITU 2 */
    for (uint8_t af = 16; af <= 124; af++) {
        uint32_t frequency = 530 + (af - 16) * 10;
        BOOST_CHECK(ISO62106::AlternativeFrequencies::frequencyKHz(af, true, 2) == frequency);
    }
}
