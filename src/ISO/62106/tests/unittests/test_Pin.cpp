/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Pin
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(PinDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    program.programmeItemNumber.decode(1, 2, 3);
    BOOST_CHECK(program.programmeItemNumber.day() == 1);
    BOOST_CHECK(program.programmeItemNumber.hour() == 2);
    BOOST_CHECK(program.programmeItemNumber.minute() == 3);

    program.programmeItemNumber.decode(0, 4, 5);
    BOOST_CHECK(program.programmeItemNumber.day() == 1);
    BOOST_CHECK(program.programmeItemNumber.hour() == 2);
    BOOST_CHECK(program.programmeItemNumber.minute() == 3);
}
