/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE RdsTime
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(mjd2ymd)
{
    int y;
    int m;
    int d;

    /* min */
    ISO62106::RdsTime::mjd2ymd(15079, y, m, d);
    BOOST_CHECK(y == 0);
    BOOST_CHECK(m == 3);
    BOOST_CHECK(d == 1);

    /* example */
    ISO62106::RdsTime::mjd2ymd(45218, y, m, d);
    BOOST_CHECK(y == 82);
    BOOST_CHECK(m == 9);
    BOOST_CHECK(d == 6);

    /* max */
    ISO62106::RdsTime::mjd2ymd(88127, y, m, d);
    BOOST_CHECK(y == 200);
    BOOST_CHECK(m == 2);
    BOOST_CHECK(d == 28);
}

BOOST_AUTO_TEST_CASE(ymd2mjd)
{
    /* min */
    BOOST_CHECK(ISO62106::RdsTime::ymd2mjd(0, 3, 1) == 15079);

    /* example */
    BOOST_CHECK(ISO62106::RdsTime::ymd2mjd(82, 9, 6) == 45218);

    /* max */
    BOOST_CHECK(ISO62106::RdsTime::ymd2mjd(200, 2, 28) == 88127);
}

BOOST_AUTO_TEST_CASE(mjd2wd)
{
    /* min */
    BOOST_CHECK(ISO62106::RdsTime::mjd2wd(15079) == 4);

    /* example */
    BOOST_CHECK(ISO62106::RdsTime::mjd2wd(45218) == 1);

    /* max */
    BOOST_CHECK(ISO62106::RdsTime::mjd2wd(88127) == 7);
}

BOOST_AUTO_TEST_CASE(wywnwd2mjd)
{
    /* min */
    BOOST_CHECK(ISO62106::RdsTime::wywnwd2mjd(0, 9, 4) == 15079);

    /* example */
    BOOST_CHECK(ISO62106::RdsTime::wywnwd2mjd(82, 36, 1) == 45218);

    /* max */
    BOOST_CHECK(ISO62106::RdsTime::wywnwd2mjd(200, 8, 7) == 88127);
}

BOOST_AUTO_TEST_CASE(mjd2wywn)
{
    int wy;
    int wn;

    /* min */
    ISO62106::RdsTime::mjd2wywn(15079, wy, wn);
    BOOST_CHECK(wy == 0);
    BOOST_CHECK(wn == 9);

    /* example */
    ISO62106::RdsTime::mjd2wywn(45218, wy, wn);
    BOOST_CHECK(wy == 82);
    BOOST_CHECK(wn == 36);

    /* max */
    ISO62106::RdsTime::mjd2wywn(88127, wy, wn);
    BOOST_CHECK(wy == 200);
    BOOST_CHECK(wn == 8);
}

BOOST_AUTO_TEST_CASE(RelationalOperators)
{
    /* comparisons */
    ISO62106::RdsTime t1;
    t1.setModifiedJulianDay(45218);
    ISO62106::RdsTime t2;
    t2.setModifiedJulianDay(45219);
    BOOST_CHECK(t1 != t2);
    BOOST_CHECK(!(t1 == t2));
    BOOST_CHECK(t1 < t2);
    BOOST_CHECK(t1 <= t2);
    BOOST_CHECK(t2 >= t2);
    BOOST_CHECK(t2 > t1);

    /* minute increments */
    ISO62106::RdsTime t3;
    t3.setModifiedJulianDay(45219);
    BOOST_CHECK(t2 == t3);
    t3.addTime(0, 0, 1); // minute++
    BOOST_CHECK(t2 < t3);
    t2.addTime(0, 0, 1); // minute++
    BOOST_CHECK(t2 == t3);

    /* comparison with varing minute/hour */
    t3.addTime(0, 0, 1); // minute++
    BOOST_CHECK(t2 < t3);
    t3.setHour(t3.hour());
    BOOST_CHECK(t2 < t3);

    /* increment overflow */
    t1.setHour(23);
    t1.setMinute(59);
    t1.addTime(0, 0, 1); // minute++
    BOOST_CHECK(t1.modifiedJulianDay() == 45219);
    BOOST_CHECK(t1.hour() == 0);
    BOOST_CHECK(t1.minute() == 0);

    /* comparison with hours */
    t1.setTime(12, 0);
    t2.setTime(13, 0);
    BOOST_CHECK(t1 < t2);
}

BOOST_AUTO_TEST_CASE(ArithmeticOperators)
{
    ISO62106::RdsTime t;

    /* addition */
    t = ISO62106::RdsTime(1, 2); // 01:02
    t.addTime(0, 3, 4); // 03:04
    BOOST_CHECK(t == ISO62106::RdsTime(4, 6)); // 04:06

    /* addition with minute overflow */
    t = ISO62106::RdsTime(0, 59);
    t.addTime(0, 0, 1); // 00:01
    BOOST_CHECK(t == ISO62106::RdsTime(1, 0)); // 01:00

    /* addition with hour overflow */
    t = ISO62106::RdsTime(20000);
    t.addTime(0, 23, 0); // 23:00
    t.addTime(0, 1, 0); // 01:00
    BOOST_CHECK(t.modifiedJulianDay() == 20001);
    BOOST_CHECK(t.hour() == 0);
    BOOST_CHECK(t.minute() == 0);

    /* substraction */
    t = ISO62106::RdsTime(4, 6); // 04:06
    t.subTime(0, 3, 4); // 03:04
    BOOST_CHECK(t == ISO62106::RdsTime(1, 2)); // 01:02

    /* substraction with minute underflow */
    t = ISO62106::RdsTime(1, 0); // 01:00
    t.subTime(0, 0, 1); // 00:01
    BOOST_CHECK(t == ISO62106::RdsTime(0, 59)); // 00:59

    /* substraction with hour underflow */
    t = ISO62106::RdsTime(20001);
    t.subTime(0, 1, 0); // 01:00
    BOOST_CHECK(t.modifiedJulianDay() == 20000);
    BOOST_CHECK(t.hour() == 23);
}

BOOST_AUTO_TEST_CASE(LocalTimeConversions)
{
    ISO62106::RdsTime utcTime1;
    utcTime1.setModifiedJulianDay(20000);
    utcTime1.setHour(12);
    utcTime1.setMinute(0);

    /* UTC + 1.5 */
    utcTime1.setSignLocalTimeOffset(0);
    utcTime1.setLocalTimeOffset(3); // 3 * 30m

    ISO62106::RdsTime localTime = utcTime1;
    localTime.toLocalTime();
    BOOST_CHECK(localTime.hour() == 13);
    BOOST_CHECK(localTime.minute() == 30);

    ISO62106::RdsTime utcTime2 = localTime;
    utcTime2.toUtcTime();
    BOOST_CHECK(utcTime1 == utcTime2);

    /* UTC - 1.5 */
    utcTime1.setSignLocalTimeOffset(1);
    utcTime1.setLocalTimeOffset(3); // 3 * 30min

    localTime = utcTime1;
    localTime.toLocalTime();
    BOOST_CHECK(localTime.hour() == 10);
    BOOST_CHECK(localTime.minute() == 30);

    utcTime2 = localTime;
    utcTime2.toUtcTime();
    BOOST_CHECK(utcTime1 == utcTime2);
}

/**
 * This test checks the assumption that every increase in MJD is reflected by an increase in the calculated day.
 * It should not happen that two MJD result in the same calculated day.
 */
BOOST_AUTO_TEST_CASE(CheckLinearity)
{
    ISO62106::RdsTime ctTime;
    uint8_t lastDay = 0;
    for (uint32_t mjd = 15079; mjd <= 88127; mjd++) {
        ctTime.setModifiedJulianDay(mjd);
        BOOST_CHECK(lastDay != ctTime.day());
        lastDay = ctTime.day();
    }
}

BOOST_AUTO_TEST_CASE(ConstructorsGettersSetters)
{
    ISO62106::RdsTime ctTime;

    /* default */
    ctTime = ISO62106::RdsTime();
    BOOST_CHECK(ctTime.valid() == false);

    /* mjd */
    ctTime = ISO62106::RdsTime(20000);
    BOOST_CHECK(ctTime.valid() == true);
    BOOST_CHECK(ctTime.modifiedJulianDay() == 20000);

    /* hour/minute */
    ctTime = ISO62106::RdsTime(8, 0); // 08:00
    BOOST_CHECK(ctTime.hour() == 8);
    BOOST_CHECK(ctTime.minute() == 0);

    /* year/month/day */
    ctTime = ISO62106::RdsTime(100, 1, 1); // 2000-01-01
    BOOST_CHECK(ctTime.year() == 100);
    BOOST_CHECK(ctTime.month() == 1);
    BOOST_CHECK(ctTime.day() == 1);
    ctTime.setDate(101, 2, 2);
    BOOST_CHECK(ctTime.year() == 101); // 2001-02-02
    BOOST_CHECK(ctTime.month() == 2);
    BOOST_CHECK(ctTime.day() == 2);
}

/**
 * The +1 day increment resulted in a wrong year.
 * This is the regression test to detect this problem.
 */
BOOST_AUTO_TEST_CASE(RegressionTest1)
{
    ISO62106::RdsTime receiveTime;
    receiveTime.setDate(2017-1900, 2, 15);
    receiveTime.setHour(16);
    receiveTime.setMinute(0);
    receiveTime.setLocalTimeOffset(2); // CET (UTC+1)

    ISO62106::RdsTime persistenceTime = receiveTime;
    persistenceTime.toLocalTime();
    BOOST_CHECK(persistenceTime.year() == (2017-1900));
    BOOST_CHECK(persistenceTime.month() == 2);
    BOOST_CHECK(persistenceTime.day() == 15);
    BOOST_CHECK(persistenceTime.hour() == 17);
    BOOST_CHECK(persistenceTime.minute() == 0);
    persistenceTime.setHour(0);
    persistenceTime.setMinute(0);
    BOOST_CHECK(persistenceTime.year() == (2017-1900));
    BOOST_CHECK(persistenceTime.month() == 2);
    BOOST_CHECK(persistenceTime.day() == 15);
    BOOST_CHECK(persistenceTime.hour() == 0);
    BOOST_CHECK(persistenceTime.minute() == 0);
    persistenceTime.addTime(1, 0, 0); // +1 day
    BOOST_CHECK(persistenceTime.year() == (2017-1900)); // resulted in (2058-1900)
    BOOST_CHECK(persistenceTime.month() == 2);
    BOOST_CHECK(persistenceTime.day() == 16); // resulted in 27
    BOOST_CHECK(persistenceTime.hour() == 0);
    BOOST_CHECK(persistenceTime.minute() == 0);
    persistenceTime.toUtcTime();
}
