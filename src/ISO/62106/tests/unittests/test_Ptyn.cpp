/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Ptyn
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(PtynDecode)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);

    /* clear */
    program.programmeTypeName.decode(1, 0, 0, 0, 0, 0);
    program.programmeTypeName.decode(0, 0, 0, 0, 0, 0);

    /* set */
    program.programmeTypeName.decode(0, 0, 'S', 'p', 'o', 'r');
    program.programmeTypeName.decode(0, 1, 't', ' ', ' ', ' ');

    /* check */
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[0] == 'S');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[1] == 'p');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[2] == 'o');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[3] == 'r');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[4] == 't');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[5] == ' ');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[6] == ' ');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[7] == ' ');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[8] == 0);
    BOOST_CHECK(program.programmeTypeName.valueUtf8() == "Sport   ");

    /* clear */
    program.programmeTypeName.decode(1, 0, 'N', 'e', 'w', 's');

    /* check */
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[0] == 'N');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[1] == 'e');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[2] == 'w');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[3] == 's');
    BOOST_CHECK(program.programmeTypeName.valueUcs2()[4] == 0);
    BOOST_CHECK(program.programmeTypeName.valueUtf8() == "News");

    /* just change some characters */
    program.programmeTypeName.decode(1, 0, 'N', 'E', 'w', 's');
    BOOST_CHECK(program.programmeTypeName.valueUtf8() == "NEws");
    program.programmeTypeName.decode(1, 0, 'N', 'E', 'W', 's');
    BOOST_CHECK(program.programmeTypeName.valueUtf8() == "NEWs");
    program.programmeTypeName.decode(1, 0, 'N', 'E', 'W', 'S');
    BOOST_CHECK(program.programmeTypeName.valueUtf8() == "NEWS");
}
