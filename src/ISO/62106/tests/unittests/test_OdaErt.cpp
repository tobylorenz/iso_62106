/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE OdaErt
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(WrongAid)
{
    /* normal registration */
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::EnhancedRadioText::registerHandler();

    /* decode with wrong AID, just to trigger return condition */
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeIdent(program.programmeIdentification.value(), 0x1234, 1);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).utf8() == false);
    ISO62106::enhancedRadioText(program.programmeIdentification.value()).decodeA(program.programmeIdentification.value(), 0x1234, 0, 0xffff, 0xffff);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[0] == 0);
}

BOOST_AUTO_TEST_CASE(OdaErtUtf16)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::EnhancedRadioText::registerHandler();

    /* rds_oda_decode_assign */
    program.openDataApplications.decodeIdent(11, 0, 0, ISO62106::EnhancedRadioText::applicationId);
    BOOST_CHECK(program.openDataApplications.value()[11][0] == ISO62106::EnhancedRadioText::applicationId);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).utf8() == false); // UCS-2
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).defaultTextFormattingDirection() == 0);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).characterTableID() == 0);

    /* rds_oda_decode_a */
    program.openDataApplications.decodeA(11, 0, 0x0052, 0x0061);
    program.openDataApplications.decodeA(11, 1, 0x0064, 0x0069);
    program.openDataApplications.decodeA(11, 2, 0x006f, 0x0000);
    /* UCS-2 */
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[0] == 'R');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[1] == 'a');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[2] == 'd');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[3] == 'i');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[4] == 'o');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[5] == 0);
    /* UTF-8 */
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUtf8().size() == 5);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUtf8() == "Radio");
}

BOOST_AUTO_TEST_CASE(OdaErtUtf8)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(0);
    ISO62106::EnhancedRadioText::registerHandler();

    /* rds_oda_decode_assign */
    program.openDataApplications.decodeIdent(11, 0, (1 << 6) | 1, ISO62106::EnhancedRadioText::applicationId); // (1<<6) is only to trigger rfu
    BOOST_CHECK(program.openDataApplications.value()[11][0] == ISO62106::EnhancedRadioText::applicationId);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).utf8() == true); // UTF-8
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).defaultTextFormattingDirection() == 0);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).characterTableID() == 0);

    /* rds_oda_decode_a */
    program.openDataApplications.decodeA(11, 0, 0x5261, 0x6469);
    program.openDataApplications.decodeA(11, 1, 0x6f00, 0x0000);
    /* UCS-2 */
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[0] == 'R');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[1] == 'a');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[2] == 'd');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[3] == 'i');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[4] == 'o');
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUcs2()[5] == 0);
    /* UTF-8 */
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUtf8().size() == 5);
    BOOST_CHECK(ISO62106::enhancedRadioText(program.programmeIdentification.value()).valueUtf8() == "Radio");
}

BOOST_AUTO_TEST_CASE(Instantiate)
{
    ISO62106::EnhancedRadioText odaErt;

    BOOST_CHECK(odaErt.utf8() == false);
    BOOST_CHECK(odaErt.defaultTextFormattingDirection() == 0);
    BOOST_CHECK(odaErt.characterTableID() == 0);
}
