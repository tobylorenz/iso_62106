/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Pi
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/62106.h"

BOOST_AUTO_TEST_CASE(PiDecode)
{
    ISO62106::RdsReceiver rdsReceiver;

    /* Reflexion 166 - Beispiele - Vierte Ziffer */
    rdsReceiver.decode(0xD361, 0, 0, 0); /* hr 1 */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD361);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD3A8, 0, 0, 0); /* RPR Eins */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD3A8);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0x1092, 0, 0, 0); /* R. Essen */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0x1092);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0x1);

    /* Reflexion 166 - Beispiele - Zweite Ziffer */
    rdsReceiver.decode(0x100A, 0, 0, 0); /* Stadtradio Stuttgart */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0x100A);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0x1);
    rdsReceiver.decode(0xD210, 0, 0, 0); /* DLF */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD210);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD301, 0, 0, 0); /* SWR1 BW */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD301);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xDC04, 0, 0, 0); /* SWR4 MA */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xDC04);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);

    /* Reflexion 166 - Beispiele - Dritte Ziffer */
    rdsReceiver.decode(0xD313, 0, 0, 0); /* Bayern 3 */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD313);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD368, 0, 0, 0); /* FFH */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD368);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD3A1, 0, 0, 0); /* SWR1 RP */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD3A1);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD220, 0, 0, 0); /* DRadio */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD220);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);

    /* Reflexion 166 - Beispiele - Vierte Ziffer */
    rdsReceiver.decode(0xD3A3, 0, 0, 0); /* SWR3 */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD3A3);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD3A5, 0, 0, 0); /* DASDING */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD3A5);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD388, 0, 0, 0); /* FFN */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD388);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD3A9, 0, 0, 0); /* RPR Zwei */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD3A9);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
    rdsReceiver.decode(0xD3AA, 0, 0, 0); /* Rockland R. */
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.value() == 0xD3AA);
    BOOST_CHECK(rdsReceiver.currentProgram()->programmeIdentification.countryCode() == 0xD);
}
