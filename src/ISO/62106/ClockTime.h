/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <ctime>

#include <sigc++-2.0/sigc++/signal.h>

#include "RdsTime.h"

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Clock Time (CT) */
class ISO_62106_EXPORT ClockTime
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ClockTime(const RdsProgram & parent);

    /**
     * Gets the value of ClockTime
     *
     * @return value of ClockTime
     */
    RdsTime value() const;

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

    /**
     * @brief Decodes and handles received CT messages
     *
     * This function decodes and handles received CT messages
     *
     * @param[in] mjd Modified Julian Day (range 0-99999)
     * @param[in] hour Hour (range 0-23)
     * @param[in] minute Minute (range 0-59)
     * @param[in] slto Sense of local time offset (0 = positive offset and 1 = negative offset)
     * @param[in] lto Local time offset (expressed in multiples of half hours within the range -15.5h to +15.5h)
     */
    void decode(uint32_t mjd, uint8_t hour, uint8_t minute, bool slto, uint8_t lto);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Clock Time (CT) */
    RdsTime m_value;
};

}
