/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Programme Type Name (PTYN) */
class ISO_62106_EXPORT ProgrammeTypeName
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ProgrammeTypeName(const RdsProgram & parent);

    /**
     * Programme Type Name (PTYN)
     *
     * @return value UCS-2 encoded
     */
    std::u16string valueUcs2() const;

    /**
     * Return the value UTF-8 encoded.
     *
     * @return value UTF-8 encoded
     */
    std::string valueUtf8() const;

    /**
     * @brief Decodes and handles received PTYN characters
     *
     * This function decodes and handles received PTYN characters.
     *
     * @param[in] ab A/B flag
     * @param[in] sa PTYN Segment Address
     * @param[in] c1 Text Character
     * @param[in] c2 Text Character
     * @param[in] c3 Text Character
     * @param[in] c4 Text Character
     */
    void decode(uint8_t ab, uint8_t sa, uint8_t c1, uint8_t c2, uint8_t c3, uint8_t c4);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Programme Type Name (PTYN) */
    std::array<char16_t, 8> m_value; // encoding: UCS-2

    /** Last A/B flag */
    uint8_t m_lastAb : 1;
};

}
