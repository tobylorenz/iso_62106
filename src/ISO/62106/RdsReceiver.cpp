/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RdsReceiver.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

RdsReceiver::RdsReceiver() :
    onUpdate(),
    m_groupTypeCnt(),
    m_currentProgram(nullptr)
{
}

std::array<std::array<uint32_t, 2>, 16> RdsReceiver::groupTypeCnt() const
{
    return m_groupTypeCnt;
}

RdsProgram * RdsReceiver::currentProgram() const
{
    return m_currentProgram;
}

void RdsReceiver::decode(uint16_t pi, uint16_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(pi <= 0xffff);
    assert(blk2 <= 0xffff);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* decode group type */
    uint8_t gtc = (blk2 >> 12) & 0xf; /* Group Type Code */
    uint8_t gtv = (blk2 >> 11) & 1; /* Group Type Version: 0=A 1=B */

    /* check identity of blk1 and blk3 */
    if ((gtv == 1) && (pi != blk3)) {
        return;
    }

    /* do statistics */
    m_groupTypeCnt[gtc][gtv]++;

    /* change check */
    uint16_t oldPi = m_currentProgram ? m_currentProgram->programmeIdentification.value() : 0;
    bool change = (oldPi != pi);
    if (change) {
        m_currentProgram = &ISO62106::rdsProgram(pi);
        m_currentProgram->programmeIdentification.decode(pi);
    }

    /* decode rest */
    m_currentProgram->decode(blk2, blk3, blk4);

    /* change check */
    if (change) {
        onChange.emit(pi);
    }

    /* inform about update */
    onUpdate.emit(pi);
}

}
