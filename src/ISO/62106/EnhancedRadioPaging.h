/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Enhanced Radio Paging (ERP) */
class ISO_62106_EXPORT EnhancedRadioPaging
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit EnhancedRadioPaging(const RdsProgram & parent);

    /**
     * ERP OPerator Code
     *
     * @return value
     */
    uint8_t operatorCode() const;

    /**
     * ERP Paging Area Code
     *
     * @return value
     */
    uint8_t pagingAreaCode() const;

    /**
     * ERP Current Carrier Frequency
     *
     * @return value
     */
    uint8_t currentCarrierFrequency() const;

    /**
     * ERP Cycle Selection
     *
     * @return value
     */
    uint8_t cycleSelection() const;
    /* cs = 0: 1 minute cycle */
    /* cs = 1: reserved for future use */
    /* cs = 2: 2 minutes cycle or mixed (even) */
    /* cs = 3: 2 minutes cycle or mixed (odd) */

    /**
     * ERP paging InTerval numbering
     *
     * @return value
     */
    uint8_t interval() const;

    /**
     * ERP message Sorting
     *
     * @return value
     */
    uint8_t sorting() const;
    /* s = 0: not sorted */
    /* s = 1: reserved for future use */
    /* s = 2: sorted in ascending order */
    /* s = 3: sorted in descending order */

    /**
     * ERP address notification bits 24..0, when only 25 bits are used
     *
     * @return value
     */
    uint32_t addressNotificationBits25() const;

    /**
     * ERP address notification bits 24..0, when 50 bits are used
     *
     * @return value
     */
    uint32_t addressNotificationBits50a() const;

    /**
     * ERP address notification bits 49..25, when 50 bits are used
     *
     * @return value
     */
    uint32_t addressNotificationBits50b() const;

    /**
     * @brief Decodes and handles received ERP information
     *
     * This function decodes and handles received ERP information.
     *
     * @param[in] st Sub type
     * @param[in] cs Cycle Selection
     * @param[in] blk3 Information field from blk3
     * @param[in] blk4 Information field from blk4
     */
    void decode(uint8_t st, uint8_t cs, uint16_t blk3, uint16_t blk4);

    /**
     * @brief Process Enhanced Paging Operator Code
     *
     * This function processes Enhanced Paging Operator Code.
     *
     * @param[in] opc Operator Code
     */
    void decodeOpc(uint8_t opc);

    /**
     * @brief Process Enhanced Paging Paging Area Code
     *
     * This function processes Enhanced Paging Paging Area Code.
     *
     * @param[in] pac Paging Area Code
     */
    void decodePac(uint8_t pac);

    /**
     * @brief Process Enhanced Paging Current Carrier Frequency
     *
     * This function processes Enhanced Paging Current Carrier Frequency.
     *
     * @param[in] ccf Current Carrier Frequency
     */
    void decodeCcf(uint8_t ccf);

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** ERP OPerator Code */
    uint8_t m_operatorCode : 4;

    /** ERP Paging Area Code */
    uint8_t m_pagingAreaCode : 6;

    /** ERP Current Carrier Frequency */
    uint8_t m_currentCarrierFrequency : 8;

    /** ERP Cycle Selection */
    uint8_t m_cycleSelection : 2;
    /* cs = 0: 1 minute cycle */
    /* cs = 1: reserved for future use */
    /* cs = 2: 2 minutes cycle or mixed (even) */
    /* cs = 3: 2 minutes cycle or mixed (odd) */

    /** ERP paging InTerval numbering */
    uint8_t m_interval : 4;

    /** ERP message Sorting */
    uint8_t m_sorting : 2;
    /* s = 0: not sorted */
    /* s = 1: reserved for future use */
    /* s = 2: sorted in ascending order */
    /* s = 3: sorted in descending order */

    /** ERP address notification bits 24..0, when only 25 bits are used */
    uint32_t m_addressNotificationBits25 : 25;

    /** ERP address notification bits 24..0, when 50 bits are used */
    uint32_t m_addressNotificationBits50a : 25;

    /** ERP address notification bits 49..25, when 50 bits are used */
    uint32_t m_addressNotificationBits50b : 25;
};

}
