/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RdsProgram.h"

#include <cassert>
#include <cstdlib>

#include "CharSet.h"

namespace ISO62106 {

RdsProgram::RdsProgram() :
    programmeIdentification(*this),
    programmeService(*this),
    programmeType(*this),
    trafficProgramme(*this),
    alternativeFrequencies(*this),
    trafficAnnouncement(*this),
    decoderIdentification(*this),
    musicSpeech(*this),
    programmeItemNumber(*this),
    extendedCountryCode(*this),
    languageIdentificationCode(*this),
    emergencyWarningSystem(*this),
    enhancedRadioPaging(*this),
    radioText(*this),
    openDataApplications(*this),
    clockTime(*this),
    transparentDataChannels(*this),
    inHouse(*this),
    radioPaging(*this),
    programmeTypeName(*this),
    enhancedOtherNetworks(*this),
    groupTypeCount()
{
}

void RdsProgram::decode(uint16_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(blk2 <= 0xffff);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* decode components in every frame */
    uint8_t gtc = (blk2 >> 12) & 0xf; /* Group Type Code */
    uint8_t gtv = (blk2 >> 11) & 1; /* Group Type Version: 0=A 1=B */
    trafficProgramme.decode((blk2 >> 10) & 1);
    programmeType.decode((blk2 >> 5) & 0x1f);

    /* do statistics */
    groupTypeCount[gtc][gtv]++;

    /* decode groups */
    assert(gtc <= 15);
    assert(gtv <= 1);
    switch (gtc) {
    case 0: /* Basic tuning and switching information */
        trafficAnnouncement.decode((blk2 >> 4) & 1);
        musicSpeech.decode((blk2 >> 3) & 1);
        decoderIdentification.decode(blk2 & 0x3, (blk2 >> 2) & 0x1);
        if (gtv == 0) {
            alternativeFrequencies.decode((blk3 >> 8) & 0xff, (blk3 >> 0) & 0xff);
        }
        programmeService.decode(blk2 & 0x3, (blk4 >> 8) & 0xff, (blk4 >> 0) & 0xff);
        break;
    case 1: /* Programme Item Number and slow labelling codes */
        programmeItemNumber.decode((blk4 >> 11) & 0x1f, (blk4 >> 6) & 0x1f, (blk4 >> 0) & 0x3f);
        if (gtv == 0) {
            enhancedOtherNetworks.decodeLa((blk3 >> 15) & 1); /* Linkage Actuator */
            radioPaging.decodeRpc(blk2 & 0x1f); /* Radio Paging Codes */
            switch ((blk3 >> 12) & 0x7) { /* Variant Code */
            case 0: /* Paging, Extended Country Code */
                enhancedRadioPaging.decodeOpc((blk3 >> 8) & 0xf); /* OPerator Code */
                extendedCountryCode.decode(blk3 & 0xff);
                if (((blk4 >> 11) & 0x1f) == 0) { /* no valid PIN */
                    if (((blk4 >> 10) & 0x1) == 0) {
                        /* sub type 0 */
                        enhancedRadioPaging.decodePac((blk4 >> 4) & 0x3f); /* Paging Area Code */
                        enhancedRadioPaging.decodeOpc(blk4 & 0xf); /* OPerator Code */
                    } else {
                        /* sub type 1 */
                        switch ((blk4 >> 8) & 0x3) { /* sub usage code */
                        case 0:
                            extendedCountryCode.decode(blk4 & 0xff);
                            break;
                        case 1:
                        case 2:
                            /* for future use */
                            decodeIssue.emit("decode: GTC=1, VC=0: for future use");
                            break;
                        case 3:
                            enhancedRadioPaging.decodeCcf(blk4 & 0xff); /* Current Carrier Frequency */
                            break;
                        }
                    }
                }
                break;
            case 1: /* not assigned */
                /* was once used for TMC identification, when ODA is not used */
                decodeIssue.emit("decode: GTC=1, VC=1: not assigned");
                break;
            case 2: /* Paging identification */
                enhancedRadioPaging.decodeOpc((blk3 >> 8) & 0xf); /* OPerator Code */
                if ((blk3 & 0xc0) != 0) {
                    decodeIssue.emit("decode: GTC=1, VC=2: not assigned");
                }
                enhancedRadioPaging.decodePac(blk3 & 0x3f); /* Paging Area Code */
                if (((blk4 >> 11) & 0x1f) == 0) { /* no valid PIN */
                    if (((blk4 >> 10) & 0x1) == 0) {
                        /* sub type 0 */
                        enhancedRadioPaging.decodePac((blk4 >> 4) & 0x3f); /* Paging Area Code */
                        enhancedRadioPaging.decodeOpc(blk4 & 0xf); /* OPerator Code */
                    } else {
                        /* sub type 1 */
                        switch ((blk4 >> 8) & 0x3) { /* sub usage code */
                        case 0:
                            extendedCountryCode.decode(blk4 & 0xff);
                            break;
                        case 1:
                        case 2:
                            /* for future use */
                            decodeIssue.emit("decode: GTC=1, VC=2, SUC=2: for future use");
                            break;
                        case 3:
                            enhancedRadioPaging.decodeCcf(blk4 & 0xff); /* Current Carrier Frequency */
                            break;
                        }
                    }
                }
                break;
            case 3: /* Language identification codes */
                languageIdentificationCode.decode(blk3 & 0xfff);
                break;
            case 4: /* not assigned */
                decodeIssue.emit("decode: GTC=1, VC=4: not assigned");
                break;
            case 5: /* not assigned */
                decodeIssue.emit("decode: GTC=1, VC=5: not assigned");
                break;
            case 6: /* For use by broadcasters */
                decodeIssue.emit("decode: GTC=1, VC=6: for use by broadcasters");
                break;
            case 7: /* Identification of EWS channel */
                emergencyWarningSystem.decodeIdent(blk3 & 0xfff);
                break;
            }
        } else {
            /* blk2 & 0x1f: Spare bits */
            if ((blk2 & 0x1f) != 0) {
                decodeIssue.emit("decode: GTC=1: spare bits");
            }
        }
        break;
    case 2: /* RadioText */
        if (gtv == 0) {
            radioText.decodeA((blk2 >> 5) & 1, (blk2 & 0xf),
                              (blk3 >> 8) & 0xff,
                              (blk3 >> 0) & 0xff,
                              (blk4 >> 8) & 0xff,
                              (blk4 >> 0) & 0xff);
        } else {
            radioText.decodeB((blk2 >> 5) & 1, (blk2 & 0xf),
                              (blk4 >> 8) & 0xff,
                              (blk4 >> 0) & 0xff);
        }
        break;
    case 3:
        if (gtv == 0) { /* Application identification for Open Data */
            openDataApplications.decodeIdent((blk2 >> 1) & 0xf, blk2 & 1, blk3, blk4);
        } else { /* Open Data Application */
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 4:
        if (gtv == 0) { /* Clock-time and date */
            clockTime.decode(static_cast<uint32_t>(((blk2 & 3) << 15) | (blk3 >> 1)),
                             static_cast<uint8_t>(((blk3 & 1) << 4) | ((blk4 >> 12) & 0xf)),
                             (blk4 >> 6) & 0x3f,
                             (blk4 >> 5) & 1,
                             blk4 & 0x1f);
        } else { /* Open Data Application */
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 5: /* Transparent data channels or ODA */
        if (gtv == 0) {
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeA(gtv, blk2 & 0x1f, blk3, blk4);
            } else {
                transparentDataChannels.decode(blk2 & 0x1f, blk3);
                transparentDataChannels.decode(blk2 & 0x1f, blk4);
            }
        } else {
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
            } else {
                transparentDataChannels.decode(blk2 & 0x1f, blk4);
            }
        }
        break;
    case 6: /* In-house applications or ODA */
        if (gtv == 0) {
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
            } else {
                inHouse.decodeA(blk2 & 0x1f, blk3, blk4);
            }
        } else {
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
            } else {
                inHouse.decodeB(blk2 & 0x1f, blk4);
            }
        }
        break;
    case 7:
        if (gtv == 0) { /* Radio paging or ODA */
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
            } else {
                radioPaging.decode((blk2 >> 4) & 1, blk2 & 0xf, blk3, blk4);
            }
        } else { /* Open Data Application */
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 8: /* Traffic Message Channel or ODA */
        if (gtv == 0) {
            openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
        } else {
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 9: /* Emergency warning systems or ODA */
        if (gtv == 0) {
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
            } else {
                emergencyWarningSystem.decode(blk2 & 0x1f, blk3, blk4);
            }
        } else {
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 10:
        if (gtv == 0) { /* Programme Type Name */
            programmeTypeName.decode((blk2 >> 4) & 1, blk2 & 1,
                                     (blk3 >> 8) & 0xff,
                                     (blk3 >> 0) & 0xff,
                                     (blk4 >> 8) & 0xff,
                                     (blk4 >> 0) & 0xff);
        } else { /* Open Data Application */
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 11: /* Open Data Application */
        if (gtv == 0) {
            openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
        } else {
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 12: /* Open Data Application */
        if (gtv == 0) {
            openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
        } else {
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 13:
        if (gtv == 0) { /* Enhanced Radio Paging or ODA */
            if (openDataApplications.value()[gtc][gtv] != 0) {
                openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
            } else {
                enhancedRadioPaging.decode(blk2 & 7, (blk2 >> 3) & 3, blk3, blk4);
            }
        } else { /* Open Data Application */
            openDataApplications.decodeB(gtc, blk2 & 0x1f, blk4);
        }
        break;
    case 14: { /* Enhanced Other Networks information */
        /* load program */
        RdsProgram * rdsProgramEon = &rdsProgram(blk4);
        enhancedOtherNetworks.decodePi(blk4);
        rdsProgramEon->trafficProgramme.decode((blk2 >> 4) & 1);
        if (gtv == 0) {
            enhancedOtherNetworks.decode(rdsProgramEon, blk2 & 0xf, blk3);
        } else {
            rdsProgramEon->trafficAnnouncement.decode((blk2 >> 3) & 1);
        }
    }
    break;
    case 15:
        if (gtv == 0) { /* former US NRSC RDBS, now ODA */
            openDataApplications.decodeA(gtc, blk2 & 0x1f, blk3, blk4);
        } else { /* Fast basic tuning and switching information */
            trafficProgramme.decode((blk4 >> 10) & 1);
            programmeType.decode((blk4 >> 5) & 0x1f);
            trafficAnnouncement.decode((blk4 >> 4) & 1);
            musicSpeech.decode((blk4 >> 3) & 1);
            decoderIdentification.decode(blk4 & 0x3, (blk4 >> 2) & 0x1);
        }
        break;
    }
}

sigc::signal<void, std::string> decodeIssue;

std::map<uint16_t, RdsProgram> & rdsProgram()
{
    /* singleton */
    static std::map<uint16_t, RdsProgram> list;
    return list;
}

RdsProgram & rdsProgram(uint16_t pi)
{
    return rdsProgram()[pi];
}

}
