/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RadioTextPlus.h"

#include <algorithm>
#include <cassert>
#include <codecvt>
#include <cstring>
#include <iomanip>
#include <locale>
#include <string>
#include <sstream>

#include "Database.h"
#include "EnhancedRadioText.h"
#include "RdsProgram.h"

namespace ISO62106 {

RadioTextPlus::RadioTextPlus() :
    m_ert(false),
    m_controlBit(0),
    m_serverControlBits(0),
    m_templateNumber(0),
    m_itemToggleBit(0),
    m_itemRunningBit(0),
    m_value()
{
}

bool RadioTextPlus::ert() const
{
    return m_ert;
}

uint8_t RadioTextPlus::controlBit() const
{
    return m_controlBit;
}

uint8_t RadioTextPlus::serverControlBits() const
{
    return m_serverControlBits;
}

uint8_t RadioTextPlus::templateNumber() const
{
    return m_templateNumber;
}

uint8_t RadioTextPlus::itemToggleBit() const
{
    return m_itemToggleBit;
}

uint8_t RadioTextPlus::itemRunningBit() const
{
    return m_itemRunningBit;
}

std::u16string RadioTextPlus::valueUcs2(uint8_t contentType) const
{
    /* check */
    assert(contentType <= 63);

    /* convert u16string to wstring */
    std::u16string retVal;
    for (auto c = m_value.at(contentType).begin(); c != m_value.at(contentType).end() && *c; c++) {
        retVal.push_back(*c);
    }

    /* null termination */
    retVal.push_back(0);

    return retVal;
}

std::string RadioTextPlus::valueUtf8(uint8_t contentType) const
{
    /* check */
    assert(contentType <= 63);

    std::string retVal;
    if (m_value.count(contentType)) {
        std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter;
        retVal = converter.to_bytes(valueUcs2(contentType));

        /* resize UTF8 */
        retVal.resize(strnlen(retVal.c_str(), retVal.size()));
    }

    return retVal;
}

std::string RadioTextPlus::classStr(uint8_t rtp)
{
    /* prepare SQL query */
    std::stringstream sql;
    sql << "select CLASS from RTP where CODE=" << static_cast<uint16_t>(rtp);
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(database().handle(), sql.str().c_str(), sizeof(sql), &stmt, nullptr);

    /* execute SQL query */
    std::string str;
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        str = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    }

    /* finish SQL */
    (void) sqlite3_finalize(stmt);

    return str;
}

void RadioTextPlus::decodeA(uint16_t pi, uint16_t aid, uint8_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(blk2 <= 0x1f);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    /* item toggle/running bits */
    radioTextPlus(pi).m_itemToggleBit = (blk2 >> 4) & 1;
    radioTextPlus(pi).m_itemRunningBit = (blk2 >> 3) & 1;

    /* item 1 */
    uint8_t contentType1 = static_cast<uint8_t>(static_cast<uint16_t>((blk2 & 7) << 3) | (blk3 >> 13));
    uint8_t startMarker1 = (blk3 >> 7) & 0x3f;
    uint8_t lengthMarker1 = (blk3 >> 1) & 0x3f;

    /* item 2 */
    uint8_t contentType2 = static_cast<uint8_t>(static_cast<uint16_t>((blk3 & 1) << 5) | (blk4 >> 11));
    uint8_t startMarker2 = (blk4 >> 5) & 0x3f;
    uint8_t lengthMarker2 = blk4 & 0x1f;

    /* clear content */
    radioTextPlus(pi).m_value[contentType1].fill(0);
    radioTextPlus(pi).m_value[contentType2].fill(0);

    /* Content 1: copy new content */
    if (radioTextPlus(pi).m_ert == 0) {
        std::copy_n(
            rdsProgram(pi).radioText.valueUcs2().begin() + startMarker1,
            lengthMarker1 + 1,
            radioTextPlus(pi).m_value[contentType1].begin());
    } else {
        std::copy_n(
            enhancedRadioText(pi).valueUcs2().begin() + startMarker1,
            lengthMarker1 + 1,
            radioTextPlus(pi).m_value[contentType1].begin());
    }

    /* Content 1: if empty, fully delete it */
    if (radioTextPlus(pi).empty(contentType1)) {
        radioTextPlus(pi).m_value.erase(contentType1);
        /* if item, then delete all items */
        if ((contentType1 >= 1) && (contentType1 <= 11)) {
            radioTextPlus(pi).deleteItems();
        }
    }

    /* Content 2: copy new content */
    if (radioTextPlus(pi).m_ert == 0) {
        std::copy_n(
            rdsProgram(pi).radioText.valueUcs2().begin() + startMarker2,
            lengthMarker2 + 1,
            radioTextPlus(pi).m_value[contentType2].begin());
    } else {
        std::copy_n(
            enhancedRadioText(pi).valueUcs2().begin() + startMarker2,
            lengthMarker2 + 1,
            radioTextPlus(pi).m_value[contentType2].begin());
    }

    /* Content 2: if empty, fully delete it */
    if (radioTextPlus(pi).empty(contentType2)) {
        radioTextPlus(pi).m_value.erase(contentType2);
        /* if item, then delete all items */
        if ((contentType2 >= 1) && (contentType2 <= 11)) {
            radioTextPlus(pi).deleteItems();
        }
    }

    /* call event handler */
    onUpdate.emit(pi, contentType1, contentType2);
}

void RadioTextPlus::decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(msg <= 0xffff);
    if (aid != applicationId) {
        return;
    }

    uint8_t rfu = msg >> 14; /* reserved for future use */
    if (rfu != 0) {
        decodeIssue.emit("ODA-RTP: reserved for future use");
    }
    radioTextPlus(pi).m_ert = (msg >> 13) & 1;
    radioTextPlus(pi).m_controlBit = (msg >> 12) & 1;
    radioTextPlus(pi).m_serverControlBits = (msg >> 8) & 0xf;
    radioTextPlus(pi).m_templateNumber = msg & 0xff;
}

sigc::signal<void, uint16_t, uint8_t, uint8_t> RadioTextPlus::onUpdate;

void RadioTextPlus::registerHandler()
{
    /* register ODA */
    OpenDataApplications::onDecodeA.connect(sigc::ptr_fun(decodeA));
    OpenDataApplications::onDecodeIdent.connect(sigc::ptr_fun(decodeIdent));
}

bool RadioTextPlus::empty(uint8_t contentType)
{
    std::string str = valueUtf8(contentType);
    return (str.find_first_not_of(' ') == std::string::npos);
}

void RadioTextPlus::deleteItems()
{
    for (uint8_t code = 1; code <= 11; code++) {
        m_value.erase(code);
    }
}

std::map<uint16_t, RadioTextPlus> & radioTextPlus()
{
    /* singleton */
    static std::map<uint16_t, RadioTextPlus> list;
    return list;
}

RadioTextPlus & radioTextPlus(uint16_t pi)
{
    return radioTextPlus()[pi];
}

}
