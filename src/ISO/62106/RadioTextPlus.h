/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <map>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/** Radio Text Plus (RT+) */
class ISO_62106_EXPORT RadioTextPlus
{
public:
    explicit RadioTextPlus();

    /** AID */
    static constexpr uint16_t applicationId = 0x4bd7;

    /**
     * eRT flag (0=RT 1=eRT)
     *
     * @return true if set
     */
    bool ert() const;

    /**
     * control bits flag (0=no template available, 1=template available)
     *
     * @return 1 if template available
     */
    uint8_t controlBit() const;

    /**
     * server control bits
     *
     * @return value
     */
    uint8_t serverControlBits() const;

    /**
     * template number
     *
     * @return value
     */
    uint8_t templateNumber() const;

    /**
     * item toggle bit
     *
     * @return value
     */
    uint8_t itemToggleBit() const;

    /**
     * item running bit
     *
     * @return value
     */
    uint8_t itemRunningBit() const;

    /**
     * RT+: content strings (UCS-2 coded)
     *
     * @param[in] contentType content type
     * @return Content string
     */
    std::u16string valueUcs2(uint8_t contentType) const;

    /**
     * RT+: content strings (UTF8 coded)
     *
     * @param[in] contentType content type
     * @return Content string
     */
    std::string valueUtf8(uint8_t contentType) const;

    /**
     * @brief Return RT+ class string
     *
     * This function returns a RT+ class string.
     *
     * @param[in] rtp RT+ class code
     * @return RT+ class string
     */
    static std::string classStr(uint8_t rtp);

    /** event handler on update */
    static sigc::signal<void, uint16_t, uint8_t, uint8_t> onUpdate;

    /**
     * @brief Decodes and handles received RT+ information
     *
     * This function decodes and handles received RT+ information.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] blk2 Block 2 data
     * @param[in] blk3 Block 3 data
     * @param[in] blk4 Block 4 data
     */
    static void decodeA(uint16_t pi, uint16_t aid, uint8_t blk2, uint16_t blk3, uint16_t blk4);

    /**
     * @brief Decode and handles received RT+ assign message
     *
     * This function decodes and handles received RT+ assign messages.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] msg Message bits
     */
    static void decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg);

    /** register handler */
    static void registerHandler();

private:
    /** each RT+ string consists of 128 16-bit characters */
    using Characters = std::array<char16_t, 128>;

    /** RT+: RT+ eRT flag (0=RT 1=eRT) */
    bool m_ert : 1;

    /** RT+: control bits flag (0=no template available, 1=template available) */
    uint8_t m_controlBit : 1;

    /** RT+: server control bits */
    uint8_t m_serverControlBits : 4;

    /** RT+: template number */
    uint8_t m_templateNumber : 8;

    /** RT+: item toggle bit */
    uint8_t m_itemToggleBit : 1;

    /** RT+: item running bit */
    uint8_t m_itemRunningBit : 1;

    /**
     * RT+: content strings (UCS-2 coded)
     *   -Key: contentType (0..63)
     *   -Value: 128 16-bit character strings
     */
    std::map<uint8_t, Characters> m_value; // encoding: UCS-2

    /**
     * Check if content is empty (only spaces)
     *
     * @param[in] contentType content type
     * @return True if empty, false otherwise
     */
    bool empty(uint8_t contentType);

    /**
     * Delete all entries of ITEM category.
     */
    void deleteItems();
};

/**
 * Return reference to ODA-RTP list.
 *
 * @return Reference to ODA-RTP list.
 */
extern ISO_62106_EXPORT std::map<uint16_t, RadioTextPlus> & radioTextPlus(); /* singleton */

/**
 * Return reference to ODA-RTP entry.
 *
 * @param[in] pi PI code
 * @return Reference to ODA-RTP entry.
 */
extern ISO_62106_EXPORT RadioTextPlus & radioTextPlus(uint16_t pi);

}
