/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RdsClock.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

RdsClock::RdsClock(RdsTime & time) :
    time(time),
    minuteChangeTimeout(std::chrono::system_clock::now() + std::chrono::seconds(61)),
    nextTime(time),
    m_clockThread(nullptr)
{
    /* set next time */
    nextTime.addTime(0, 0, 1); // increase by one minute
}

void RdsClock::startThread()
{
    /* start and detach thread */
    if (!m_clockThread) {
        m_clockThread = new std::thread(clockTimedOut, this);
        m_clockThread->detach();
    }
}

void RdsClock::ctUpdated(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);

    /* set new time */
    time = program.clockTime.value();

    /* set next time */
    nextTime = time;
    nextTime.addTime(0, 0, 1); // increase by one minute

    /* timeout one second after expected CT update */
    minuteChangeTimeout = std::chrono::system_clock::now() + std::chrono::seconds(61);

    /* emit time signal */
    onMinuteChange.emit(time);
}

[[ noreturn ]] void RdsClock::clockTimedOut(RdsClock * clock)
{
    /* check */
    assert(clock);

    /* run till program terminates */
    for (;;) {
        /* sleep until next timeout */
        std::this_thread::sleep_until(clock->minuteChangeTimeout);

        /* check if timeout occured */
        if (std::chrono::system_clock::now() > clock->minuteChangeTimeout) {
            /* set new time */
            clock->time = clock->nextTime;

            /* set next time */
            clock->nextTime = clock->time;
            clock->nextTime.addTime(0, 0, 1); // increase by one minute

            /* next timeout in 60 seconds */
            clock->minuteChangeTimeout += std::chrono::seconds(60);

            /* emit time signal */
            onMinuteChange.emit(clock->time);
        }
    }
}

sigc::signal<void, RdsTime> RdsClock::onMinuteChange;

}
