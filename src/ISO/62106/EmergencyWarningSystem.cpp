/*
 * Copyright (C) 2009-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EmergencyWarningSystem.h"

#include <cassert>

#include "RdsProgram.h"

namespace ISO62106 {

EmergencyWarningSystem::EmergencyWarningSystem(const RdsProgram & parent) :
    m_parent(parent)
{
}

void EmergencyWarningSystem::decodeIdent(uint16_t id)
{
    /* check */
    assert(id <= 0xfff);

    /* call event handler */
    onUpdateIdent.emit(id);
}

void EmergencyWarningSystem::decode(uint8_t blk2, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert(blk2 <= 0x1f);
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value(), blk2, blk3, blk4);
}

sigc::signal<void, uint16_t> EmergencyWarningSystem::onUpdateIdent;

sigc::signal<void, uint16_t, uint8_t, uint16_t, uint16_t> EmergencyWarningSystem::onUpdate;

}
