/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Programme Item Number (PIN) */
class ISO_62106_EXPORT ProgrammeItemNumber
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit ProgrammeItemNumber(const RdsProgram & parent);

    /**
     * day
     *
     * @return day
     */
    uint8_t day() const;

    /**
     * hour
     *
     * @return hour
     */
    uint8_t hour() const;

    /**
     * minute
     *
     * @return minute
     */
    uint8_t minute() const;

    /**
      * @brief Decodes and handles received PIN codes
      *
      * This function decodes and handles received PIN codes.
      *
      * @param[in] day Day
      * @param[in] hour Hour
      * @param[in] minute Minute
      */
    void decode(uint8_t day, uint8_t hour, uint8_t minute);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** day */
    uint8_t m_day : 5;

    /** hour */
    uint8_t m_hour : 5;

    /** minute */
    uint8_t m_minute : 6;
};

}
