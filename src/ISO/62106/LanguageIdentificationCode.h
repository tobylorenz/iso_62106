/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <string>

#include <sigc++-2.0/sigc++/signal.h>

#include "iso_62106_export.h"
#include "platform.h"

namespace ISO62106 {

/* forward declaration */
class RdsProgram;

/** Language Identification Code (LIC) */
class ISO_62106_EXPORT LanguageIdentificationCode
{
public:
    /**
     * Constructor
     *
     * @param parent Link to parent
     */
    explicit LanguageIdentificationCode(const RdsProgram & parent);

    /**
     * Language Identification Code
     *
     * @return value
     */
    uint16_t value() const;

    /**
     * @brief Return LIC string
     *
     * This function returns a LIC string.
     *
     * @return LIC string
     */
    std::string language() const;

    /**
     * @brief Decodes and handles received LI Codes
     *
     * This function decodes and handles received LI Codes.
     *
     * @param[in] lic LI Code
     */
    void decode(uint16_t lic);

    /** event handler on change */
    static sigc::signal<void, uint16_t> onChange;

    /** event handler on update */
    static sigc::signal<void, uint16_t> onUpdate;

private:
    /** link to program */
    const RdsProgram & m_parent;

    /** Language Identification Code */
    uint16_t m_value : 12;
};

}
