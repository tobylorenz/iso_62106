/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "LanguageIdentificationCode.h"

#include <cassert>
#include <iomanip>
#include <sstream>

#include <sqlite3.h>

#include "Database.h"
#include "RdsProgram.h"

namespace ISO62106 {

LanguageIdentificationCode::LanguageIdentificationCode(const RdsProgram & parent) :
    m_parent(parent),
    m_value(0)
{
}

uint16_t LanguageIdentificationCode::value() const
{
    return m_value;
}

std::string LanguageIdentificationCode::language() const
{
    /* prepare SQL query */
    std::stringstream sql;
    sql
            << std::hex << std::setfill('0') << std::uppercase
            << "select NAME from LIC where LIC='"
            << std::setw(2) << static_cast<uint16_t>(m_value)
            << "'";

    /* execute SQL query */
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(database().handle(), sql.str().c_str(), sizeof(sql), &stmt, nullptr);
    std::string str;
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        str = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    }

    /* finish SQL */
    (void) sqlite3_finalize(stmt);

    return str;
}

void LanguageIdentificationCode::decode(uint16_t lic)
{
    /* check */
    assert(lic <= 0xfff);

    /* change check */
    if (m_value != lic) {
        m_value = lic;

        /* call event handler */
        onChange.emit(m_parent.programmeIdentification.value());
    }

    /* call event handler */
    onUpdate.emit(m_parent.programmeIdentification.value());
}

sigc::signal<void, uint16_t> LanguageIdentificationCode::onChange;

sigc::signal<void, uint16_t> LanguageIdentificationCode::onUpdate;

}
