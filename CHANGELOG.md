# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.2.0] - 2017-09-22
### Added
- RdsTime::setTime(uint8_t hour, uint8_t minute) added
- enhancedRadioText(), radioTextPlus() and rdsProgram() to improve singletons
### Changed
- Update to latest project template
- "using" instead of "typedef"

## [1.1.0] - 2017-02-16
### Added
- RdsTime::valid() to check if date/time was set properly
### Changed
- RdsTime::addTime() replaces RdsTime::operator+()
- RdsTime::subTime() replaces RdsTime::operator-()

## [1.0.0] - 2017-01-16
### Added
- Initial version
